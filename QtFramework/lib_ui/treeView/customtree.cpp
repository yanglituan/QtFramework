CustomTree::CustomTree(QWidget *parent)
    :QTreeView(parent)
{
    m_model = new QStandardItemModel(this);
    this->setModel(m_model);
    // CustomTreeDelegate是CustomTree定制的代理，所以在内部设置
    this->setHeaderHidden(true);
    this->setEditTriggers(QAbstractItemView::NoEditTriggers);
    this->setRootIsDecorated(false); // 设置根节点的小箭头隐藏
    this->setStyleSheet("QTreeView::branch {image:none;}"); // 设置子项的小箭头隐藏
    this->setItemDelegate(new CustomTreeDelegate(this, this));

    m_bgColor = QColor("#31495A");
    m_hoverBgColor = QColor("#77D8C4");
    m_checkedBgColor = QColor("#18BD9B");
    m_textColor = QColor("#F0F0F0");
    m_hoverTextColor = QColor("#000000");
    m_checkedTextColor = QColor("#F0F0F0");
    m_lineColor = QColor("#404244");
}

void CustomTree::addItem(const QString &text, QStandardItem *parent)
{
    if (NULL == parent)
    {
        m_model->appendRow(new QStandardItem(text));
    }
    else
    {
        parent->appendRow(new QStandardItem(text));
    }
}

void CustomTree::addItem(const QString &parentText, const QString &text)
{
    QList<QStandardItem *> ls = m_model->findItems(parentText, Qt::MatchRecursive);

    if (!ls.isEmpty())
    {
        foreach (QStandardItem *item, ls)
        { // 找到的都加上
            addItem(text, item);
        }
    }
}

void CustomTree::setItemInfo(const QString &itemText, const QString &info)
{
    if (m_infoMap.contains(itemText))
    {
        m_infoMap.remove(itemText);
        m_infoMap.insert(itemText, info);
        return;
    }

    m_infoMap.insert(itemText, info);
}

QString CustomTree::infoStr(const QModelIndex &index)
{
    QString itemText = m_model->itemFromIndex(index)->data(Qt::DisplayRole).toString();

    if (m_infoMap.contains(itemText))
    {
        return m_infoMap.value(itemText);
    }

    return QString();
}
