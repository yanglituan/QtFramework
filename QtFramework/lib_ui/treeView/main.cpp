#include "widget.h"
#include <QApplication>
#include <QFile>
#include <QStyleFactory>
#include <QDebug>
#include <QTextCodec>


/*
 * 函数名：setSkin
 * 函数说明: 设置样式
 *
 *
 * 输入参数： app：需要设置样式的界面  skinFile：样式文件路径
 * 输出参数：无
 * 返回值：无
 * */
bool setSkin(QApplication* const app, QString const &skinFile)
{
    QFile file(skinFile);
    if (file.exists() && file.open(QIODevice::ReadOnly))
    {
        QApplication::setStyle(QStyleFactory::create("Windows"));
        QString strTemp;
        QTextStream in(&file);
        while (!in.atEnd())
        {
            strTemp.append(in.readLine());
        }
        file.close();
        app->setStyleSheet(strTemp);
    }
    else
    {
        qDebug() << "Qss file don't exist." << file.errorString();
    }

    return true;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QTextCodec::setCodecForLocale(QTextCodec::codecForName("utf-8"));

    Widget w;
    w.show();
    setSkin(&a, QApplication::applicationDirPath() + "/treeview.qss");
    
    return a.exec();
}
