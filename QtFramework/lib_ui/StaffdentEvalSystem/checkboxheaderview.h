#ifndef CHECKBOXHEADERVIEW_H
#define CHECKBOXHEADERVIEW_H

#include<QObject>
#include<QHeaderView>
#include<QPainter>

class CheckBoxHeaderView:public QHeaderView
{
public:
    CheckBoxHeaderView(Qt::Orientation orientation,QWidget* parent=0);
    ~CheckBoxHeaderView();
protected:
    void paintSection(QPainter *painter, const QRect &rect, int logicalIndex) const;
    void mousePressEvent(QMouseEvent* event);

private:
    bool isOn;
};

#endif // CHECKBOXHEADERVIEW_H
