#include "checkboxheaderview.h"
#include<QCheckBox>
#include<QDebug>


/*
 * 函数名：ButtonDelegate
 * 函数说明:构造函数
 *
 *
 * 输入参数：orientation：表头类型 水平 or 垂直 parent： 父类指针
 * 输出参数：无
 * 返回值：无
 * */
CheckBoxHeaderView::CheckBoxHeaderView(Qt::Orientation orientation, QWidget *parent)
    :QHeaderView(orientation,parent)
{

}


/*
 * 函数名：~CheckBoxHeaderView
 * 函数说明:析构函数
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
CheckBoxHeaderView::~CheckBoxHeaderView()
{

}


/*
 * 函数名：paintSection
 * 函数说明:重画部分
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
void CheckBoxHeaderView::paintSection(QPainter *painter, const QRect &rect, int logicalIndex) const
{
    painter->save();
    QHeaderView::paintSection(painter,rect,logicalIndex);
    //painter->restore();
    if(logicalIndex==0)
    {
        QStyleOptionButton option;
        option.iconSize  = QSize(10,10);
        option.rect = rect;//rect.adjusted(4,4,-30,-4);左对齐   
        if(isOn)
        {
            option.state = QStyle::State_On;
        }else
        {
            option.state = QStyle::State_Off;
        }
        painter->restore();
        this->style()->drawPrimitive(QStyle::PE_IndicatorCheckBox,&option,painter);
    }
}


/*
 * 函数名：mousePressEvent
 * 函数说明:鼠标按下事件
 *
 *
 * 输入参数：event：事件类型
 * 输出参数：无
 * 返回值：无
 * */
void CheckBoxHeaderView::mousePressEvent(QMouseEvent *event)
{
    if(isOn)
    {
        isOn = false;
    }else{
        isOn = true;
    }
    this->update();
    QHeaderView::mousePressEvent(event);
}

