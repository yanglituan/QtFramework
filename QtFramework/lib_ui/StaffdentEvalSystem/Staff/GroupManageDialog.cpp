#include "GroupManageDialog.h"
#include "ui_GroupManageDialog.h"
#include <QMenu>
#include <QCursor>

#include <QMessageBox>

/*
 * 函数名：GroupManageDialog
 * 函数说明:构造函数
 *
 *
 * 输入参数：parent： 父类指针
 * 输出参数：无
 * 返回值：无
 * */
GroupManageDialog::GroupManageDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GroupManageDialog)
{
    ui->setupUi(this);


    initActions();

    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);

    connect(ui->comboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(onDepChanged(int)));

    connect(ui->tableWidget,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(onMenu(QPoint)));


    connect(ui->tableWidget,SIGNAL(itemChanged(QTableWidgetItem*)),this,SLOT(onItemChanged(QTableWidgetItem*)));
}


/*
 * 函数名：~GroupManageDialog
 * 函数说明:析构函数
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
GroupManageDialog::~GroupManageDialog()
{
    delete ui;
}


/*
 * 函数名：initActions
 * 函数说明:析构函数
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
void GroupManageDialog::initActions()
{
    m_actionAdd = new QAction("新建小组",this);

    m_actionDel = new QAction("删除小组",this);

    connect(m_actionAdd,SIGNAL(triggered(bool)),this,SLOT(onAddGroup()));

    connect(m_actionDel,SIGNAL(triggered(bool)),this,SLOT(onDelGroup()));

}


/*
 * 函数名：refresh
 * 函数说明:刷新部门数据
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
void GroupManageDialog::refresh()
{

    loadDeps();

}


/*
 * 函数名：loadDeps
 * 函数说明:加载部门数据
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
void GroupManageDialog::loadDeps()
{
    QList<DepartmentInfo> deps = m_departmentDao.departmentList();

    ui->comboBox->clear();

    for(DepartmentInfo info : deps)
    {
        ui->comboBox->addItem(info.dep_name,info.dep_id);
    }

}


/*
 * 函数名：loadGroupByDep
 * 函数说明:通过部门信息加载小组数据
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
void GroupManageDialog::loadGroupByDep()
{

    m_isLoading = true;

    int dep_id = ui->comboBox->currentData().toInt();


    QList<GroupInfo> groups = m_basicService.groupList(dep_id);

    ui->tableWidget->clear();

    ui->tableWidget->setRowCount(groups.size());

    ui->tableWidget->setColumnCount(1);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList()<<"小组名称");

    for(int i = 0 ; i < groups.size();i++)
    {
        GroupInfo & info = groups[i];

        QTableWidgetItem * item = new QTableWidgetItem(info.group_name);

        item->setData(Qt::UserRole,info.group_id);

        ui->tableWidget->setItem(i,0,item);

    }

    m_isLoading = false;
}


/*
 * 函数名：onDepChanged
 * 函数说明:槽函数 部门改变了重新加载小组数据
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
void GroupManageDialog::onDepChanged(int)
{
    loadGroupByDep();
}


/*
 * 函数名：onMenu
 * 函数说明：槽函数 右键点击表格弹出菜单
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
void GroupManageDialog::onMenu(QPoint)
{

    QMenu menu;

    menu.addActions(QList<QAction*>()<<m_actionAdd<<m_actionDel);

    menu.exec(QCursor::pos());
}

/*
 * 函数名：onAddGroup
 * 函数说明：槽函数 添加小组
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
void GroupManageDialog::onAddGroup()
{
    ui->tableWidget->insertRow(ui->tableWidget->rowCount());
}

/*
 * 函数名：onDelGroup
 * 函数说明：槽函数 删除小组
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
void GroupManageDialog::onDelGroup()
{
    QTableWidgetItem * item = ui->tableWidget->currentItem();


    if(item)
    {
        int btn = QMessageBox::warning(this,"警告","删除该小组将同时删除小组下所有员工信息,是否继续删除","是","否");

        if(btn == 0)
        {
            //执行删除

            int Group_id = item->data(Qt::UserRole).toInt();

            m_basicService.delGroup(Group_id);

            //刷新

            this->loadGroupByDep();
        }
    }
}


/*
 * 函数名：onItemChanged
 * 函数说明：槽函数 表格数据有变化
 *
 *
 * 输入参数：item：改变的item
 * 输出参数：无
 * 返回值：无
 * */
void GroupManageDialog::onItemChanged(QTableWidgetItem * item)
{
    if(item->text() != "" && !m_isLoading)
    {


        int dep_id = ui->comboBox->currentData().toInt();


        int group_id = item->data(Qt::UserRole).toInt();

        if(group_id == 0)
        {
            m_basicService.addGroup(dep_id,item->text());

            this->loadGroupByDep();

        }else
        {

            //qDebug()<<"update";
            m_basicService.updateGroup(dep_id,group_id,item->text());

//            this->loadData();
        }

    }
}


