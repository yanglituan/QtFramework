#include "DepartmentManageDialog.h"
#include "ui_DepartmentManageDialog.h"
#include <QMenu>
#include <QMessageBox>

#include <QDebug>


/*
 * 函数名：DepartmentManageDialog
 * 函数说明:构造函数
 *
 *
 * 输入参数：parent： 父类指针
 * 输出参数：无
 * 返回值：无
 * */
DepartmentManageDialog::DepartmentManageDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DepartmentManageDialog)
{
    ui->setupUi(this);

    this->initActions();

    this->loadData();

    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);

    connect(ui->tableWidget,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(onMenu(QPoint)));

    connect(ui->tableWidget,SIGNAL(itemChanged(QTableWidgetItem*)),this,SLOT(onItemChanged(QTableWidgetItem*)));


}


/*
 * 函数名：~DepartmentManageDialog
 * 函数说明:构造函数
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
DepartmentManageDialog::~DepartmentManageDialog()
{
    delete ui;
}


/*
 * 函数名：loadData
 * 函数说明:加载数据
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
void DepartmentManageDialog::loadData()
{
    m_isLoading = true;

    ui->tableWidget->clear();

    QList<DepartmentInfo> depInfos = m_basicService.departmentList();

    ui->tableWidget->setRowCount(depInfos.size());
    ui->tableWidget->setHorizontalHeaderLabels(QStringList()<<"部门名称");

    int row = 0;

    for(DepartmentInfo info : depInfos)
    {

        QTableWidgetItem * item = new QTableWidgetItem(info.dep_name);

        item->setData(Qt::UserRole,info.dep_id);

        ui->tableWidget->setItem(row,0,item);

        row ++;
    }

    m_isLoading = false;
    emit updateData();

}


/*
 * 函数名：initActions
 * 函数说明:初始化菜单
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
void DepartmentManageDialog::initActions()
{
    m_actionAdd = new QAction("新建部门",this);

    m_actionDel = new QAction("删除部门",this);

    connect(m_actionAdd,SIGNAL(triggered(bool)),this,SLOT(onAddDep()));

    connect(m_actionDel,SIGNAL(triggered(bool)),this,SLOT(onDelDep()));

}


/*
 * 函数名：onMenu
 * 函数说明：槽函数 右键点击表格弹出菜单
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
void DepartmentManageDialog::onMenu(QPoint)
{
    QMenu menu;

    menu.addActions(QList<QAction*>()<<m_actionAdd<<m_actionDel);

    menu.exec(QCursor::pos());

}


/*
 * 函数名：onAddDep
 * 函数说明：槽函数 添加部门
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
void DepartmentManageDialog::onAddDep()
{
    ui->tableWidget->insertRow(ui->tableWidget->rowCount());
}


/*
 * 函数名：onDelDep
 * 函数说明：槽函数 删除部门
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
void DepartmentManageDialog::onDelDep()
{
    QTableWidgetItem * item = ui->tableWidget->currentItem();

    if(item)
    {
        int btn = QMessageBox::warning(this,"警告","删除该部门将同时删除部门下所有小组、员工信息,是否继续删除","是","否");

        if(btn == 0)
        {
            //执行删除

            int dep_id = item->data(Qt::UserRole).toInt();

            m_basicService.delDep(dep_id);

            //刷新

            this->loadData();
        }
    }

}

/*
 * 函数名：onItemChanged
 * 函数说明：槽函数 表格数据有变化
 *
 *
 * 输入参数：item：改变的item
 * 输出参数：无
 * 返回值：无
 * */
void DepartmentManageDialog::onItemChanged(QTableWidgetItem * item)
{


    if(item->text() != "" && !m_isLoading)
    {
        int dep_id = item->data(Qt::UserRole).toInt();

        if(dep_id == 0)
        {
            m_basicService.addDep(item->text());

            this->loadData();

        }else
        {

            qDebug()<<"update";
            m_basicService.updateDep(dep_id,item->text());

//            this->loadData();
        }

    }

}
