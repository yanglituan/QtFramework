#ifndef DEPARTMENTMANAGEDIALOG_H
#define DEPARTMENTMANAGEDIALOG_H

#include <QDialog>
#include "dao/departmentDao.h"
#include <QAction>
#include <QTableWidgetItem>
namespace Ui {
class DepartmentManageDialog;
}

class DepartmentManageDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DepartmentManageDialog(QWidget *parent = 0);
    ~DepartmentManageDialog();

private:
    Ui::DepartmentManageDialog *ui;


    void loadData();

    void initActions();
signals:
    void updateData();

private:

    DepartmentDao m_basicService;

    QAction * m_actionAdd;

    QAction * m_actionDel;


    bool m_isLoading;

private slots:

    void onMenu(QPoint);

    void onAddDep();

    void onDelDep();

    void onItemChanged(QTableWidgetItem *);


}
;

#endif // DEPARTMENTMANAGEDIALOG_H
