#ifndef GroupMANAGEDIALOG_H
#define GroupMANAGEDIALOG_H

#include <QDialog>
#include <QAction>
#include "dao/groupDao.h"
#include "Dao/departmentDao.h"

#include <QTableWidgetItem>
namespace Ui {
class GroupManageDialog;
}

class GroupManageDialog : public QDialog
{
    Q_OBJECT

public:
    explicit GroupManageDialog(QWidget *parent = 0);
    ~GroupManageDialog();

    void refresh();


private:

    void loadDeps();

    void loadGroupByDep();


    void initActions();

    bool m_isLoading;


private:
    Ui::GroupManageDialog *ui;

    groupDao m_basicService;
    DepartmentDao m_departmentDao;

    QAction * m_actionAdd;

    QAction * m_actionDel;


private slots:

    void onDepChanged(int);

    void onMenu(QPoint);

    void onAddGroup();

    void onDelGroup();


    void onItemChanged(QTableWidgetItem *);
};

#endif // GroupMANAGEDIALOG_H
