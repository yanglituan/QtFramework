#ifndef StaffLISTDIALOG_H
#define StaffLISTDIALOG_H

#include <QDialog>
#include <QTableWidgetItem>
#include "Dao/staffDao.h"
#include "Dao/groupDao.h"
#include "Dao/departmentDao.h"
#include "checkboxdelegate.h"
namespace Ui {
class StaffListDialog;
}

class StaffListDialog : public QDialog
{
    Q_OBJECT

public:
    explicit StaffListDialog(QWidget *parent = 0);
    ~StaffListDialog();


    void refresh();   

private:
    Ui::StaffListDialog *ui;

    DepartmentDao m_departmentDao;

    staffDao m_StaffService;

    groupDao m_groupDao;

    QAction * m_actionAdd;

    QAction * m_actionDel;
    CheckboxDelegate *m_checkboxDelegate;
    bool m_isAllCheck;

    int m_curPage;//当前页数
    int m_pageCount;//每一页数量
    int m_totalPage;//总共页数


    bool m_isLoading;

    QStringList m_grades;

    QString m_orderByColumnName;

    void loadGrade();

    void initActions();

    void queryCount();

private slots:


    void loadDeps();

    void loadMajorByDep();

    void loadClassByMajorAndGrade();

//    void loadStaffByClass();

    void onMenu(QPoint);

    void onAddStaff();

    void onDelStaff();

    void onItemChanged(QTableWidgetItem *item);




    void on_pushButton_printf_clicked();
    void on_pushButton_export_clicked();
    void on_pushButton_back_clicked();
    void on_pushButton_next_clicked();
    void on_pushButton_last_clicked();
    void on_pushButton_first_clicked();
    void on_spinBox_editingFinished();

    void on_checkAll_clicked(int column);

    void sortByColumn(int Column);

    void on_tableWidget_itemSelectionChanged();
    void on_tableWidget_clicked(const QModelIndex &index);
    void columnVisibleChange(QString colName);
};

#endif // StaffLISTDIALOG_H
