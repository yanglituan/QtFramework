/***************************************************
 * 类名:groupService.h
 * 功能:小组信息类
 * 日期:
 * 作者:xhh
 * 备注:
 **************************************************/

#ifndef GROUPSERVICE_H
#define GROUPSERVICE_H

#include <QObject>
#include "allentitys.h"
#include "DB/sqlInterface.h"
#include "staffDao.h"

class groupDao : public QObject
{
    Q_OBJECT
public:
    explicit groupDao(QObject *parent = 0);

    QList<GroupInfo> groupList();//查询所有小组信息
    QList<GroupInfo> groupList(int dep_id);//查询一个部门下的小组信息
    int delGroup(int group_id);//删除小组信息
    int delGroup();//删除所有小组信息
    int addGroup(int dep_id, QString group_name);//添加小组信息
    int updateGroup(int dep_id,int group_id,QString group_name);//更新小组信息
    int updateGroup(int dep_id);//部门删除时更新所有部门id为空
public:
    sqlInterface sqlDML;
    staffDao     staffs;
};

#endif // GROUPSERVICE_H
