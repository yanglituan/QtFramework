#include "departmentDao.h"

/***************************************************
 * 名称:DepartmentDao
 * 功能:构造函数
 * 输入:parent对象
 * 输出:无
 * 备注:
 **************************************************/
DepartmentDao::DepartmentDao(QObject *parent) : QObject(parent)
{

}

/***************************************************
 * 名称:departmentList
 * 功能:查询部门所有信息
 * 输入:无
 * 输出:部门信息列表
 * 备注:
 **************************************************/
QList<DepartmentInfo> DepartmentDao::departmentList()
{
    QString sql = "select * from t_department";
    QList<QVariantMap> result = sqlDML.queryList(sql);

    QList<DepartmentInfo> deps ;
    for(QVariantMap map : result)
    {
        int dep_id = map["dep_id"].toInt();

        QString dep_name = map["dep_name"].toString();

        DepartmentInfo depInfo;
        depInfo.dep_id = dep_id;
        depInfo.dep_name = dep_name;
        deps<<depInfo;
    }

    return deps;
}

/***************************************************
 * 名称:delDep
 * 功能:删除部门指定信息
 * 输入:dep_id 部门编号
 * 输出:-1 编号不正常，0 删除失败，1 删除正常
 * 备注:
 **************************************************/
int DepartmentDao::delDep(int dep_id)
{
    if(dep_id<0)
    {
        return -1;
    }
    QString sql = "delete from t_department where dep_id = ?";

    if(sqlDML.exec(sql,QVariantList()<<dep_id)){
        //需要把小组中该部门的所有部门id更新为“其他”
        int result = groups.updateGroup(dep_id);
        if(0 == result)
        {
            //需要回滚该数据删除操作
            //...
        }
        qDebug() << "删除部门成功。";
        return 1;
    }
    else{
        qDebug() << "删除部门失败。";
        return 0;
    }
}

/***************************************************
 * 名称:delDep
 * 功能:删除部门所有信息
 * 输入:无
 * 输出:0 删除失败，1 删除正常
 * 备注:
 **************************************************/
int DepartmentDao::delDep()
{
    //首先记录所有部门编号
    QStringList depId = sqlDML.queryOneFieldResult("select dep_id from t_department");
    if(depId.size()<0)
    {
        return 1;
    }
    qDebug() << "------DepartmentDao::delDep()------" << depId;

    QString sql = "delete from t_department";
    if(sqlDML.exec(sql)){
        for(int i=0;i<depId.size();i++)
        {
            int result = groups.updateGroup(depId.at(i).toInt());
            if(0 == result)
            {
                qDebug() << "删除部门所有信息,更新" << depId.at(i) << "部门的小组部门id时失败。";
                //需要回滚该数据删除操作
                return 0;
            }
        }
        qDebug() << "删除部门成功。";
        return 1;
    }
    else{
        qDebug() << "删除部门失败。";
        return 0;
    }
}

/***************************************************
 * 名称:addDep
 * 功能:添加部门信息
 * 输入:部门名称
 * 输出:0 添加失败，1 添加成功
 * 备注:
 **************************************************/
int DepartmentDao::addDep(QString dep_name)
{
    QString sql = "insert into t_department(dep_name) values(?)";

    if(sqlDML.exec(sql,QVariantList()<<dep_name)){
        qDebug() << tr("添加部门信息成功。");
        return 1;
    }
    else{
        qDebug() << tr("添加部门信息失败。");
        return 0;
    }
}

/***************************************************
 * 名称:updateDep
 * 功能:更新部门信息
 * 输入:dep_id 部门编号，dep_name 部门名称
 * 输出:0 更新失败，1 更新成功
 * 备注:
 **************************************************/
int DepartmentDao::updateDep(int dep_id, QString dep_name)
{
    QString sql = "update t_department set dep_name = ?  where dep_id = ?";
    if(sqlDML.exec(sql,QVariantList()<<dep_name<<dep_id)){
        qDebug() << tr("修改部门信息成功。");
        return 1;
    }
    else{
        qDebug() << tr("修改部门信息失败。");
        return 0;
    }
}
