#ifndef STAFFSERVICE_H
#define STAFFSERVICE_H

#include <QObject>
#include <QSql>
#include "allentitys.h"
#include "DB/sqlInterface.h"
//#include "DB/sqlHelper.h"

class staffDao : public QObject
{
    Q_OBJECT
public:
    explicit staffDao(QObject *parent = 0);

    QList<StaffInfo> staffList();//查询成员所有信息
    QList<StaffInfo> staffList(int group_id);//查询指定成员信息
    QSqlQueryModel* staffModel();//查询成员所有信息
    QSqlQueryModel* staffModel(int group_id);//查询指定成员信息
    int delStaff(int staff_id);//删除成员信息
    int delStaff();//删除所有成员信息
    int addStaff(int group_id, int staff_sex, QString staff_name);//添加成员信息
    int updateStaff(int group_id, int staff_id, int staff_sex, QString group_name);//更新小组信息
    int updateStaff(int group_id);//小组删除时更新所有小组id为空

public:
    sqlInterface sqlDML;
};

#endif // STAFFSERVICE_H
