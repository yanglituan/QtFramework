#include "staffDao.h"

/***************************************************
 * 名称:staffDao
 * 功能:构造函数
 * 输入:parent对象
 * 输出:无
 * 备注:
 **************************************************/
staffDao::staffDao(QObject *parent) : QObject(parent)
{

}

/***************************************************
 * 名称:staffList
 * 功能:查询所有小组内的信息
 * 输入:无
 * 输出:小组信息列表
 * 备注:
 **************************************************/
QList<StaffInfo> staffDao::staffList()
{
    QString sql = "select * from t_staffInfo";
    QList<QVariantMap> result = sqlDML.queryList(sql);

    QList<StaffInfo> staffs ;
    for(QVariantMap map : result)
    {
        int group_id = map["group_id"].toInt();
        int staff_idcard = map["staff_idcard"].toInt();
        int staff_sex = map["staff_sex"].toInt();
        QString staff_name = map["staff_name"].toString();

        StaffInfo staffInfo;
        staffInfo.group_id = group_id;
        staffInfo.staff_idcard = staff_idcard;
        staffInfo.staff_sex = staff_sex;
        staffInfo.staff_name = staff_name;

        staffs<<staffInfo;
    }

    return staffs;
}

/***************************************************
 * 名称:staffList
 * 功能:查询一个小组内所有信息
 * 输入:group_id 组编号
 * 输出:小组信息列表
 * 备注:
 **************************************************/
QList<StaffInfo> staffDao::staffList(int group_id)
{
    QString sql = "select * from t_staffInfo where group_id=?";
    QList<QVariantMap> result = sqlDML.queryList(sql,QVariantList()<<group_id);

    QList<StaffInfo> staffs ;
    for(QVariantMap map : result)
    {
        int group_id = map["group_id"].toInt();
        int staff_idcard = map["staff_idcard"].toInt();
        int staff_sex = map["staff_sex"].toInt();
        QString staff_name = map["staff_name"].toString();

        StaffInfo staffInfo;
        staffInfo.group_id = group_id;
        staffInfo.staff_idcard = staff_idcard;
        staffInfo.staff_sex = staff_sex;
        staffInfo.staff_name = staff_name;

        staffs<<staffInfo;
    }

    return staffs;
}

/***************************************************
 * 名称:staffModel
 * 功能:查询所有小组的信息
 * 输入:无
 * 输出:小组信息列表model
 * 备注:
 **************************************************/
QSqlQueryModel* staffDao::staffModel()
{
    QString sql = "select * from t_staffInfo";
    QSqlQueryModel *result = new QSqlQueryModel();
    result = sqlDML.queryModel(sql);

    return result;
}

/***************************************************
 * 名称:staffModel
 * 功能:查询一个小组的信息
 * 输入:group_id 小组编号
 * 输出:小组信息列表model
 * 备注:
 **************************************************/
QSqlQueryModel* staffDao::staffModel(int group_id)
{
    QString sql = "select * from t_staffInfo where group_id=?";
    QSqlQueryModel* result = new QSqlQueryModel();
    result = sqlDML.queryModel(sql,QVariantList()<<group_id);

    return result;
}

/***************************************************
 * 名称:delStaff
 * 功能:删除指定员工信息
 * 输入:staff_id 员工编号
 * 输出:-1 编号不正常，0 删除失败，1 删除正常
 * 备注:
 **************************************************/
int staffDao::delStaff(int staff_id)
{
    if(staff_id<0)
    {
        return -1;
    }
    QString sql = "delete from t_staffInfo where staff_idcard = ?";

    if(sqlDML.exec(sql,QVariantList()<<staff_id)){
        return 1;
    }
    else{
        return 0;
    }
}

/***************************************************
 * 名称:delDep
 * 功能:删除部门所有信息
 * 输入:无
 * 输出:0 删除失败，1 删除正常
 * 备注:
 **************************************************/
int staffDao::delStaff()
{
    //首先记录所有小组编号
    QStringList staffId = sqlDML.queryOneFieldResult("select staff_idcard from t_staffInfo");
    if(staffId.size()<0)
    {
        return 1;
    }
    qDebug() << "------staffDao::delStaff()------" << staffId;

    QString sql = "delete from t_staffInfo";
    if(sqlDML.exec(sql)){
        qDebug() << tr("删除员工成功。");
        return 1;
    }
    else{
        qDebug() << tr("删除员工失败。");
        return 0;
    }
}

/***************************************************
 * 名称:addStaff
 * 功能:添加小组信息
 * 输入:group_id 小组编号,
 *     staff_name 员工名称,
 *     staff_sex  员工性别(0-男，1-女)
 * 输出:-1 参数无效，-2 小组编号冲突，0 失败，1 成功
 * 备注:
 **************************************************/
int staffDao::addStaff(int group_id, int staff_sex, QString staff_name)
{
    //无效参数
    if(group_id<0 || staff_name.isEmpty())
    {
        return -1;
    }
    qDebug() << "sex:" << staff_sex;
    if(staff_sex>1 && staff_sex<0)
    {
        return -1;
    }

    //开始插入
    QString sql = "insert into t_staffInfo(group_id,staff_sex,staff_name) values(?,?,?)";
    qDebug() << sql;
    if(sqlDML.exec(sql,QVariantList()<<group_id<<staff_sex<<staff_name)){
        qDebug() << "添加员工成功。" ;
        return 1;
    }
    else{
        qDebug() << "添加员工失败。";
        return 0;
    }
}

/***************************************************
 * 名称:updateGroup
 * 功能:更新小组信息
 * 输入:group_id 小组编号，
 *     group_name 小组名称
 *     type  0-同一个部门更新小组信息,1-变更到不同部门，但是编号和名称不变
 * 输出:-1 参数无效，0 失败，1 成功
 * 备注:
 **************************************************/
int staffDao::updateStaff(int group_id, int staff_id, int staff_sex, QString staff_name)
{
    QString sql = "";
    //无效参数
    if(group_id<0 || staff_id<0 || staff_name.isEmpty())
    {
        return -1;
    }
    if(staff_sex>1 || staff_sex<0)
    {
        return -1;
    }


    sql = "update t_staffInfo set group_id=?,staff_sex=?,staff_name=? where staff_idcard=?";
    if(sqlDML.exec(sql,QVariantList()<<group_id<<staff_sex<<staff_name<<staff_id)){
        qDebug() << "更新员工多个信息成功。" << sql;
        return 1;
    }
    else{
        qDebug() << "更新员工多个信息失败。" << sql;
        return 0;
    }
}

/***************************************************
 * 名称:updateGroup
 * 功能:更新小组信息
 * 输入:group_id 小组编号，
 * 输出:-1 参数无效，0 失败，1 成功
 * 备注:
 **************************************************/
int staffDao::updateStaff(int group_id)
{
    QString sql = "";
    //无效参数
    if(group_id<0)
    {
        return -1;
    }
    QStringList list = sqlDML.queryOneFieldResult("select staff_idcard from t_staffInfo where group_id=?",QVariantList()<<group_id);
    if(list.size()<0)
    {
        return 1;
    }
    for(int i=0;i<list.size();i++)
    {
        sql = "update t_staffInfo set group_id=-1 where staff_idcard=?";
        sqlDML.exec(sql,QVariantList()<<list.at(i).toInt());
    }
    return 1;
}
