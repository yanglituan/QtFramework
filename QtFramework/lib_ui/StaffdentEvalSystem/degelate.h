#ifndef DEGELATE_H
#define DEGELATE_H

#include <QItemDelegate>
#include <QLineEdit>
#include <QComboBox>
#include <QSpinBox>
#include <QApplication>

//编号列，只读委托
class ReadOnlyDelegate : public QItemDelegate
{
    Q_OBJECT
    public:
    ReadOnlyDelegate(QObject *parent = 0): QItemDelegate(parent) { }//构造函数
    QWidget *createEditor(QWidget*parent, const QStyleOptionViewItem &option,
    const QModelIndex &index) const
    {
        return NULL;
    }
};

//ID列，只能输入1－12个数字
//利用QLineEdit委托和正则表达式对输入进行限制
class UserIDDelegate : public QItemDelegate
{
    Q_OBJECT
    public:
    UserIDDelegate(QObject *parent = 0): QItemDelegate(parent) { }//构造函数
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,const QModelIndex &index) const
    {
        QLineEdit *editor = new QLineEdit(parent);//创建一个控件LineEdit
        QRegExp regExp("[0-9]{0,11}");//限定只能输入1－11个数字
        editor->setValidator(new QRegExpValidator(regExp, parent));
        return editor;
    }
    void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        QString text = index.model()->data(index, Qt::EditRole).toString();
        QLineEdit *lineEdit = static_cast<QLineEdit*>(editor);
        lineEdit->setText(text);//窗口设定内容
    }
    void setModelData(QWidget *editor, QAbstractItemModel *model,const QModelIndex &index) const
    {
        QLineEdit *lineEdit = static_cast<QLineEdit*>(editor);
        QString text = lineEdit->text();
        model->setData(index, text, Qt::EditRole);
    }
    void updateEditorGeometry(QWidget *editor,const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        editor->setGeometry(option.rect);//矩形框
    }
};

//年龄列，利用QSpinBox委托进行输入限制，只能输入1－100之间的数字
class AgeDelegate : public QItemDelegate
{
    Q_OBJECT
    public:
    AgeDelegate(QObject *parent = 0): QItemDelegate(parent) { }
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
    const QModelIndex &index) const
    {
        QSpinBox *editor = new QSpinBox(parent);
        editor->setMinimum(1);
        editor->setMaximum(100);
        return editor;
    }
    void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        int value = index.model()->data(index, Qt::EditRole).toInt();
        //QSpinBox *spinBox = static_cast<<span style=" color:#800080;">QSpinBox*>(editor);
        QSpinBox *spinBox = static_cast<QSpinBox*>(editor);
        spinBox->setValue(value);
    }
    void setModelData(QWidget *editor, QAbstractItemModel *model,
    const QModelIndex &index) const
    {
        QSpinBox *spinBox = static_cast<QSpinBox*>(editor);
        spinBox->interpretText();
        int value = spinBox->value();
        model->setData(index, value, Qt::EditRole);
    }
    void updateEditorGeometry(QWidget *editor,
    const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        editor->setGeometry(option.rect);
    }
};

//性别列，利用QComboBox委托对输入进行限制
//这一列的单元格只能输入goose1或goose2
class SexDelegate : public QItemDelegate
{
    Q_OBJECT
    public:
    SexDelegate(QObject *parent = 0): QItemDelegate(parent) { }
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
    const QModelIndex &index) const
    {
        QComboBox *editor = new QComboBox(parent);
        editor->addItem(tr("男"));//添加下拉项
        editor->addItem(tr("女"));//添加下拉项
        return editor;
    }
    void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        QString text = index.model()->data(index, Qt::EditRole).toString();
        QComboBox *comboBox = static_cast<QComboBox*>(editor);
        int tindex = comboBox->findText(text);
        comboBox->setCurrentIndex(tindex);
    }
    void setModelData(QWidget *editor, QAbstractItemModel *model,
    const QModelIndex &index) const
    {
        QComboBox *comboBox = static_cast<QComboBox*>(editor);
        QString text = comboBox->currentText();
        model->setData(index, text, Qt::EditRole);
    }
    void updateEditorGeometry(QWidget *editor,
    const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        editor->setGeometry(option.rect);
    }
};

//头像列，只是在单元格中央放一张小图而已
class IconDelegate : public QItemDelegate
{
    Q_OBJECT
    public:
    IconDelegate(QObject *parent = 0): QItemDelegate(parent) { }
    void paint(QPainter *painter, const QStyleOptionViewItem &option,
    const QModelIndex & index ) const
    {
        QPixmap pixmap = QPixmap("XX.bmp").scaled(24, 24);
        qApp->style()->drawItemPixmap(painter, option.rect, Qt::AlignCenter, QPixmap(pixmap));
    }
};



#endif // DEGELATE_H
