#ifndef CHECKBOXDELEGATE_H
#define CHECKBOXDELEGATE_H

#include<QStyledItemDelegate>
#include<QModelIndex>
#include<QSqlTableModel>

class CheckboxDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit CheckboxDelegate(QObject *parent = 0);
    ~CheckboxDelegate();
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    bool editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index);
    QMap<int, QStyleOptionButton *> checkBoxs() const;
    void setAllChecked(int, bool isCheck);
    void setChecked(int row,bool isCheck);

    bool isCheckFlag() const;
    void setIsCheckFlag(bool isCheckFlag);

signals:
    void checkChangeSignal(bool flag,int row);
private:
    QRect CheckboxRect(const QStyleOptionViewItem &viewItemStyleOptions)const;

public slots:

private:
    QMap<int,QStyleOptionButton*> m_checkBoxs;
    bool m_isCheckFlag;//是否可选
};

#endif // CHECKBOXDELEGATE_H
