#include "mainwindow.h"
#include <QApplication>

#include <QDebug>
#include <QFile>
#include <QDir>

#include "DB/sqlHelper.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);


    QString path = QDir::currentPath().append("/style.qss");//theme_dark_orange style


    qDebug()<<path;


    QFile file(path);

    if(file.exists())
    {
        if(file.open(QIODevice::ReadOnly))
        {

            QByteArray qss = file.readAll();
            QString paletteColor = qss.mid(20, 7);
            a.setPalette(QPalette(QColor(paletteColor)));
            a.setStyleSheet(QString(qss));

            file.close();
        }


    }

    createConn c;
    QString appPath = QCoreApplication::applicationDirPath()+"/DB.db";
    qDebug() << appPath;
    c.setDriver("QSQLITE");
    c.setDBName(appPath);
    c.openDB();

    MainWindow w;
    w.show();

    return a.exec();
}
