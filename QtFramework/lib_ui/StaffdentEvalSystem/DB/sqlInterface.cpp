#include "sqlInterface.h"
#include <QDebug>
#include <QTextCodec>

/***************************************************
 * 名称:sqlInterface
 * 功能:构造函数
 * 输入:parent对象
 * 输出:无
 * 备注:
 **************************************************/
sqlInterface::sqlInterface(QObject *parent) : QObject(parent)
{
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");//情况2
    QTextCodec::setCodecForLocale(codec);
}

/***************************************************
 * 名称:query
 * 功能:查询sql
 * 输入:sql sql语句，params 查询参数
 * 输出:QList<QVariantMap> 结果集
 * 备注:
 **************************************************/
QList<QVariantMap> sqlInterface::queryList(QString sql, QVariantList params)
{
    QList<QVariantMap> result ;
    if(!g_myDB.isOpen())
    {
        g_myDB.open();
    }

    QSqlQuery gquery(g_myDB);
    gquery.prepare(sql);
    for( int i = 0 ;i < params.size();i++)
    {
        QVariant & p = params[i];
        gquery.bindValue(i,p);
    }

    gquery.exec();

    while(gquery.next())
    {
        QSqlRecord record = gquery.record();
        int fieldCount = record.count();

        QVariantMap rowMap ;
        for(int i = 0 ; i < fieldCount ; i++)
        {
            QString fieldName = record.fieldName(i);
            QVariant fieldValue = record.value(i);
            qDebug() << fieldName << "----" << fieldValue.toString();
            rowMap.insert(fieldName,fieldValue);
        }
        result.append(rowMap);
    }
    QSqlError error = gquery.lastError();
    if(error.type() != QSqlError::NoError)
    {
        qDebug()<<error.text();
    }

    return result;
}

/***************************************************
 * 名称:queryModel
 * 功能:查询sql,返回一个QSqlQueryModel
 * 输入:sql sql语句，params 查询参数
 * 输出:QSqlQueryModel 结果集
 * 备注:
 **************************************************/
QSqlQueryModel* sqlInterface::queryModel(QString sql, QVariantList params)
{
    QSqlQuery query(g_myDB);
    QSqlQueryModel *model = new QSqlQueryModel;

    if(!g_myDB.isOpen())
    {
        qDebug() << "--------";
        g_myDB.open();
    }

    query.prepare(sql);
    for( int i = 0 ;i < params.size();i++)
    {
        QVariant & p = params[i];
        query.bindValue(i,p);
    }

    query.exec();

    while(query.next())
    {
        QSqlRecord record = query.record();
        int fieldCount = record.count();

        for(int i = 0 ; i < fieldCount ; i++)
        {
            QString fieldName = record.fieldName(i);
            model->setHeaderData(i,Qt::Horizontal,fieldName);
        }
    }

    QSqlError error = query.lastError();
    if(error.type() != QSqlError::NoError)
    {
        qDebug()<<error.text();
    }

    return model;
}

/***************************************************
 * 名称:exec
 * 功能:sql执行函数
 * 输入:sql sql语句，params 查询参数，lastInseredId 插入id
 * 输出:false 失败，true 成功
 * 备注:
 **************************************************/
bool sqlInterface::exec(QString sql, QVariantList params, int *lastInseredId)
{
    if(!g_myDB.isOpen())
    {
        g_myDB.open();
    }

    QSqlQuery query(g_myDB);
    query.prepare(sql);
    for( int i = 0 ;i < params.size();i++)
    {
        QVariant & p = params[i];
        query.bindValue(i,p);
    }

    if(query.exec())
    {
        if(lastInseredId)
        {
            *lastInseredId = query.lastInsertId().toInt();
        }
        return true;
    }else
    {
        QSqlError error = query.lastError();
        qDebug()<<error.text();
        return false;
    }
}

/***************************************************
 * 名称:querySingleResult
 * 功能:查询单个结果
 * 输入:sql sql语句，params 查询参数
 * 输出:QVariant 结果集
 * 备注:
 **************************************************/
QVariant sqlInterface::querySingleResult(QString sql, QVariantList params)
{
    qDebug() << "querySingleResult--------" << sql;
    QList<QVariantMap> result = queryList(sql,params);

    for(int i=0;i<result.size();i++)
    {
        QVariantMap map = result[i];
        qDebug() << "querySingleResult----result:" <<map.first().toString();
    }
    if(!result.isEmpty())
    {
        if(!result[0].isEmpty())
        {
            return result[0].first();
        }
    }
    return QVariant();
}

/***************************************************
 * 名称:queryOneFieldResult
 * 功能:查询单个字段结果集
 * 输入:sql sql语句，params 查询参数
 * 输出:QVariant 结果集
 * 备注:
 **************************************************/
QStringList sqlInterface::queryOneFieldResult(QString sql, QVariantList params)
{
    QList<QVariantMap> result = queryList(sql,params);

    QStringList list;
    for(int i=0;i<result.size();i++)
    {
        QVariantMap map = result[i];
        list << map.first().toString();
    }
    return list;
}
