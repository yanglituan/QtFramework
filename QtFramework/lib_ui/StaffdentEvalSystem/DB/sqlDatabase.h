#ifndef SQLDATABASE_H
#define SQLDATABASE_H

#pragma once
#include <QObject>
#include <QtSql>

extern QSqlDatabase  g_myDB;
extern QString g_driver;//驱动名称
extern QString g_userName;//数据库用户名
extern QString g_password;//数据库用户密码


/***************************************************
 * 名称:DBOpen
 * 功能:打开数据库
 * 输入:无
 * 输出:无
 * 备注:
 **************************************************/
//void DBOpen()
//{
//    if (!g_myDB.open()) {
//        qDebug() << g_myDB.lastError().text();
//        QString msg = QString("Cannot open database.\n Unable to establish a database connection.\n"
//                "This example needs %1 support. Please read "
//                "the Qt SQL driver documentation for information how "
//                "to build it.\n\n"
//                "Click Cancel to exit.").arg(g_driver);
//        qDebug() << msg;
//        qDebug() << "数据库连接失败。";
//    }
//    else{
//        qDebug() << "数据库连接成功。";
//    }
//}



//QSqlQuery getSQLQuery(){
//    return QSqlQuery(g_myDB);
//}

#endif // SQLDATABASE_H
