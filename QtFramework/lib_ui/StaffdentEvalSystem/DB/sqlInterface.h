/***************************************************
 * 类名:sqliteInterface.h
 * 功能:sqlite数据库操作接口类
 * 日期:
 * 作者:xhh
 * 备注:
 **************************************************/


#ifndef SQLDMLINTERFACE_H
#define SQLDMLINTERFACE_H

#include <QObject>
#include <QtSql>
#include "sqlDatabase.h"

class sqlInterface : public QObject
{
    Q_OBJECT
public:
    explicit sqlInterface(QObject *parent = 0);

public://sql操作接口函数
    //查询
    QList<QVariantMap> queryList(QString sql,QVariantList params = QVariantList());

    QSqlQueryModel *queryModel(QString sql, QVariantList params = QVariantList());

    //执行sql语句
    bool exec(QString sql,QVariantList params = QVariantList(),int * lastInseredId = 0);

    //单个查询
    QVariant querySingleResult(QString sql,QVariantList params = QVariantList());

    //查询单个字段结果集
    QStringList queryOneFieldResult(QString sql,QVariantList params = QVariantList());

private:
};

#endif // SQLDMLINTERFACE_H
