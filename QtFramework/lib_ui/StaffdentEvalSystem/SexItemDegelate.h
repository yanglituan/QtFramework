#ifndef SEXITEMDEGELATE_H
#define SEXITEMDEGELATE_H

#include <QObject>
#include <QStyledItemDelegate>

class SexItemDegelate : public QStyledItemDelegate
{
public:
    SexItemDegelate(QObject* p =0);

    // QAbstractItemDelegate interface
public:
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
};

#endif // SEXITEMDEGELATE_H
