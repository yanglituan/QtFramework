#include "SexItemDegelate.h"
#include <QComboBox>
SexItemDegelate::SexItemDegelate(QObject *p):QStyledItemDelegate(p)
{

}

QWidget *SexItemDegelate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{

    QComboBox * box = new QComboBox(parent);
    box->addItems(QStringList()<<"男"<<"女");

    return box;
}

void SexItemDegelate::setEditorData(QWidget *editor, const QModelIndex &index) const
{

    QComboBox * box = (QComboBox*)editor;


    if(box)
    {
        box->setCurrentText(index.data().toString());
    }

}

void SexItemDegelate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QComboBox * box = (QComboBox*)editor;

    if(box)
    {
        model->setData(index,box->currentText());
    }

}
