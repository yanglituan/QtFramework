#ifndef ALLENTITYS_H
#define ALLENTITYS_H

#include <QString>
#include <QDate>

//部门  t_department
typedef struct DepartmentInfo
{
    int     dep_id;//部门编号  （主键唯一，递增）
    QString dep_name;//部门名称（不为空）
}DepartmentInfo;

//小组  t_groupInfo
typedef struct GroupInfo
{
    int     dep_id;//所在部门编号(此表中不唯一)
    int     group_id;//小组编号（同一个部门唯一）
    QString group_name;//小组名称（不为空,不唯一）
}GroupInfo;

//成员  t_staffInfo
typedef struct StaffInfo
{
    int     group_id;//小组编号
    int     staff_idcard;//成员id（唯一）
    int     staff_sex;//成员性别
    QString staff_name;//成员名称
}StaffInfo;


#endif // ALLENTITYS_H
