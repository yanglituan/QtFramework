#ifndef EXPORTEXCEL_H
#define EXPORTEXCEL_H

#include <QObject>
#include <QAxObject>


#include "DAO/staffDao.h"
/**************************************
 * 版本：
 * 作者：
 * 日期：
 * 类用途描述：
 * ************************************/
class ExportExcel : public QObject
{
    Q_OBJECT
public:
    explicit ExportExcel(QObject *parent = nullptr);



    /**
     * @brief 导出员工信息  
     * @param fileName
     */
    bool exportStaffInfo(QString fileName);



private:

    QAxObject * sheet(QString fileName,QAxObject ** pApplication =0,QAxObject ** pWorkbook = 0);


private:

    staffDao m_stuService;



signals:

public slots:
};

#endif // EXPORTEXCEL_H
