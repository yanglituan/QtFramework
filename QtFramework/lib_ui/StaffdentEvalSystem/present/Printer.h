#ifndef Printer_H
#define Printer_H

#include <QMainWindow>
#include <QPrinter>
#include <QPainter>
#include <QPrintPreviewDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <QPrintDialog>
#include <QDebug>
#include <QTableWidget>


class Printer : public QWidget
{
    Q_OBJECT

public:
    explicit Printer(QWidget *parent = 0);
    ~Printer();
    void setTableWidget(QTableWidget *tableview);
    void print();




private slots:

    void printTable(QPrinter *printer);

private:
    void printTableWidget(QString stitile,QPrinter *printer);



private:
    QTableWidget *view;

};

#endif // Printer_H
