#include "checkboxdelegate.h"
#include<QApplication>
#include<QMouseEvent>
#include<QKeyEvent>
#include<QPainter>
#include<QStyle>
#include<QDebug>
#include<QMessageBox>

/*
 * 函数名：CheckboxDelegate
 * 函数说明: 构造函数
 *
 *
 * 输入参数：parent： 父类指针
 * 输出参数：无
 * 返回值：无
 * */
CheckboxDelegate::CheckboxDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
    m_isCheckFlag = true;
}

/*
 * 函数名：~CheckboxDelegate
 * 函数说明: 析构函数
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
CheckboxDelegate::~CheckboxDelegate()
{

}

/*
 * 函数名：paint
 * 函数说明: 绘制函数 设置表格样式，绘制复选框-状态、大小、显示区域等
 *
 *
 * 输入参数： painter：画笔 option:用于描述用于在视图小部件中绘制项的参数  index：表格索引
 * 输出参数：无
 * 返回值：无
 * */
void CheckboxDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{    
       QStyleOptionButton* checkBoxOption = m_checkBoxs.value(index.row());
        if(!checkBoxOption)
        {            
            checkBoxOption = new QStyleOptionButton();
            checkBoxOption->state |= QStyle::State_Enabled|QStyle::State_Off;
            (const_cast<CheckboxDelegate*>(this))->m_checkBoxs.insert(index.row(),checkBoxOption);
        }
        //bool  checked = index.model()->data(index,Qt::UserRole).toBool();
        //qDebug()<<checked<<index.row()<<index.column();
        //QStyleOptionButton *checkBoxOption = new QStyleOptionButton();
        //checkBoxOption->state|=QStyle::State_Enabled;
//        if(checked)
//        {
//            checkBoxOption->state|=QStyle::State_On;
//        }else {
//            checkBoxOption->state|=QStyle::State_Off;
//        }
        //checkBoxOption->rect = option.rect.adjusted(4,4,-4,-4);//左对齐
        checkBoxOption->rect = CheckboxRect(option);//居中
        painter->save();
        if(option.state&QStyle::State_Selected)
        {
            painter->fillRect(option.rect,option.palette.highlight());
            //qDebug()<<"select  checkbox";
        }
        painter->restore();
        QApplication::style()->drawControl(QStyle::CE_CheckBox,checkBoxOption,painter);

}

/*
 * 函数名：editorEvent
 * 函数说明: 设置数据，响应鼠标事件
 *
 *
 * 输入参数： event：事件  model：模型   option：样式   index：索引
 * 输出参数：无
 * 返回值：无
 * */
bool CheckboxDelegate::editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index)
{
    QMouseEvent* e = (QMouseEvent*)event;
    if(e->button()!=Qt::LeftButton||!CheckboxRect(option).contains(e->pos()))
    {
        return true;
    }
    if(event->type() == QEvent::MouseButtonPress)
    {
//        QMouseEvent* e = (QMouseEvent*)event;
        if(option.rect.adjusted(4,4,-4,-4).contains(e->x(),e->y())&&m_checkBoxs.contains(index.row())){
            m_checkBoxs.value(index.row())->state |= QStyle::State_Sunken;

            if(m_checkBoxs.value(index.row())->state == (QStyle::State_On|QStyle::State_Enabled|QStyle::State_Sunken))
            {
                  m_checkBoxs.value(index.row())->state &=(~QStyle::State_On);
                  m_checkBoxs.value(index.row())->state |=QStyle::State_Off;
                  emit checkChangeSignal(false,index.row());
            }else {

                   if(m_isCheckFlag)  {
                      m_checkBoxs.value(index.row())->state &=(~QStyle::State_Off);
                      m_checkBoxs.value(index.row())->state |=QStyle::State_On;
                      emit checkChangeSignal(true,index.row());
                   }else{
                        m_checkBoxs.value(index.row())->state &= (~QStyle::State_Sunken);
                        QMessageBox::warning(NULL, tr("查询"), tr("查询变量总数不能超过20条！"), "是","","",1);
                   }

           }
        }
    }
    if(event->type()==QEvent::MouseButtonRelease)
    {

       if(option.rect.adjusted(4,4,-4,-4).contains(e->x(),e->y())&&m_checkBoxs.contains(index.row())){
           m_checkBoxs.value(index.row())->state &= (~QStyle::State_Sunken);//sunken下沉
       }

    }


//        if((event->type()==QEvent::MouseButtonPress)||(event->type()==QEvent::MouseButtonDblClick))
//        {
//            QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
//            if(mouseEvent->button()!=Qt::LeftButton)//||!CheckboxRect(option).contains(mouseEvent->pos()))
//            {
//                return true;
//            }
//            if(!option.rect.adjusted(4,4,-4,-4).contains(mouseEvent->x(),mouseEvent->y())||!m_checkBoxs.contains(index))
//            {
//                return true;
//            }
//            if(event->type() == QEvent::MouseButtonDblClick)
//            {
//                return true;
//            }
//        }else if(event->type()==QEvent::KeyPress){
//            if(static_cast<QKeyEvent*>(event)->key()!=Qt::Key_Space&&static_cast<QKeyEvent*>(event)->key()!=Qt::Key_Select )
//            {
//                return false;
//            }
//        }else {
//            return false;
//        }
////        bool checked = index.model()->data(index).toBool();
//  //      QSqlTableModel *tablemodel = (QSqlTableModel*)index.model();
////        tablemodel->setData(index,!checked);
// //       qDebug()<<index.row()<<index.column()<<index.model()->data(index).toBool()<<tablemodel->data(index).toBool();
//        //option.state = QStyle::State_Selected;
//        return  true;
//    }else {
//        return QStyledItemDelegate::editorEvent(event,model,option,index);

}

QRect CheckboxDelegate::CheckboxRect(const QStyleOptionViewItem &viewItemStyleOptions) const
{
    //绘制按钮所需要的参数
    QStyleOptionButton checkBoxStyleOption;
    //按照给定的风格参数 返回元素子区域
    QRect checkBoxRect = QApplication::style()->subElementRect(QStyle::SE_CheckBoxIndicator,&checkBoxStyleOption);
    //返回QCheckbox坐标
    QPoint checkBoxPoint(viewItemStyleOptions.rect.x()+viewItemStyleOptions.rect.width()/2-checkBoxRect.width()/2,
                         viewItemStyleOptions.rect.y()+viewItemStyleOptions.rect.height()/2-checkBoxRect.height()/2);

    return QRect(checkBoxPoint,checkBoxRect.size());
}

/*
 * 函数名：checkBoxs
 * 函数说明: 初始化设置所有选中状态
 *
 *
 * 输入参数： 无
 * 输出参数：返回checkbox结果集合
 * 返回值：无
 * */
QMap<int, QStyleOptionButton *> CheckboxDelegate::checkBoxs() const
{
    return m_checkBoxs;
}

/*
 * 函数名：setAllChecked
 * 函数说明: 初始化设置所有选中状态
 *
 *
 * 输入参数： rowNum：总行数  isCheck：true 选中 false不选中
 * 输出参数：无
 * 返回值：无
 * */
void CheckboxDelegate::setAllChecked(int rowNum,bool isCheck)
{
    m_checkBoxs.clear();
    for (int row=0;row<rowNum;row++){
        QStyleOptionButton* checkBoxOption = m_checkBoxs.value(row);
        if(!checkBoxOption)
        {
            checkBoxOption = new QStyleOptionButton();        
            if(isCheck)
            {
                checkBoxOption->state &=(~QStyle::State_Off);
                checkBoxOption->state |=QStyle::State_Enabled|QStyle::State_On;
            }else{
                checkBoxOption->state &=(~QStyle::State_On);
                checkBoxOption->state |=QStyle::State_Enabled|QStyle::State_Off;
            }
            (const_cast<CheckboxDelegate*>(this))->m_checkBoxs.insert(row,checkBoxOption);
        }
    }
}

/*
 * 函数名：setChecked
 * 函数说明: 初始化设置每行选中状态
 *
 *
 * 输入参数： row：行号  isCheck：true 选中 false不选中
 * 输出参数：无
 * 返回值：无
 * */
void CheckboxDelegate::setChecked(int row, bool isCheck)
{
    QStyleOptionButton* checkBoxOption = m_checkBoxs.value(row);
    if(!checkBoxOption)
    {
        checkBoxOption = new QStyleOptionButton();
        if(isCheck)
        {
            checkBoxOption->state &=(~QStyle::State_Off);
            checkBoxOption->state |=QStyle::State_Enabled|QStyle::State_On;
        }else{
            checkBoxOption->state &=(~QStyle::State_On);
            checkBoxOption->state |=QStyle::State_Enabled|QStyle::State_Off;
        }
        (const_cast<CheckboxDelegate*>(this))->m_checkBoxs.insert(row,checkBoxOption);
    }

}

/*
 * 函数名：isCheckFlag
 * 函数说明: 获取是否可选中红状态
 *
 *
 * 输入参数： 无
 * 输出参数：false 不可选中 true可选中
 * 返回值：无
 * */
bool CheckboxDelegate::isCheckFlag() const
{
    return m_isCheckFlag;
}

/*
 * 函数名：setIsCheckFlag
 * 函数说明: 选中数目大于20 设置为不可选中 选中数目小于20  设置为可选中
 *
 *
 * 输入参数： isCheckFlag：true可选中 false不可选中
 * 输出参数：无
 * 返回值：无
 * */
void CheckboxDelegate::setIsCheckFlag(bool isCheckFlag)
{
    m_isCheckFlag = isCheckFlag;
}





