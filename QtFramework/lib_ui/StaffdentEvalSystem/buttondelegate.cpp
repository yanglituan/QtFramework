#include "buttondelegate.h"
#include<QApplication>
#include<QMouseEvent>
#include<QToolTip>
#include<QDebug>

int FILE_OPERATE_COLUMN = 4;//按键所在列


/*
 * 函数名：ButtonDelegate
 * 函数说明:构造函数
 *
 *
 * 输入参数：parent： 父类指针
 * 输出参数：无
 * 返回值：无
 * */
ButtonDelegate::ButtonDelegate(QWidget  *parent)
    :QStyledItemDelegate(parent),
      m_pOpenButton(new QPushButton()),
      m_pDeleteButton(new QPushButton()),
      m_nSpacing(5),
      m_nWidth(52),
      m_nHeight(30)
{
    // 设置按钮正常、划过、按下样式
    m_pDeleteButton->setStyleSheet("QPushButton {border: none; background-color: transparent; image:url(:/images/icon_Delete_52x52.png);} \
                                 QPushButton:hover {image:url(:/images/icon_DeleteHover_52x52.png);} \
                                 QPushButton:pressed {image:url(:/images/icon_Delete_52x52.png);}");
    m_list <<QStringLiteral("删除");
}


/*
 * 函数名：~ButtonDelegate
 * 函数说明:析构函数
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
ButtonDelegate::~ButtonDelegate()
{

}


/*
 * 函数名：paint
 * 函数说明: 绘制按钮
 *
 *
 * 输入参数： painter：画笔 option:用于描述用于在视图小部件中绘制项的参数  index：表格索引
 * 输出参数：无
 * 返回值：无
 * */
void ButtonDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStyleOptionViewItem viewOption(option);
    initStyleOption(&viewOption, index);
    if (option.state.testFlag(QStyle::State_HasFocus))
        viewOption.state = viewOption.state ^ QStyle::State_HasFocus;

    QStyledItemDelegate::paint(painter, viewOption, index);

    if (index.column() == FILE_OPERATE_COLUMN)
    {
        // 计算按钮显示区域
        int nCount = m_list.count();
        int nHalf = (option.rect.width() - m_nWidth * nCount - m_nSpacing * (nCount - 1)) / 2;
        int nTop = (option.rect.height() - m_nHeight) / 2;

        for (int i = 0; i < nCount; ++i)
        {
            // 绘制按钮
            QStyleOptionButton button;
            button.rect = QRect(option.rect.left() + nHalf + m_nWidth * i + m_nSpacing * i,
                                option.rect.top() + nTop,  m_nWidth, m_nHeight);
            button.state |= QStyle::State_Enabled;
            //button.iconSize = QSize(16, 16);
            //button.icon = QIcon(QString(":/Images/%1").arg(m_list.at(i)));

            if (button.rect.contains(m_mousePoint))
            {
                if (m_nType == 0)
                {
                    button.state |= QStyle::State_MouseOver;
                    //button.icon = QIcon(QString(":/Images/%1Hover").arg(m_list.at(i)));
                }
                else if (m_nType == 1)
                {
                    button.state |= QStyle::State_Sunken;
                    //button.icon = QIcon(QString(":/Images/%1Pressed").arg(m_list.at(i)));
                }
            }

            //QWidget *pWidget = (i == 0) ? m_pOpenButton.data() : m_pDeleteButton.data();
            QWidget *pWidget = m_pDeleteButton.data();
            QApplication::style()->drawControl(QStyle::CE_PushButton, &button, painter, pWidget);
        }
    }
}


/*
 * 函数名：editorEvent
 * 函数说明: 响应按钮事件 - 划过、按下
 *
 *
 * 输入参数： event：事件  model：模型   option：样式   index：索引
 * 输出参数：无
 * 返回值：无
 * */
bool ButtonDelegate::editorEvent(QEvent* event, QAbstractItemModel* model, const QStyleOptionViewItem& option, const QModelIndex& index)
{
    if (index.column() != FILE_OPERATE_COLUMN)
        return false;

    m_nType = -1;
    bool bRepaint = false;
    QMouseEvent *pEvent = static_cast<QMouseEvent *> (event);
    m_mousePoint = pEvent->pos();

    int nCount = m_list.count();
    int nHalf = (option.rect.width() - m_nWidth * nCount - m_nSpacing * (nCount - 1)) / 2;
    int nTop = (option.rect.height() - m_nHeight) / 2;

    // 还原鼠标样式
    QApplication::restoreOverrideCursor();

    for (int i = 0; i < nCount; ++i)
    {
        QStyleOptionButton button;
        button.rect = QRect(option.rect.left() + nHalf + m_nWidth * i + m_nSpacing * i,
                            option.rect.top() + nTop,  m_nWidth, m_nHeight);


        // 鼠标位于按钮之上
        if (!button.rect.contains(m_mousePoint))
            continue;

        qDebug()<<"mouse end"<<event->type();
        bRepaint = true;
        switch (event->type())
        {
        // 鼠标滑过
        case QEvent::MouseMove:
        {

            // 设置鼠标样式为手型
            QApplication::setOverrideCursor(Qt::PointingHandCursor);

            m_nType = 0;
            QToolTip::showText(pEvent->globalPos(), m_list.at(i));
            qDebug()<<"mouse hover"<<event->type();
            break;
        }
            // 鼠标按下
        case QEvent::MouseButtonPress:
        {
            m_nType = 1;
            break;
        }
            // 鼠标释放
        case QEvent::MouseButtonRelease:
        {
            if (i == 0)
            {
                emit open(index);
            }
            else
            {
                emit deleteData(index);
            }
            break;
        }
        default:
            break;
        }
    }

    return bRepaint;
}
