#-------------------------------------------------
#
# Project created by QtCreator 2018-11-09T11:09:56
#
#-------------------------------------------------

QT       += core gui sql axcontainer printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = StaffManagement_System
TEMPLATE = app

CONFIG+=  c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    Staff/StaffListDialog.cpp \
    present/ExportExcel.cpp \
    Staff/DepartmentManageDialog.cpp \
    Staff/GroupManageDialog.cpp \
    checkboxdelegate.cpp \
    present/Printer.cpp \    
    Dao/departmentDao.cpp \
    Dao/groupDao.cpp \
    Dao/staffDao.cpp \
    DB/sqldatabase.cpp \
    DB/sqlHelper.cpp \
    DB/sqlInterface.cpp \
    buttondelegate.cpp \
    checkboxheaderview.cpp \
    MultiSelectComboBox.cpp



HEADERS += \
        mainwindow.h \
    Staff/StaffListDialog.h \
    present/ExportExcel.h \
    Staff/DepartmentManageDialog.h \
    Staff/GroupManageDialog.h \
    checkboxdelegate.h \
    present/Printer.h \
    Dao/departmentDao.h \
    Dao/groupDao.h \
    Dao/staffDao.h \
    allEntitys.h \
    DB/sqlDatabase.h \
    DB/sqlHelper.h \
    DB/sqlInterface.h \
    degelate.h \
    buttondelegate.h \
    checkboxheaderview.h \
    MultiSelectComboBox.h

FORMS += \
        mainwindow.ui \
    Staff/StaffListDialog.ui \
    Staff/DepartmentManageDialog.ui \
    Staff/GroupManageDialog.ui



DESTDIR+= $${PWD}/dest

RESOURCES += \
    images.qrc

