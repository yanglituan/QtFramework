#include "mainwindow.h"
#include "ui_mainwindow.h"

/*
 * 函数名：MainWindow
 * 函数说明: 构造函数
 *
 *
 * 输入参数：parent： 父类指针
 * 输出参数：无
 * 返回值：无
 * */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_depManageDlg(0),
    m_groupManageDlg(0),
    m_stuListDlg(0)

{
    ui->setupUi(this);



    connect(ui->action_dep,SIGNAL(triggered(bool)),this,SLOT(onDepartClick()));

    connect(ui->action_group,SIGNAL(triggered(bool)),this,SLOT(onGroupClick()));


    if(!m_stuListDlg)
    {
        m_stuListDlg = new StaffListDialog(this);

    }
    ui->gridLayout->addWidget(m_stuListDlg);


}


/*
 * 函数名：~MainWindow
 * 函数说明: 析构函数
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
MainWindow::~MainWindow()
{
    delete ui;
}



/*
 * 函数名：onDepartClick
 * 函数说明:点击部门管理菜单 显示部门管理界面 槽函数
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
void MainWindow::onDepartClick()
{
    if(!m_depManageDlg)
    {
        m_depManageDlg = new DepartmentManageDialog(this);
        connect(m_depManageDlg,SIGNAL(updateData()),m_stuListDlg,SLOT(loadDeps()));


    }

    m_depManageDlg->show();
}


/*
 * 函数名：onGroupClick
 * 函数说明:点击小组管理菜单 显示小组管理界面 槽函数
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
void MainWindow::onGroupClick()
{
    if(!m_groupManageDlg)
    {
        m_groupManageDlg = new GroupManageDialog(this);
    }

    m_groupManageDlg->show();
    m_groupManageDlg->refresh();
}




