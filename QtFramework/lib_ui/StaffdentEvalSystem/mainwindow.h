#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "allEntitys.h"


#include "Staff/DepartmentManageDialog.h"
#include "Staff/GroupManageDialog.h"
#include "Staff/StaffListDialog.h"
#include "dao/departmentDao.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setUserInfo();

private:
    Ui::MainWindow *ui;


    DepartmentManageDialog * m_depManageDlg;

    GroupManageDialog * m_groupManageDlg;


    StaffListDialog * m_stuListDlg;



private slots:


    void onDepartClick();

    void onGroupClick();


};

#endif // MAINWINDOW_H
