#include "customstyleexample.h"
#include <QComboBox>
#include <QStyledItemDelegate>
#include <QTableView>
#include <QDebug>

#include "CustomMainWindow.h"
CustomStyleExample::CustomStyleExample(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	QList<QComboBox *> allComboBox = findChildren<QComboBox*>();
	Q_FOREACH(QComboBox*_c, allComboBox)
	{
		_c->setItemDelegate(new QStyledItemDelegate());
	}
}

void CustomStyleExample::on_pBtn_custom_2_clicked()
{
    CustomMainWindow *customWindow = new CustomMainWindow();
    customWindow->setTitle(ui.lineEdit_title_2->text());
    customWindow->setGeometry(ui.lineEdit_x_2->text().toInt(),ui.lineEdit_y_2->text().toInt(),ui.lineEdit_w_2->text().toInt(),ui.lineEdit_h_2->text().toInt());
    if(ui.radioButton_y->isChecked())
    {
        customWindow->setAttribute(Qt::WA_ShowModal,true);
    }
    customWindow->show();
}
