#-------------------------------------------------
#
# Project created by QtCreator 2019-04-30T14:36:34
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = widgetExample
TEMPLATE = app


SOURCES += main.cpp\
    customstyle.cpp \
    customstyleexample.cpp \
    BaseTitleBar.cpp \
    BaseWindow.cpp \
    CustomMainWindow.cpp

HEADERS  += \
    customstyle.h \
    customstyleexample.h \
    BaseTitleBar.h \
    BaseWindow.h \
    CustomMainWindow.h

FORMS    += \
    customstyleexample.ui \
    CustomMainWindow.ui

DISTFILES +=

RESOURCES += \
    resource.qrc
