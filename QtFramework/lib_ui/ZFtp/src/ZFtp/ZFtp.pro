#-------------------------------------------------
#
# Project created by QtCreator 2019-02-17T17:37:21
#
#-------------------------------------------------

QT       -= gui
QT       += network
TEMPLATE = lib

include($$PWD/../dist/QFtpLib/QFtp.pri)

DEFINES += ZFTP_LIB_MAKER

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# output
DESTDIR = $$PWD/../dist/ZFtpLib
CONFIG(debug, debug|release) {
    TARGET = ZFtpd
} else {
    TARGET = ZFtp
}

INCLUDEPATH += $$PWD/ZFtpRequest \
            $$PWD/ZFtpResponse

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += ZFtp.cpp \
    ZFtpPrivate.cpp \
    ZFtpRequest/ZFtpRequest.cpp \
    ZFtpResponse/ZFtpResponse.cpp \
    ZFtpRequest/ConnectToHostRequestPrivate.cpp \
    ZFtpRequest/ZFtpRequestPrivate.cpp \
    ZFtpRequest/LoginRequestPrivate.cpp \
    ZFtpRequest/CloseFtpRequestPrivate.cpp \
    ZFtpRequest/ListDirRequestPrivate.cpp \
    ZFtpRequest/MkdirRequestPrivate.cpp \
    ZFtpRequest/RmDirRequestPrivate.cpp \
    ZFtpRequest/DeleteFileRequestPrivate.cpp \
    ZFtpRequest/RenameFileRequestPrivate.cpp \
    ZFtpRequest/ConnectToHostRequest.cpp \
    ZFtpRequest/LoginRequest.cpp \
    ZFtpRequest/CloseFtpRequest.cpp \
    ZFtpRequest/ListDirRequest.cpp \
    ZFtpRequest/MkdirRequest.cpp \
    ZFtpRequest/RenameFileRequest.cpp \
    ZFtpRequest/RmDirRequest.cpp \
    ZFtpRequest/DeleteFileRequest.cpp \
    ZFtpRequest/CdDirRequestPrivate.cpp \
    ZFtpRequest/CdDirRequest.cpp \
    ZFtpRequest/UploadFileRequest1Private.cpp \
    ZFtpRequest/UploadFileRequest2Private.cpp \
    ZFtpRequest/UploadFileRequest2.cpp \
    ZFtpRequest/UploadFileRequest1.cpp \
    ZFtpResponse/ZFtpResponsePrivate.cpp \
    ZFtpResponse/ConnectToHostResponse.cpp \
    ZFtpResponse/ConnectToHostResponsePrivate.cpp \
    ZFtpResponse/LoginResponse.cpp \
    ZFtpResponse/LoginResponsePrivate.cpp \
    ZFtpResponse/CloseFtpResponse.cpp \
    ZFtpResponse/CloseFtpResponsePrivate.cpp \
    ZFtpResponse/ListDirResponse.cpp \
    ZFtpResponse/ListDirResponsePrivate.cpp \
    ZFtpResponse/MkDirResponse.cpp \
    ZFtpResponse/MkDirResponsePrivate.cpp \
    ZFtpResponse/RmDirResponse.cpp \
    ZFtpResponse/RmDirResponsePrivate.cpp \
    ZFtpResponse/DeleteFileResponse.cpp \
    ZFtpResponse/DeleteFileResponsePrivate.cpp \
    ZFtpResponse/CdDirResponse.cpp \
    ZFtpResponse/CdDirResponsePrivate.cpp \
    ZFtpResponse/RenameFileResponse.cpp \
    ZFtpResponse/RenameFileResponsePrivate.cpp \
    ZFtpResponse/UploadFileResponse2.cpp \
    ZFtpResponse/UploadFileResponse1.cpp \
    ZFtpResponse/UploadFileResponse1Private.cpp \
    ZFtpResponse/UploadFileResponse2Private.cpp \
    ZFtpConfig.cpp \
    ZFtpRequest/UploadFileRequest3.cpp \
    ZFtpRequest/UploadFileRequest3Private.cpp \
    ZFtpResponse/UploadFileResponse3.cpp \
    ZFtpResponse/UploadFileResponse3Private.cpp \
    ZFtpResponse/DownloadFileResponse2.cpp \
    ZFtpRequest/DownloadFileRequest2.cpp \
    ZFtpRequest/DownloadFileRequest2Private.cpp \
    ZFtpResponse/DownloadFileResponse2Private.cpp \
    ZFtpRequest/DownloadFileRequest1.cpp \
    ZFtpRequest/DownloadFileRequest1Private.cpp \
    ZFtpResponse/DownloadFileResponse1.cpp \
    ZFtpResponse/DownloadFileResponse1Private.cpp \
    ZFtpUtils.cpp

HEADERS += ZFtp.h \
    ZFtpExports.h \
    ZFtpPrivate.h \
    ZFtpRequest/ZFtpRequest.h \
    ZFtpResponse/ZFtpResponse.h \
    ZFtpRequest/ConnectToHostRequestPrivate.h \
    ZFtpRequest/ZFtpRequestPrivate.h \
    ZFtpRequest/LoginRequestPrivate.h \
    ZFtpRequest/CloseFtpRequestPrivate.h \
    ZFtpRequest/ListDirRequestPrivate.h \
    ZFtpRequest/RmDirRequestPrivate.h \
    ZFtpRequest/DeleteFileRequestPrivate.h \
    ZFtpRequest/RenameFileRequestPrivate.h \
    ZFtpRequest/ConnectToHostRequest.h \
    ZFtpRequest/LoginRequest.h \
    ZFtpRequest/CloseFtpRequest.h \
    ZFtpRequest/ListDirRequest.h \
    ZFtpRequest/RenameFileRequest.h \
    ZFtpRequest/RmDirRequest.h \
    ZFtpRequest/DeleteFileRequest.h \
    ZFtpRequest/CdDirRequestPrivate.h \
    ZFtpRequest/CdDirRequest.h \
    ZFtpRequest/UploadFileRequest1Private.h \
    ZFtpRequest/UploadFileRequest2Private.h \
    ZFtpRequest/UploadFileRequest2.h \
    ZFtpRequest/UploadFileRequest1.h \
    ZFtpGlobal.h \
    ZFtpResponse/ZFtpResponsePrivate.h \
    ZFtpResponse/ConnectToHostResponse.h \
    ZFtpResponse/ConnectToHostResponsePrivate.h \
    ZFtpResponse/LoginResponse.h \
    ZFtpResponse/LoginResponsePrivate.h \
    ZFtpResponse/CloseFtpResponse.h \
    ZFtpResponse/CloseFtpResponsePrivate.h \
    ZFtpResponse/ListDirResponse.h \
    ZFtpResponse/ListDirResponsePrivate.h \
    ZFtpResponse/MkDirResponse.h \
    ZFtpResponse/MkDirResponsePrivate.h \
    ZFtpResponse/RmDirResponse.h \
    ZFtpResponse/RmDirResponsePrivate.h \
    ZFtpResponse/DeleteFileResponse.h \
    ZFtpResponse/DeleteFileResponsePrivate.h \
    ZFtpResponse/CdDirResponse.h \
    ZFtpResponse/CdDirResponsePrivate.h \
    ZFtpResponse/RenameFileResponse.h \
    ZFtpResponse/RenameFileResponsePrivate.h \
    ZFtpResponse/UploadFileResponse1.h \
    ZFtpResponse/UploadFileResponse1Private.h \
    ZFtpResponse/UploadFileResponse2.h \
    ZFtpResponse/UploadFileResponse2Private.h \
    ZFtpRequest/MkDirRequest.h \
    ZFtpRequest/MkDirRequestPrivate.h \
    ZFtpConfig.h \
    ZFtpRequest/UploadFileRequest3.h \
    ZFtpRequest/UploadFileRequest3Private.h \
    ZFtpResponse/UploadFileResponse3.h \
    ZFtpResponse/UploadFileResponse3Private.h \
    ZFtpResponse/DownloadFileResponse2.h \
    ZFtpRequest/DownloadFileRequest2.h \
    ZFtpRequest/DownloadFileRequest2Private.h \
    ZFtpResponse/DownloadFileResponse2Private.h \
    ZFtpRequest/DownloadFileRequest1.h \
    ZFtpRequest/DownloadFileRequest1Private.h \
    ZFtpResponse/DownloadFileResponse1.h \
    ZFtpResponse/DownloadFileResponse1Private.h \
    ZFtpUtils.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
