﻿#include "ZFtpUtils.h"
#include <QTextCodec>

BEGIN_NAMESPACE_ZFTPNS

ZFtpUtils::ZFtpUtils()
{

}

QString ZFtpUtils::toTextCodecString(QTextCodec *text_codec, const QString &origin_string)
{
    // QString直接包裹指定编码的数据
    QString result_string = QString::fromLatin1(text_codec->fromUnicode(origin_string));
    return result_string;
}

QString ZFtpUtils::fromTextCodecString(QTextCodec *text_codec, const QString &origin_string)
{
    // 将包裹某编码格式数据的QString中的数据原封不动取出来，进行编码转换
    QByteArray ba;
    ba.reserve(1024); // 优化

    const QChar *str_data = origin_string.data();
    while (!str_data->isNull())
    {
        ba.append(str_data->unicode());
        ++str_data;
    }

    QString result_string = text_codec->toUnicode(ba);
    return result_string;
}

END_NAMESPACE
