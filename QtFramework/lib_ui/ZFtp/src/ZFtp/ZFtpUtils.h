﻿#ifndef ZFTPUTILS_H
#define ZFTPUTILS_H

#include "ZFtpGlobal.h"
#include <QString>
class QTextCodec;

BEGIN_NAMESPACE_ZFTPNS

class ZFtpUtils
{
public:
    ZFtpUtils();

    /**
     * @brief toCodedString
     *  将Unicode字符串QString按照指定编码格式编码后保存到QString返回
     * @param text_codec
     *  编码器
     * @param origin_string
     *  原字符串
     * @return
     */
    static QString toTextCodecString(QTextCodec *text_codec, const QString &origin_string);

    /**
     * @brief fromTextCodecString
     *  将包含指定编码的QString对象转换为包含Unicode数据的正常QString。
     * @param text_codec
     * @param origin_string
     * @return
     */
    static QString fromTextCodecString(QTextCodec *text_codec, const QString &origin_string);
};

END_NAMESPACE

#endif // ZFTPUTILS_H
