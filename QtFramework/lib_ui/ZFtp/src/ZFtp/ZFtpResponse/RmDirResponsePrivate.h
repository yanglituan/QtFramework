﻿#ifndef RMDIRRESPONSEPRIVATE_H
#define RMDIRRESPONSEPRIVATE_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpGlobal.h"
#include "ZFtpResponsePrivate.h"

class QFtp;

BEGIN_NAMESPACE_ZFTPNS

class RmDirResponse;
class RmDirResponsePrivate : public ZFtpResponsePrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC_D(q_ptr.data(), RmDirResponse)

public:
    explicit RmDirResponsePrivate(QObject *parent = 0);

public:
    virtual void listenTo(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig> cfg);
    virtual QPointer<ZFtpResponse> publicObject();

private:
    virtual void setPublicResponseObject(QPointer<ZFtpResponse> response);

private slots:
    void slotCommandFinished(int id, bool error);
    void slotFtpStateChanged(int state);

private:
    QPointer<RmDirResponse> q_ptr;
};

END_NAMESPACE

#endif // RMDIRRESPONSEPRIVATE_H
