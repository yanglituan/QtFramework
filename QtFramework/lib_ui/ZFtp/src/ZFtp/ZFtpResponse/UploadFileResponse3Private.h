﻿#ifndef UPLOADFILERESPONSE3PRIVATE_H
#define UPLOADFILERESPONSE3PRIVATE_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpGlobal.h"
#include "ZFtpResponsePrivate.h"

BEGIN_NAMESPACE_ZFTPNS

class UploadFileResponse3;
class UploadFileResponse3Private : public ZFtpResponsePrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC_D(q_ptr.data(), UploadFileResponse3)

public:
    explicit UploadFileResponse3Private(QObject *parent = 0);

public:
    virtual void listenTo(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig> cfg);
    virtual QPointer<ZFtpResponse> publicObject();

private:
    virtual void setPublicResponseObject(QPointer<ZFtpResponse> response);

private:
    QPointer<UploadFileResponse3> q_ptr;
    QSharedPointer<ZFtpResponse> m_public_object;
    QSharedPointer<QFtp> m_current_ftp;

private slots:
    void slotCommandFinished(int id, bool error);
    void slotFtpStateChanged(int state);
    void slotUploadProgress(qint64 done, qint64 total);
};

END_NAMESPACE

#endif // UPLOADFILERESPONSE3PRIVATE_H
