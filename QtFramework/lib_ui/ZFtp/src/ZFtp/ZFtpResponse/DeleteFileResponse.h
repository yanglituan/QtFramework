﻿#ifndef DELETEFILERESPONSE_H
#define DELETEFILERESPONSE_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpResponse.h"
#include "ZFtpExports.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class DeleteFileResponsePrivate;

class ZFTP_API DeleteFileResponse : public ZFtpResponse
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(DeleteFileResponse)

public:
    explicit DeleteFileResponse(QObject *parent = 0);
    virtual QString lastErrorString();

public:
    QSharedPointer<DeleteFileResponsePrivate> d_ptr;

signals:
    void sigResult(bool success);
};

END_NAMESPACE

#endif // DELETEFILERESPONSE_H
