﻿#ifndef LISTDIRRESPONSEPRIVATE_H
#define LISTDIRRESPONSEPRIVATE_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpGlobal.h"
#include "ZFtpResponsePrivate.h"
#include "qurlinfo.h"

class QFtp;

BEGIN_NAMESPACE_ZFTPNS

class ListDirResponse;
class ListDirResponsePrivate : public ZFtpResponsePrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC_D(q_ptr.data(), ListDirResponse)

public:
    explicit ListDirResponsePrivate(QObject *parent = 0);

public:
    virtual void listenTo(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig>);
    virtual QPointer<ZFtpResponse> publicObject();

private:
    virtual void setPublicResponseObject(QPointer<ZFtpResponse> response);

private slots:
    void slotCommandFinished(int id, bool error);
    void slotListInfo(QUrlInfo i);
    void slotFtpStateChanged(int state);

private:
    QPointer<ListDirResponse> q_ptr;
    QSharedPointer<ZFtpConfig> m_ftp_config;
};

END_NAMESPACE

#endif // LISTDIRRESPONSEPRIVATE_H
