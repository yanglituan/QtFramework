﻿#ifndef CDDIRRESPONSEPRIVATE_H
#define CDDIRRESPONSEPRIVATE_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpGlobal.h"
#include "ZFtpResponsePrivate.h"
#include "CdDirResponse.h"

class QFtp;

BEGIN_NAMESPACE_ZFTPNS

class CdDirResponse;
class CdDirResponsePrivate : public ZFtpResponsePrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC_D(q_ptr.data(), CdDirResponse)

public:
    explicit CdDirResponsePrivate(QObject *parent = 0);

public:
    virtual void listenTo(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig>);
    virtual QPointer<ZFtpResponse> publicObject();

private:
    virtual void setPublicResponseObject(QPointer<ZFtpResponse> response);

private:
    QPointer<CdDirResponse> q_ptr;

private slots:
    void slotCommandFinished(int id, bool error);
    void slotFtpStateChanged(int state);
};

END_NAMESPACE

#endif // CDDIRRESPONSEPRIVATE_H
