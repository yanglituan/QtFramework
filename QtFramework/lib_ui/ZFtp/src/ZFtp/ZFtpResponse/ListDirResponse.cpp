﻿#include "ListDirResponse.h"
#include "ListDirResponsePrivate.h"

BEGIN_NAMESPACE_ZFTPNS

ListDirResponse::ListDirResponse(QObject *parent) :
    ZFtpResponse(parent),
    d_ptr(new ListDirResponsePrivate)
{
    d_ptr->setPublicResponseObject(this);
}

QString ListDirResponse::lastErrorString()
{
    return d_ptr->lastErrorString();
}

END_NAMESPACE
