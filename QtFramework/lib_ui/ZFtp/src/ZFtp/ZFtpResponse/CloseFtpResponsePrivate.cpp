﻿#include "CloseFtpResponsePrivate.h"
#include "qftp.h"
#include "CloseFtpResponse.h"

BEGIN_NAMESPACE_ZFTPNS

CloseFtpResponsePrivate::CloseFtpResponsePrivate(QObject *parent) :
    ZFtpResponsePrivate(parent),
    q_ptr(NULL)
{

}

void CloseFtpResponsePrivate::listenTo(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> ftp_config)
{
    // 保证继承的信号工作
    ZFtpResponsePrivate::listenTo(ftp, ftp_config);

    if (!ftp) return;
    ftp->disconnect(this, SLOT(slotCommandFinished(int,bool)));
    connect(ftp.data(), SIGNAL(commandFinished(int,bool)), SLOT(slotCommandFinished(int,bool)));

    // qftp是先置状态再返回结果的，所以此处的槽必须QueuedConnection，其他请求直连或排队连接都行
    //connect(ftp.data(), SIGNAL(stateChanged(int)), SLOT(slotFtpStateChanged(int)), Qt::QueuedConnection);
}

void CloseFtpResponsePrivate::setPublicResponseObject(QPointer<ZFtpResponse> response)
{
    ZFtpResponsePrivate::setPublicResponseObject(response);
    q_ptr = qobject_cast<CloseFtpResponse *>(response.data());
    Q_ASSERT(q_ptr);
}

QPointer<ZFtpResponse> CloseFtpResponsePrivate::publicObject()
{
    return QPointer<ZFtpResponse>(q_ptr);
}

void CloseFtpResponsePrivate::slotCommandFinished(int id, bool error)
{
    if (this->ftpTaskID() != id) return;

    // ftp任务id置为无效
    invalidFtpTaskID();

    Q_ASSERT(q_ptr);
    emit q_ptr->sigResult(!error);
}

END_NAMESPACE
