﻿#include "UploadFileResponse1.h"
#include "UploadFileResponse1Private.h"

BEGIN_NAMESPACE_ZFTPNS

UploadFileResponse1::UploadFileResponse1(QObject *parent) :
    ZFtpResponse(parent),
    d_ptr(new UploadFileResponse1Private)
{
    d_ptr->setPublicResponseObject(this);
}

QString UploadFileResponse1::lastErrorString()
{
    return d_ptr->lastErrorString();
}

END_NAMESPACE
