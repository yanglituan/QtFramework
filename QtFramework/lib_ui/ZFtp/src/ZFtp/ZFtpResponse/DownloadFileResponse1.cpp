﻿#include "DownloadFileResponse1.h"
#include "DownloadFileResponse1Private.h"

BEGIN_NAMESPACE_ZFTPNS

DownloadFileResponse1::DownloadFileResponse1(QObject *parent) :
    ZFtpResponse(parent),
    d_ptr(new DownloadFileResponse1Private)
{
    d_ptr->setPublicResponseObject(this);
}

QString DownloadFileResponse1::lastErrorString()
{
    return d_ptr->lastErrorString();
}

END_NAMESPACE
