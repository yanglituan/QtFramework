﻿#ifndef CLOSEFTPRESPONSE_H
#define CLOSEFTPRESPONSE_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpResponse.h"
#include "ZFtpExports.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class CloseFtpResponsePrivate;

class ZFTP_API CloseFtpResponse : public ZFtpResponse
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(CloseFtpResponse)

public:
    explicit CloseFtpResponse(QObject *parent = 0);
    virtual QString lastErrorString();

public:
    QSharedPointer<CloseFtpResponsePrivate> d_ptr;

signals:
    void sigResult(bool suceess);
};

END_NAMESPACE

#endif // CLOSEFTPRESPONSE_H
