﻿#include "RmDirResponse.h"
#include "RmDirResponsePrivate.h"

BEGIN_NAMESPACE_ZFTPNS

RmDirResponse::RmDirResponse(QObject *parent) : ZFtpResponse(parent),
    d_ptr(new RmDirResponsePrivate)
{
    d_ptr->setPublicResponseObject(this);
}

QString RmDirResponse::lastErrorString()
{
    return d_ptr->lastErrorString();
}

END_NAMESPACE
