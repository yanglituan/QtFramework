﻿#ifndef LISTDIRRESPONSE_H
#define LISTDIRRESPONSE_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpResponse.h"
#include "ZFtpExports.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class ListDirResponsePrivate;

class ZFTP_API ListDirResponse : public ZFtpResponse
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(ListDirResponse)

public:
    explicit ListDirResponse(QObject *parent = 0);
    virtual QString lastErrorString();

public:
    QSharedPointer<ListDirResponsePrivate> d_ptr;

signals:
    void sigListInfo(QUrlInfo);
    void sigResult(bool success);
};

END_NAMESPACE

#endif // LISTDIRRESPONSE_H
