﻿#include "LoginResponse.h"
#include "LoginResponsePrivate.h"

BEGIN_NAMESPACE_ZFTPNS

LoginResponse::LoginResponse(QObject *parent) : ZFtpResponse(parent),
    d_ptr(new LoginResponsePrivate)
{
    d_ptr->setPublicResponseObject(this);
}

QString LoginResponse::lastErrorString()
{
    return d_ptr->lastErrorString();
}

END_NAMESPACE
