﻿#include "LoginResponsePrivate.h"
#include "qftp.h"
#include "LoginResponse.h"

BEGIN_NAMESPACE_ZFTPNS

LoginResponsePrivate::LoginResponsePrivate(QObject *parent) :
    ZFtpResponsePrivate(parent),
    q_ptr(NULL)
{

}

void LoginResponsePrivate::listenTo(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> ftp_config)
{
    ZFtpResponsePrivate::listenTo(ftp, ftp_config);

    if (!ftp) return;

    ftp->disconnect(this, SLOT(slotCommandFinished(int,bool)));
    ftp->disconnect(this, SLOT(slotFtpStateChanged(int)));

    connect(ftp.data(), SIGNAL(commandFinished(int,bool)), SLOT(slotCommandFinished(int,bool)));
    connect(ftp.data(), SIGNAL(stateChanged(int)), SLOT(slotFtpStateChanged(int)));
}

void LoginResponsePrivate::setPublicResponseObject(QPointer<ZFtpResponse> response)
{
    ZFtpResponsePrivate::setPublicResponseObject(response);
    q_ptr = qobject_cast<LoginResponse *>(response.data());
    Q_ASSERT(q_ptr);
}

QPointer<ZFtpResponse> LoginResponsePrivate::publicObject()
{
    return QPointer<ZFtpResponse>(q_ptr);
}

void LoginResponsePrivate::slotCommandFinished(int id, bool error)
{
    if (this->ftpTaskID() != id) return;
    invalidFtpTaskID();

    Q_ASSERT(q_ptr);
    emit q_ptr->sigResult(!error);
}

void LoginResponsePrivate::slotFtpStateChanged(int state)
{
}

END_NAMESPACE
