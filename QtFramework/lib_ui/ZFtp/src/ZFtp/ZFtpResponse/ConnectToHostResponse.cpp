﻿#include "ConnectToHostResponse.h"
#include "ConnectToHostResponsePrivate.h"

BEGIN_NAMESPACE_ZFTPNS

ConnectToHostResponse::ConnectToHostResponse(QObject *parent) :
    ZFtpResponse(parent),
    d_ptr(new ConnectToHostResponsePrivate)
{
    d_ptr->setPublicResponseObject(this);
}

QString ConnectToHostResponse::lastErrorString()
{
    return d_ptr->lastErrorString();
}

END_NAMESPACE
