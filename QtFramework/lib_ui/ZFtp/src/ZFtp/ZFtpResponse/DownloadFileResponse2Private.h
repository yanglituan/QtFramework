﻿#ifndef DownloadFileResponse2Private_H
#define DownloadFileResponse2Private_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpGlobal.h"
#include "ZFtpResponsePrivate.h"

BEGIN_NAMESPACE_ZFTPNS

class DownloadFileResponse2;
class DownloadFileResponse2Private : public ZFtpResponsePrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC_D(q_ptr.data(), DownloadFileResponse2)

public:
    explicit DownloadFileResponse2Private(QObject *parent = 0);

public:
    virtual void listenTo(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig>);
    virtual QPointer<ZFtpResponse> publicObject();

private:
    virtual void setPublicResponseObject(QPointer<ZFtpResponse> response);

private slots:
    void slotDownloadFileProgress(qint64 done, qint64 total);
    void slotCommandFinished(int id, bool error);
    void slotFtpStateChanged(int state);

private:
    QPointer<DownloadFileResponse2> q_ptr;
    QSharedPointer<QFtp> m_current_ftp;
};

END_NAMESPACE

#endif // DownloadFileResponse2Private_H
