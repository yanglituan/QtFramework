﻿#include "RenameFileResponsePrivate.h"
#include "qftp.h"
#include "RenameFileResponse.h"

BEGIN_NAMESPACE_ZFTPNS

RenameFileResponsePrivate::RenameFileResponsePrivate(QObject *parent) :
    ZFtpResponsePrivate(parent),
    q_ptr(NULL)
{

}

void RenameFileResponsePrivate::listenTo(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg)
{
    ZFtpResponsePrivate::listenTo(ftp, cfg);

    if (!ftp) return;
    ftp->disconnect(this, SLOT(slotCommandFinished(int,bool)));
    ftp->disconnect(this, SLOT(slotFtpStateChanged(int)));

    connect(ftp.data(), SIGNAL(commandFinished(int,bool)), SLOT(slotCommandFinished(int,bool)));
    connect(ftp.data(), SIGNAL(stateChanged(int)), SLOT(slotFtpStateChanged(int)));
}

void RenameFileResponsePrivate::setPublicResponseObject(QPointer<ZFtpResponse> response)
{
    ZFtpResponsePrivate::setPublicResponseObject(response);
    q_ptr = qobject_cast<RenameFileResponse *>(response.data());
    Q_ASSERT(q_ptr);
}

QPointer<ZFtpResponse> RenameFileResponsePrivate::publicObject()
{
    return QPointer<ZFtpResponse>(q_ptr);
}

void RenameFileResponsePrivate::slotCommandFinished(int id, bool error)
{
    if (this->ftpTaskID() != id) return;
    invalidFtpTaskID();

    Q_ASSERT(q_ptr);
    emit q_ptr->sigResult(!error);
}

void RenameFileResponsePrivate::slotFtpStateChanged(int state)
{
}

END_NAMESPACE
