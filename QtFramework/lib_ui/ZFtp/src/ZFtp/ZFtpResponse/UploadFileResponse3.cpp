﻿#include "UploadFileResponse3.h"
#include "UploadFileResponse3Private.h"

BEGIN_NAMESPACE_ZFTPNS

UploadFileResponse3::UploadFileResponse3(QObject *parent) :
    ZFtpResponse(parent),
    d_ptr(new UploadFileResponse3Private)
{
    d_ptr->setPublicResponseObject(this);
}

QString UploadFileResponse3::lastErrorString()
{
    return d_ptr->lastErrorString();
}

END_NAMESPACE
