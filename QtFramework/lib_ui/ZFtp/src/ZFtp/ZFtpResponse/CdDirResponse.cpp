﻿#include "CdDirResponse.h"
#include "CdDirResponsePrivate.h"

BEGIN_NAMESPACE_ZFTPNS

CdDirResponse::CdDirResponse(QObject *parent) : ZFtpResponse(parent),
    d_ptr(new CdDirResponsePrivate)
{
    d_ptr->setPublicResponseObject(this);
}

QString CdDirResponse::lastErrorString()
{
    return d_ptr->lastErrorString();
}

END_NAMESPACE
