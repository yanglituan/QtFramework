﻿#ifndef CONNECTTOHOSTRESPONSEPRIVATE_H
#define CONNECTTOHOSTRESPONSEPRIVATE_H

#include <QObject>
#include "QSharedPointer"
#include "ZFtpGlobal.h"
#include "ZFtpResponsePrivate.h"

class QFtp;

BEGIN_NAMESPACE_ZFTPNS

class ConnectToHostResponse;
class ConnectToHostResponsePrivate : public ZFtpResponsePrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC_D(q_ptr.data(), ConnectToHostResponse)

public:
    explicit ConnectToHostResponsePrivate(QObject *parent = 0);

public:
    virtual void listenTo(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig>);
    virtual QPointer<ZFtpResponse> publicObject();
    virtual void setFtpTaskID(const int &id);

private:
    virtual void setPublicResponseObject(QPointer<ZFtpResponse> response);

private slots:
    void slotCommandFinished(int id, bool error);
    void slotFtpStateChanged(int state);

private:
    QPointer<ConnectToHostResponse> q_ptr;
};

END_NAMESPACE

#endif // CONNECTTOHOSTRESPONSEPRIVATE_H
