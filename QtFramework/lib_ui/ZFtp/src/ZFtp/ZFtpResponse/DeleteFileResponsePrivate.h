﻿#ifndef DELETEFILERESPONSEPRIVATE_H
#define DELETEFILERESPONSEPRIVATE_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpGlobal.h"
#include "ZFtpResponsePrivate.h"

class QFtp;

BEGIN_NAMESPACE_ZFTPNS

class DeleteFileResponse;

class DeleteFileResponsePrivate : public ZFtpResponsePrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC_D(q_ptr.data(), DeleteFileResponse)

public:
    explicit DeleteFileResponsePrivate(QObject *parent = 0);

public:
    virtual void listenTo(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig>);
    virtual QPointer<ZFtpResponse> publicObject();

private:
    virtual void setPublicResponseObject(QPointer<ZFtpResponse> response);

private slots:
    void slotCommandFinished(int id, bool error);
    void slotFtpStateChanged(int state);

private:
    QPointer<DeleteFileResponse> q_ptr;
};

END_NAMESPACE

#endif // DELETEFILERESPONSEPRIVATE_H
