﻿#include "MkDirResponse.h"
#include "MkDirResponsePrivate.h"

BEGIN_NAMESPACE_ZFTPNS

MkDirResponse::MkDirResponse(QObject *parent) :
    ZFtpResponse(parent),
    d_ptr(new MkDirResponsePrivate)
{
    d_ptr->setPublicResponseObject(this);
}

QString MkDirResponse::lastErrorString()
{
    return d_ptr->lastErrorString();
}

END_NAMESPACE
