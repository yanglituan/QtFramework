﻿#ifndef ZFTPRESPONSEPRIVATE_H
#define ZFTPRESPONSEPRIVATE_H

#include <QObject>
#include <QVariantHash>
#include <QPointer>
#include <QSharedPointer>
#include "ZFtpGlobal.h"
#include "ZFtpResponsePrivate.h"
#include "ZFtpConfig.h"

class QFtp;

BEGIN_NAMESPACE_ZFTPNS

class ZFtpResponse;
class ZFtpResponsePrivate : public QObject
{
    Q_OBJECT

public:
    enum
    {
        InvalidTaskID = -1
    };

    explicit ZFtpResponsePrivate(QObject *parent = 0);

public:
    /**
     * @brief listenTo
     *  监听指定Ftp，响应对象自动做出响应
     */
    virtual void listenTo(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig>);

    /**
     * @brief publicObject
     * @return
     */
    virtual QPointer<ZFtpResponse> publicObject() = 0;

    /**
     * @brief user_data
     *  用户数据
     */
    QVariantHash user_data;

    /**
     * @brief setFtpTaskID
     *  设置任务id，子类不可访问
     * @param id
     */
    virtual void setFtpTaskID(const int &id);
    int ftpTaskID();

    /**
     * @brief invalidFtpTaskID
     *  将任务id置为无效
     */
    void invalidFtpTaskID();

    /**
     * @brief isFtpTaskIDValid
     *  判断ftp任务是否有效
     * @return
     *  有效：true，无效：false
     */
    bool isFtpTaskIDValid();

    /**
     * @brief setLastErrorString
     *  设置错误信息
     * @param error_string
     */
    void setLastErrorString(const QString &error_string);

    /**
     * @brief lastErrorString
     *  获取错误信息
     * @return
     */
    QString lastErrorString();

protected:
    /**
     * @brief publicObject
     *  关联公有的Response对象，必须调用此函数来设置
     * @return
     */
    virtual void setPublicResponseObject(QPointer<ZFtpResponse>);

private:
    int m_task_id;
    QString m_last_error_string;
    QSharedPointer<QFtp> m_current_ftp;

private:
    void init();
    void createConnects();

private slots:
    void slotFtpCommandStarted(int id);
    void slotFtpCommandFinished(int id, bool error);

signals:
    void sigRequestQueued();
    void sigRequestStarted();
    void sigRequestFinished(bool);
};

END_NAMESPACE

#include "ConnectToHostResponsePrivate.h"
#include "LoginResponsePrivate.h"
#include "CloseFtpResponsePrivate.h"
#include "RmDirResponsePrivate.h"
#include "MkDirResponsePrivate.h"
#include "CdDirResponsePrivate.h"
#include "DeleteFileResponsePrivate.h"
#include "ListDirResponsePrivate.h"
#include "RenameFileResponsePrivate.h"
#include "UploadFileResponse1Private.h"
#include "UploadFileResponse2Private.h"
#include "UploadFileResponse3Private.h"
#include "DownloadFileResponse1Private.h"
#include "DownloadFileResponse2Private.h"

#endif // ZFTPRESPONSEPRIVATE_H
