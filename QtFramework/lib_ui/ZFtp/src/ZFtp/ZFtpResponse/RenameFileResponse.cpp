﻿#include "RenameFileResponse.h"
#include "RenameFileResponsePrivate.h"

BEGIN_NAMESPACE_ZFTPNS

RenameFileResponse::RenameFileResponse(QObject *parent) :
    ZFtpResponse(parent),
    d_ptr(new RenameFileResponsePrivate)
{
    d_ptr->setPublicResponseObject(this);
}

QString RenameFileResponse::lastErrorString()
{
    return d_ptr->lastErrorString();
}

END_NAMESPACE
