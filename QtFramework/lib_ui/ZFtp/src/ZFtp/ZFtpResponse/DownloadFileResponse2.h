﻿#ifndef DOWNLOADFILERESPONSE2_H
#define DOWNLOADFILERESPONSE2_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpExports.h"
#include "ZFtpGlobal.h"
#include "ZFtpResponse.h"

BEGIN_NAMESPACE_ZFTPNS

class DownloadFileResponse2Private;

class ZFTP_API DownloadFileResponse2 : public ZFtpResponse
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(DownloadFileResponse2)

public:
    explicit DownloadFileResponse2(QObject *parent = 0);
    virtual QString lastErrorString();

public:
    QSharedPointer<DownloadFileResponse2Private> d_ptr;

signals:
    void sigDownloadProgress(qint64 done, qint64 total);
    void sigResult(bool success);
};

END_NAMESPACE

#endif // DOWNLOADFILERESPONSE2_H
