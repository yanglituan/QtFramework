﻿#include "CloseFtpResponse.h"
#include "CloseFtpResponsePrivate.h"

BEGIN_NAMESPACE_ZFTPNS

CloseFtpResponse::CloseFtpResponse(QObject *parent) : ZFtpResponse(parent),
    d_ptr(new CloseFtpResponsePrivate)
{
    d_ptr->setPublicResponseObject(this);
}

QString CloseFtpResponse::lastErrorString()
{
    return d_ptr->lastErrorString();
}

END_NAMESPACE
