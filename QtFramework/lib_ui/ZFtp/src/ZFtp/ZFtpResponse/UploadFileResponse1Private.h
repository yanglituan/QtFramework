﻿#ifndef UPLOADFILERESPONSE1PRIVATE_H
#define UPLOADFILERESPONSE1PRIVATE_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpGlobal.h"
#include "ZFtpResponsePrivate.h"

BEGIN_NAMESPACE_ZFTPNS

class UploadFileResponse1;
class UploadFileResponse1Private : public ZFtpResponsePrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC_D(q_ptr.data(), UploadFileResponse1)

public:
    explicit UploadFileResponse1Private(QObject *parent = 0);
    virtual void listenTo(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig> cfg);
    virtual QPointer<ZFtpResponse> publicObject();

private:
    virtual void setPublicResponseObject(QPointer<ZFtpResponse> response);

private:
    QPointer<UploadFileResponse1> q_ptr;
    QSharedPointer<QFtp> m_current_ftp;

private slots:
    void slotUploadFileProgress(qint64 done, qint64 total);
    void slotCommandFinished(int id, bool error);
    void slotFtpStateChanged(int state);
};

END_NAMESPACE

#endif // UPLOADFILERESPONSE1PRIVATE_H
