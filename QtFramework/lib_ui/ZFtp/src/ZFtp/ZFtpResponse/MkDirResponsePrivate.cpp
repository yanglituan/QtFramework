﻿#include "MkDirResponsePrivate.h"
#include "qftp.h"
#include "MkDirResponse.h"

BEGIN_NAMESPACE_ZFTPNS

MkDirResponsePrivate::MkDirResponsePrivate(QObject *parent) :
    ZFtpResponsePrivate(parent),
    q_ptr(NULL)
{

}

void MkDirResponsePrivate::listenTo(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg)
{
    ZFtpResponsePrivate::listenTo(ftp, cfg);

    if (!ftp) return;
    ftp->disconnect(this, SLOT(slotCommandFinished(int,bool)));
    ftp->disconnect(this, SLOT(slotFtpStateChanged(int)));

    connect(ftp.data(), SIGNAL(commandFinished(int,bool)), SLOT(slotCommandFinished(int,bool)));
    connect(ftp.data(), SIGNAL(stateChanged(int)), SLOT(slotFtpStateChanged(int)));
}

void MkDirResponsePrivate::setPublicResponseObject(QPointer<ZFtpResponse> response)
{
    ZFtpResponsePrivate::setPublicResponseObject(response);
    q_ptr = qobject_cast<MkDirResponse *>(response.data());
    Q_ASSERT(q_ptr);
}

QPointer<ZFtpResponse> MkDirResponsePrivate::publicObject()
{
    return QPointer<ZFtpResponse>(q_ptr);
}

void MkDirResponsePrivate::slotCommandFinished(int id, bool error)
{
    if (this->ftpTaskID() != id) return;
    invalidFtpTaskID();

    Q_ASSERT(q_ptr);
    emit q_ptr->sigResult(!error);
}

void MkDirResponsePrivate::slotFtpStateChanged(int state)
{
}

END_NAMESPACE
