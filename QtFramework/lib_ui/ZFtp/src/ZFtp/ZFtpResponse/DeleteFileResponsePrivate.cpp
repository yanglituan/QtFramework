﻿#include "DeleteFileResponsePrivate.h"
#include "qftp.h"
#include "DeleteFileResponse.h"

BEGIN_NAMESPACE_ZFTPNS

DeleteFileResponsePrivate::DeleteFileResponsePrivate(QObject *parent) :
    ZFtpResponsePrivate(parent),
    q_ptr(NULL)
{

}

void DeleteFileResponsePrivate::listenTo(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> ftp_config)
{
    // 保证继承的信号工作
    ZFtpResponsePrivate::listenTo(ftp, ftp_config);

    if (!ftp) return;
    ftp->disconnect(this, SLOT(slotCommandFinished(int,bool)));
    ftp->disconnect(this, SLOT(slotFtpStateChanged(int)));
    connect(ftp.data(), SIGNAL(commandFinished(int,bool)), SLOT(slotCommandFinished(int,bool)));
    connect(ftp.data(), SIGNAL(stateChanged(int)), SLOT(slotFtpStateChanged(int)));
}

void DeleteFileResponsePrivate::setPublicResponseObject(QPointer<ZFtpResponse> response)
{
    ZFtpResponsePrivate::setPublicResponseObject(response);
    q_ptr = qobject_cast<DeleteFileResponse *>(response.data());
    Q_ASSERT(q_ptr);
}

QPointer<ZFtpResponse> DeleteFileResponsePrivate::publicObject()
{
    return QPointer<ZFtpResponse>(q_ptr);
}

void DeleteFileResponsePrivate::slotCommandFinished(int id, bool error)
{
    if (this->ftpTaskID() != id) return;
    invalidFtpTaskID();

    Q_ASSERT(q_ptr);
    emit q_ptr->sigResult(!error);
}

void DeleteFileResponsePrivate::slotFtpStateChanged(int state)
{
}

END_NAMESPACE
