﻿#ifndef LOGINRESPONSEPRIVATE_H
#define LOGINRESPONSEPRIVATE_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpGlobal.h"
#include "ZFtpResponsePrivate.h"

class QFtp;

BEGIN_NAMESPACE_ZFTPNS

class LoginResponse;
class LoginResponsePrivate : public ZFtpResponsePrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC_D(q_ptr.data(), LoginResponse)

public:
    explicit LoginResponsePrivate(QObject *parent = 0);

public:
    virtual void listenTo(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig> cfg);
    virtual QPointer<ZFtpResponse> publicObject();

private:
    virtual void setPublicResponseObject(QPointer<ZFtpResponse> response);

private slots:
    void slotCommandFinished(int id, bool error);
    void slotFtpStateChanged(int state);

private:
    QPointer<LoginResponse> q_ptr;
};

END_NAMESPACE

#endif // LOGINRESPONSEPRIVATE_H
