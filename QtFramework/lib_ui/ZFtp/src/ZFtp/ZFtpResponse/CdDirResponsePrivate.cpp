﻿#include "CdDirResponsePrivate.h"
#include "qftp.h"

BEGIN_NAMESPACE_ZFTPNS

CdDirResponsePrivate::CdDirResponsePrivate(QObject *parent) :
    ZFtpResponsePrivate(parent),
    q_ptr(NULL)
{

}

void CdDirResponsePrivate::listenTo(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> ftp_config)
{
    // 保证继承的信号工作
    ZFtpResponsePrivate::listenTo(ftp, ftp_config);

    if (!ftp) return;
    ftp->disconnect(this, SLOT(slotCommandFinished(int,bool)));
    ftp->disconnect(this, SLOT(slotFtpStateChanged(int)));
    connect(ftp.data(), SIGNAL(commandFinished(int,bool)), SLOT(slotCommandFinished(int,bool)));
    connect(ftp.data(), SIGNAL(stateChanged(int)), SLOT(slotFtpStateChanged(int)));
}

void CdDirResponsePrivate::setPublicResponseObject(QPointer<ZFtpResponse> response)
{
    ZFtpResponsePrivate::setPublicResponseObject(response);
    q_ptr = qobject_cast<CdDirResponse *>(response.data());
    Q_ASSERT(q_ptr);
}

QPointer<ZFtpResponse> CdDirResponsePrivate::publicObject()
{
    return QPointer<ZFtpResponse>(q_ptr.data());
}

void CdDirResponsePrivate::slotCommandFinished(int id, bool error)
{
    if (this->ftpTaskID() != id) return;

    // 任务id置无效
    invalidFtpTaskID();

    Q_ASSERT(q_ptr);
    emit q_ptr->sigResult(!error);
}

void CdDirResponsePrivate::slotFtpStateChanged(int state)
{
//    if (state == QFtp::Unconnected)
//    {
//        if (isFtpTaskIDValid())
//        {
//            invalidFtpTaskID();

//            Q_ASSERT(q_ptr);
//            emit q_ptr->sigResult(false);
//        }
//    }
}

END_NAMESPACE
