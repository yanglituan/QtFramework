﻿#ifndef UPLOADFILERESPONSE2PRIVATE_H
#define UPLOADFILERESPONSE2PRIVATE_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpGlobal.h"
#include "ZFtpResponsePrivate.h"

BEGIN_NAMESPACE_ZFTPNS

class UploadFileResponse2;
class UploadFileResponse2Private : public ZFtpResponsePrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC_D(q_ptr.data(), UploadFileResponse2)

public:
    explicit UploadFileResponse2Private(QObject *parent = 0);

public:
    virtual void listenTo(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig> cfg);
    virtual QPointer<ZFtpResponse> publicObject();

private:
    virtual void setPublicResponseObject(QPointer<ZFtpResponse> response);

private:
    QPointer<UploadFileResponse2> q_ptr;
    QSharedPointer<QFtp> m_current_ftp;

private slots:
    void slotCommandFinished(int id, bool error);
    void slotFtpStateChanged(int state);
    void slotUploadProgress(qint64, qint64);
};

END_NAMESPACE

#endif // UPLOADFILERESPONSE2PRIVATE_H
