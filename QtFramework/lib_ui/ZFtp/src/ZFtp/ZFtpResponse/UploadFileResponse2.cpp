﻿#include "UploadFileResponse2.h"
#include "UploadFileResponse2Private.h"

BEGIN_NAMESPACE_ZFTPNS

UploadFileResponse2::UploadFileResponse2(QObject *parent) :
    ZFtpResponse(parent),
    d_ptr(new UploadFileResponse2Private)
{
    d_ptr->setPublicResponseObject(this);
}

QString UploadFileResponse2::lastErrorString()
{
    return d_ptr->lastErrorString();
}

END_NAMESPACE
