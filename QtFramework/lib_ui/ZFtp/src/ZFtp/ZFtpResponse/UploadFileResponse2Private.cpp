﻿#include "UploadFileResponse2Private.h"
#include "qftp.h"
#include "ZFtpResponse.h"

BEGIN_NAMESPACE_ZFTPNS

UploadFileResponse2Private::UploadFileResponse2Private(QObject *parent) :
    ZFtpResponsePrivate(parent),
    q_ptr(NULL)
{

}

void UploadFileResponse2Private::listenTo(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg)
{
    ZFtpResponsePrivate::listenTo(ftp, cfg);

    if (!ftp) return;

    m_current_ftp = ftp;

    ftp->disconnect(this, SLOT(slotCommandFinished(int,bool)));
    ftp->disconnect(this, SLOT(slotFtpStateChanged(int)));
    ftp->disconnect(this, SLOT(slotUploadProgress(qint64,qint64)));

    connect(ftp.data(), SIGNAL(commandFinished(int,bool)), SLOT(slotCommandFinished(int,bool)));
    connect(ftp.data(), SIGNAL(dataTransferProgress(qint64,qint64)), SLOT(slotUploadProgress(qint64,qint64)));
    connect(ftp.data(), SIGNAL(stateChanged(int)), SLOT(slotFtpStateChanged(int)));
}

void UploadFileResponse2Private::setPublicResponseObject(QPointer<ZFtpResponse> response)
{
    ZFtpResponsePrivate::setPublicResponseObject(response);
    q_ptr = qobject_cast<UploadFileResponse2 *>(response.data());
    Q_ASSERT(q_ptr);
}

QPointer<ZFtpResponse> UploadFileResponse2Private::publicObject()
{
    return QPointer<ZFtpResponse>(q_ptr);
}

void UploadFileResponse2Private::slotCommandFinished(int id, bool error)
{
    if (id != ftpTaskID()) return;
    invalidFtpTaskID();

    Q_ASSERT(q_ptr);
    emit q_ptr->sigResult(!error);
}

void UploadFileResponse2Private::slotFtpStateChanged(int state)
{
}

void UploadFileResponse2Private::slotUploadProgress(qint64 done, qint64 total)
{
    Q_ASSERT(m_current_ftp);
    if (m_current_ftp->currentId() != this->ftpTaskID())
        return;

    emit q_ptr->sigUploadProgress(done, total);
}

END_NAMESPACE
