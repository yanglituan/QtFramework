﻿#include "DownloadFileResponse2.h"
#include "DownloadFileResponse2Private.h"

BEGIN_NAMESPACE_ZFTPNS

DownloadFileResponse2::DownloadFileResponse2(QObject *parent) :
    ZFtpResponse(parent),
    d_ptr(new DownloadFileResponse2Private)
{
    d_ptr->setPublicResponseObject(this);
}

QString DownloadFileResponse2::lastErrorString()
{
    return d_ptr->lastErrorString();
}

END_NAMESPACE
