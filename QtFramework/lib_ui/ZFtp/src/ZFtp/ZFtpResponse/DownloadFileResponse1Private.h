﻿#ifndef DOWNLOADFILERESPONSE1PRIVATE_H
#define DOWNLOADFILERESPONSE1PRIVATE_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpGlobal.h"
#include "ZFtpResponsePrivate.h"

BEGIN_NAMESPACE_ZFTPNS

class DownloadFileResponse1;
class DownloadFileResponse1Private : public ZFtpResponsePrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC_D(q_ptr.data(), DownloadFileResponse1)

public:
    explicit DownloadFileResponse1Private(QObject *parent = 0);

public:
    virtual void listenTo(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig>);
    virtual QPointer<ZFtpResponse> publicObject();

private:
    virtual void setPublicResponseObject(QPointer<ZFtpResponse> response);

private slots:
    void slotDownloadFileProgress(qint64 done, qint64 total);
    void slotCommandFinished(int id, bool error);
    void slotFtpStateChanged(int state);

private:
    QPointer<DownloadFileResponse1> q_ptr;
    QSharedPointer<QFtp> m_current_ftp;
};

END_NAMESPACE

#endif // DOWNLOADFILERESPONSE1PRIVATE_H
