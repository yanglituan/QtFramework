﻿#include "RmDirResponsePrivate.h"
#include "qftp.h"
#include "RmDirResponse.h"

BEGIN_NAMESPACE_ZFTPNS

RmDirResponsePrivate::RmDirResponsePrivate(QObject *parent) :
    ZFtpResponsePrivate(parent),
    q_ptr(NULL)
{

}

void RmDirResponsePrivate::listenTo(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg)
{
    ZFtpResponsePrivate::listenTo(ftp, cfg);

    if (!ftp) return;
    ftp->disconnect(this, SLOT(slotCommandFinished(int,bool)));
    ftp->disconnect(this, SLOT(slotFtpStateChanged(int)));

    connect(ftp.data(), SIGNAL(commandFinished(int,bool)), SLOT(slotCommandFinished(int,bool)));
    connect(ftp.data(), SIGNAL(stateChanged(int)), SLOT(slotFtpStateChanged(int)));
}

void RmDirResponsePrivate::setPublicResponseObject(QPointer<ZFtpResponse> response)
{
    ZFtpResponsePrivate::setPublicResponseObject(response);
    q_ptr = qobject_cast<RmDirResponse *>(response.data());
    Q_ASSERT(q_ptr);
}

QPointer<ZFtpResponse> RmDirResponsePrivate::publicObject()
{
    return QPointer<ZFtpResponse>(q_ptr);
}

void RmDirResponsePrivate::slotCommandFinished(int id, bool error)
{
    if (this->ftpTaskID() != id) return;
    invalidFtpTaskID();

    Q_ASSERT(q_ptr);
    emit q_ptr->sigResult(!error);
}

void RmDirResponsePrivate::slotFtpStateChanged(int state)
{
}

END_NAMESPACE
