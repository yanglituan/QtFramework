﻿#ifndef RMDIRRESPONSE_H
#define RMDIRRESPONSE_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpResponse.h"
#include "ZFtpExports.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class RmDirResponsePrivate;

class ZFTP_API RmDirResponse : public ZFtpResponse
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(RmDirResponse)

public:
    explicit RmDirResponse(QObject *parent = 0);
    virtual QString lastErrorString();

public:
    QSharedPointer<RmDirResponsePrivate> d_ptr;

signals:
    void sigResult(bool success);
};

END_NAMESPACE

#endif // RMDIRRESPONSE_H
