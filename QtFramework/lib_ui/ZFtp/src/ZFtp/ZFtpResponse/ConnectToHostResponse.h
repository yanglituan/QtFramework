﻿#ifndef CONNECTTOHOSTRESPONSE_H
#define CONNECTTOHOSTRESPONSE_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpResponse.h"
#include "ZFtpExports.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class ConnectToHostResponsePrivate;

class ZFTP_API ConnectToHostResponse : public ZFtpResponse
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(ConnectToHostResponse)

public:
    explicit ConnectToHostResponse(QObject *parent = 0);
    virtual QString lastErrorString();

public:
    QSharedPointer<ConnectToHostResponsePrivate> d_ptr;

signals:
    /**
     * @brief sigResult
     *  结果信号
     * @param success
     *  标记连接失败或成功
     */
    void sigResult(bool success);
};

END_NAMESPACE

#endif // CONNECTTOHOSTRESPONSE_H
