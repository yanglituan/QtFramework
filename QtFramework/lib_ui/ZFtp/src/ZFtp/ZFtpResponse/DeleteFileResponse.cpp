﻿#include "DeleteFileResponse.h"
#include "DeleteFileResponsePrivate.h"

BEGIN_NAMESPACE_ZFTPNS

DeleteFileResponse::DeleteFileResponse(QObject *parent) :
    ZFtpResponse(parent),
    d_ptr(new DeleteFileResponsePrivate)
{
    d_ptr->setPublicResponseObject(this);
}

QString DeleteFileResponse::lastErrorString()
{
    return d_ptr->lastErrorString();
}

END_NAMESPACE
