﻿#include "ZFtp.h"
#include "ZFtpPrivate.h"

BEGIN_NAMESPACE_ZFTPNS

ZFtp::ZFtp(QObject *parent) : QObject(parent),
    d_ptr(new ZFtpPrivate(this))
{
    d_ptr->q_ptr = this;

    init();
}

ZFtp::~ZFtp()
{
    if (d_ptr) delete d_ptr;
}

void ZFtp::setFtpServerTextCodec(const QString &text_codec_name)
{
    d_ptr->setFtpServerTextCodec(text_codec_name);
}

QString ZFtp::ftpServerTextCodec()
{
    return d_ptr->ftpServerTextCodec();
}

#if 0
void ZFtp::setAutoReconnect(bool reconnect)
{
    d_ptr->setAutoReconnect(reconnect);
}

bool ZFtp::isAutoReconnect()
{
    return d_ptr->autoReconnect();
}
#endif

void ZFtp::connectToHost(QSharedPointer<ConnectToHostRequest> request,
                         QSharedPointer<ConnectToHostResponse> response)
{
    d_ptr->connectToHost(request->d_ptr, response->d_ptr);
}

void ZFtp::login(QSharedPointer<LoginRequest> request, QSharedPointer<LoginResponse> response)
{
    d_ptr->login(request->d_ptr, response->d_ptr);
}

void ZFtp::close(QSharedPointer<CloseFtpRequest> request, QSharedPointer<CloseFtpResponse> response)
{
    d_ptr->close(request->d_ptr, response->d_ptr);
}

void ZFtp::listDir(QSharedPointer<ListDirRequest> request, QSharedPointer<ListDirResponse> response)
{
    d_ptr->listDir(request->d_ptr, response->d_ptr);
}

void ZFtp::mkdir(QSharedPointer<MkDirRequest> request, QSharedPointer<MkDirResponse> response)
{
    d_ptr->mkdir(request->d_ptr, response->d_ptr);
}

void ZFtp::rmdir(QSharedPointer<RmDirRequest> request, QSharedPointer<RmDirResponse> response)
{
    d_ptr->rmdir(request->d_ptr, response->d_ptr);
}

void ZFtp::cd(QSharedPointer<CdDirRequest> request, QSharedPointer<CdDirResponse> response)
{
    d_ptr->cd(request->d_ptr, response->d_ptr);
}

void ZFtp::uploadFile(QSharedPointer<UploadFileRequest1> request, QSharedPointer<UploadFileResponse1> response)
{
    d_ptr->uploadFile(request->d_ptr, response->d_ptr);
}

void ZFtp::uploadFile(QSharedPointer<UploadFileRequest2> request, QSharedPointer<UploadFileResponse2> response)
{
    d_ptr->uploadFile(request->d_ptr, response->d_ptr);
}

void ZFtp::uploadFile(QSharedPointer<UploadFileRequest3> request, QSharedPointer<UploadFileResponse3> response)
{
    d_ptr->uploadFile(request->d_ptr, response->d_ptr);
}

void ZFtp::downloadFile(QSharedPointer<DownloadFileRequest1> request, QSharedPointer<DownloadFileResponse1> response)
{
    d_ptr->downloadFile(request->d_ptr, response->d_ptr);
}

void ZFtp::downloadFile(QSharedPointer<DownloadFileRequest2> request, QSharedPointer<DownloadFileResponse2> response)
{
    d_ptr->downloadFile(request->d_ptr, response->d_ptr);
}

void ZFtp::deleteFile(QSharedPointer<DeleteFileRequest> request, QSharedPointer<DeleteFileResponse> response)
{
    d_ptr->deleteFile(request->d_ptr, response->d_ptr);
}

void ZFtp::renameFile(QSharedPointer<RenameFileRequest> request, QSharedPointer<RenameFileResponse> response)
{
    d_ptr->renameFile(request->d_ptr, response->d_ptr);
}

void ZFtp::setTransferMode(QFtp::TransferMode mode)
{
    d_ptr->setTransferMode(mode);
}

QFtp::State ZFtp::state() const
{
    return d_ptr->state();
}

QFtp::Error ZFtp::error() const
{
    return d_ptr->error();
}

QString ZFtp::errorString() const
{
    return d_ptr->errorString();
}

void ZFtp::abort()
{
    d_ptr->abort();
}

void ZFtp::init()
{
    createConnects();
}

void ZFtp::createConnects()
{
    connect(d_ptr, SIGNAL(sigFtpStateChanged(int)), SIGNAL(sigFtpStateChanged(int)));
}

END_NAMESPACE

