﻿#ifndef ZFTP_H
#define ZFTP_H

/*****************************************************************************
** ZFtp，基于QFtp进行封装
** 自己维护任务队列，不使用QFtp的任务队列
** 将任务结果作为一个对象返回。
** 采用实现私有化方式进行封装
*****************************************************************************/

#include "ZFtpGlobal.h"
#include "ZFtpExports.h"
#include "qftp.h"
#include "qurlinfo.h"
#include "ZFtpRequest.h"
#include "ZFtpResponse.h"
#include <QObject>
#include <QSharedPointer>

class QIODevice;

BEGIN_NAMESPACE_ZFTPNS

class ZFtpPrivate;
class ZFTP_API ZFtp : public QObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(ZFtp)

public:
    explicit ZFtp(QObject *parent = NULL);
    ~ZFtp();

public:
    /**
     * @brief setFtpServerTextCodec
     *  设置服务器的编码，防止中文乱码
     * @param text_codec_name
     */
    void setFtpServerTextCodec(const QString &text_codec_name);

    /**
     * @brief ftpServerTextCodec
     * @return
     */
    QString ftpServerTextCodec();

#if 0
    /**
     * @brief setAutoReconnect
     *  设置连接断开时是否自动重连
     * @param reconnect
     */
    void setAutoReconnect(bool reconnect);

    /**
     * @brief isAutoReconnect
     *  判断当前是否自动重连
     * @return
     */
    bool isAutoReconnect();
#endif

    /**
     * @brief connectToHost
     *  连接服务器
     * @param request
     * @param response
     */
    void connectToHost(QSharedPointer<ConnectToHostRequest> request, QSharedPointer<ConnectToHostResponse> response);

    /**
     * @brief login
     *  登录Ftp服务器
     * @param request
     * @param response
     */
    void login(QSharedPointer<LoginRequest> request, QSharedPointer<LoginResponse> response);

    /**
     * @brief close
     *  关闭Ftp连接
     * @param request
     * @param response
     */
    void close(QSharedPointer<CloseFtpRequest> request, QSharedPointer<CloseFtpResponse> response);

    /**
     * @brief listDir
     *  列出目录
     * @param request
     * @param response
     */
    void listDir(QSharedPointer<ListDirRequest> request, QSharedPointer<ListDirResponse> response);

    /**
     * @brief mkdir
     *  创建目录
     * @param request
     * @param response
     */
    void mkdir(QSharedPointer<MkDirRequest> request, QSharedPointer<MkDirResponse> response);

    /**
     * @brief rmdir
     *  删除目录
     * @param request
     * @param response
     */
    void rmdir(QSharedPointer<RmDirRequest> request, QSharedPointer<RmDirResponse> response);

    /**
     * @brief cd
     *  进入目录
     * @param request
     * @param response
     */
    void cd(QSharedPointer<CdDirRequest> request, QSharedPointer<CdDirResponse> response);

    /**
     * @brief uploadFile
     *  上传1
     * @param request
     * @param response
     */
    void uploadFile(QSharedPointer<UploadFileRequest1> request, QSharedPointer<UploadFileResponse1> response);

    /**
     * @brief uploadFile
     *  上传2
     * @param request
     * @param response
     */
    void uploadFile(QSharedPointer<UploadFileRequest2> request, QSharedPointer<UploadFileResponse2> response);

    /**
     * @brief uploadFile
     *  上传3
     * @param request
     * @param response
     */
    void uploadFile(QSharedPointer<UploadFileRequest3> request, QSharedPointer<UploadFileResponse3> response);

    /**
     * @brief downloadFile
     *  下载
     * @param request
     * @param response
     */
    void downloadFile(QSharedPointer<DownloadFileRequest1> request, QSharedPointer<DownloadFileResponse1> response);

    /**
     * @brief downloadFile
     *  下载
     * @param request
     * @param response
     */
    void downloadFile(QSharedPointer<DownloadFileRequest2> request, QSharedPointer<DownloadFileResponse2> response);

    /**
     * @brief deleteFile
     *  删除文件
     * @param request
     * @param response
     */
    void deleteFile(QSharedPointer<DeleteFileRequest> request, QSharedPointer<DeleteFileResponse> response);

    /**
     * @brief renameFile
     *  重命名文件
     * @param request
     * @param response
     */
    void renameFile(QSharedPointer<RenameFileRequest> request, QSharedPointer<RenameFileResponse> response);

    /**
     * @brief setTransferMode
     *  设置传输模式
     * @param mode
     */
    void setTransferMode(QFtp::TransferMode mode);

    /**
     * @brief state
     * @return
     */
    QFtp::State state() const;

    /**
     * @brief error
     * @return
     */
    QFtp::Error	error() const;

    /**
     * @brief errorString
     * @return
     */
    QString	errorString() const;

public slots:
    void abort();

private:
    void init();
    void createConnects();

protected:
    ZFtpPrivate* const d_ptr;

signals:
    void sigFtpStateChanged(int state);
    void sigNewTaskAdded(QPointer<ZFtpRequest>, QPointer<ZFtpResponse>);
};

END_NAMESPACE

#endif // ZFTP_H
