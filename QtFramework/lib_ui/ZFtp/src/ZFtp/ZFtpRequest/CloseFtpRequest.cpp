﻿#include "CloseFtpRequest.h"
#include "CloseFtpRequestPrivate.h"

BEGIN_NAMESPACE_ZFTPNS

CloseFtpRequest::CloseFtpRequest(QObject *parent) : ZFtpRequest(parent),
    d_ptr(new CloseFtpRequestPrivate)
{
    d_ptr->q_ptr = this;
}

END_NAMESPACE
