﻿#include "ListDirRequestPrivate.h"
#include "ZFtpUtils.h"
#include "ListDirResponsePrivate.h"
#include <QTextCodec>

BEGIN_NAMESPACE_ZFTPNS

ListDirRequestPrivate::ListDirRequestPrivate(QObject *parent) :
    ZFtpRequestPrivate(parent),
    q_ptr(NULL)
{

}

int ListDirRequestPrivate::commandID() const
{
    return ZFtpRequestPrivate::List;
}

int ListDirRequestPrivate::doRequest(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg)
{
    if (!ftp) return -1;

    if (cfg)
    {
        // 进行编码转换，防止中文乱码
        QTextCodec *text_codec = cfg->ftpServerTextCodec();

        // QString直接包裹指定编码的数据
        QString coded_ftp_dirpath = ZFtpUtils::toTextCodecString(text_codec, m_dirpath);
        return ftp->list(coded_ftp_dirpath);
    }
    else
    {
        return ftp->list(m_dirpath);
    }
}

QPointer<ZFtpRequest> ListDirRequestPrivate::publicObject()
{
    return QPointer<ZFtpRequest>(q_ptr);
}

ZFtpResponsePrivate *ListDirRequestPrivate::createResponse()
{
    return new ListDirResponsePrivate();
}

END_NAMESPACE
