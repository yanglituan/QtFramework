﻿#ifndef UPLOADFILEREQUEST3PRIVATE_H
#define UPLOADFILEREQUEST3PRIVATE_H

#include "ZFtpRequestPrivate.h"
#include "ZFtpGlobal.h"
#include "UploadFileRequest3.h"
#include <QFile>

BEGIN_NAMESPACE_ZFTPNS

class UploadFileRequest3Private : public ZFtpRequestPrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC_D(q_ptr.data(), UploadFileRequest3)

public:
    UploadFileRequest3Private(QObject *parent = NULL);
    virtual ~UploadFileRequest3Private();

    virtual int commandID() const;
    virtual int doRequest(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg);
    virtual QPointer<ZFtpRequest> publicObject();
    virtual ZFtpResponsePrivate *createResponse();

private slots:
    void slotCommandFinished(int id, bool error);

private:
    QPointer<UploadFileRequest3> q_ptr;
    QString m_local_filepath;
    QString m_ftp_filepath;
    QFile   m_file;
    int     m_task_id;
};

END_NAMESPACE

#endif // UPLOADFILEREQUEST3PRIVATE_H
