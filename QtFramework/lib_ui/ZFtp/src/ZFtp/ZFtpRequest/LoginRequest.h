﻿#ifndef LOGINREQUEST_H
#define LOGINREQUEST_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpRequest.h"
#include "ZFtpGlobal.h"
#include "ZFtpExports.h"

BEGIN_NAMESPACE_ZFTPNS

class LoginRequestPrivate;

class ZFTP_API LoginRequest : public ZFtpRequest
{
    Q_DECLARE_PRIVATE(LoginRequest)

public:
    explicit LoginRequest(QObject *parent = 0);

    /**
     * @brief setLoginInfo
     *  设置登录信息
     * @param username
     * @param password
     */
    void setLoginInfo(const QString &username, const QString &password);

public:
    QSharedPointer<LoginRequestPrivate> d_ptr;
};

END_NAMESPACE

#endif // LOGINREQUEST_H
