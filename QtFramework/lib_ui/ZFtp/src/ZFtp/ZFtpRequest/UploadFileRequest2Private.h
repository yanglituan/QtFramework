﻿#ifndef UPLOADFILEREQUEST2PRIVATE_H
#define UPLOADFILEREQUEST2PRIVATE_H

#include <QObject>
#include "ZFtpRequestPrivate.h"
#include "ZFtpGlobal.h"

class QIODevice;

BEGIN_NAMESPACE_ZFTPNS

class UploadFileRequest2;
class UploadFileRequest2Private : public ZFtpRequestPrivate
{
    Q_DECLARE_PUBLIC_D(q_ptr.data(), UploadFileRequest2)

public:
    explicit UploadFileRequest2Private(QObject *parent = 0);

    virtual int commandID() const;
    virtual int doRequest(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg);
    virtual QPointer<ZFtpRequest> publicObject();
    virtual ZFtpResponsePrivate *createResponse();

private:
    QIODevice *dev;
    QString m_dest_ftp_filepath;

    QPointer<UploadFileRequest2> q_ptr;
};

END_NAMESPACE

#endif // UPLOADFILEREQUEST2PRIVATE_H
