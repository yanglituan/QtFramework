﻿#include "LoginRequest.h"
#include "LoginRequestPrivate.h"

BEGIN_NAMESPACE_ZFTPNS

LoginRequest::LoginRequest(QObject *parent) : ZFtpRequest(parent),
    d_ptr(new LoginRequestPrivate)
{
}

void LoginRequest::setLoginInfo(const QString &username, const QString &password)
{
    d_ptr->m_username = username;
    d_ptr->m_password = password;
}

END_NAMESPACE
