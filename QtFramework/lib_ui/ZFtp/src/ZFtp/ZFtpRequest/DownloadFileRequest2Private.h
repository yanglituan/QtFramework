﻿#ifndef DOWNLOADFILEREQUEST2PRIVATE_H
#define DOWNLOADFILEREQUEST2PRIVATE_H

#include <QObject>
#include <QFile>
#include "ZFtpRequestPrivate.h"

BEGIN_NAMESPACE_ZFTPNS

class DownloadFileRequest2;

class DownloadFileRequest2Private : public ZFtpRequestPrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC_D(q_ptr.data(), DownloadFileRequest2)

public:
    explicit DownloadFileRequest2Private(QObject *parent = 0);

    virtual int commandID() const;
    virtual int doRequest(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig> cfg);
    virtual QPointer<ZFtpRequest> publicObject();
    virtual ZFtpResponsePrivate *createResponse();

private slots:
    void slotCommandFinished(int id, bool error);

private:
    QPointer<DownloadFileRequest2> q_ptr;
    QString m_ftp_filepath;
    QString m_local_savepath;
    QFile   m_file;
    int     m_task_id;
};

END_NAMESPACE

#endif // DOWNLOADFILEREQUEST2PRIVATE_H
