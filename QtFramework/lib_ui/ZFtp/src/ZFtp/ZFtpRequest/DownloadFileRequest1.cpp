﻿#include "DownloadFileRequest1.h"
#include "DownloadFileRequest1Private.h"

BEGIN_NAMESPACE_ZFTPNS

DownloadFileRequest1::DownloadFileRequest1(QObject *parent) : ZFtpRequest(parent),
    d_ptr(new DownloadFileRequest1Private)
{
    d_ptr->q_ptr = this;
}

void DownloadFileRequest1::setDownloadFileParam(const QString &ftp_filepath, QIODevice *dev)
{
    d_ptr->m_ftp_filepath = ftp_filepath;
    d_ptr->dev = dev;
}

END_NAMESPACE
