﻿#ifndef CLOSEFTPREQUESTPRIVATE_H
#define CLOSEFTPREQUESTPRIVATE_H

#include <QObject>
#include <QPointer>
#include "ZFtpRequestPrivate.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class CloseFtpRequest;
class CloseFtpRequestPrivate : public ZFtpRequestPrivate
{
    Q_DECLARE_PUBLIC_D(q_ptr.data(), CloseFtpRequest)

public:
    explicit CloseFtpRequestPrivate(QObject *parent = 0);

    virtual int commandID() const;
    virtual int doRequest(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig>);
    virtual QPointer<ZFtpRequest> publicObject();
    virtual ZFtpResponsePrivate *createResponse();

private:
    QPointer<CloseFtpRequest> q_ptr;
};

END_NAMESPACE

#endif // CLOSEFTPREQUESTPRIVATE_H
