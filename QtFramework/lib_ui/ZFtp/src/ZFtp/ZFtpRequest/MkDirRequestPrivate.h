﻿#ifndef MKDIRREQUESTPRIVATE_H
#define MKDIRREQUESTPRIVATE_H

#include <QObject>
#include "ZFtpRequestPrivate.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class MkDirRequest;
class MkDirRequestPrivate : public ZFtpRequestPrivate
{
    Q_DECLARE_PUBLIC(MkDirRequest)

public:
    explicit MkDirRequestPrivate(QObject *parent = 0);

    virtual int commandID() const;
    virtual int doRequest(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg);
    virtual QPointer<ZFtpRequest> publicObject();
    virtual ZFtpResponsePrivate *createResponse();

private:
    QString m_ftp_dirpath;
    QPointer<MkDirRequest> q_ptr;
};

END_NAMESPACE

#endif // MKDIRREQUESTPRIVATE_H
