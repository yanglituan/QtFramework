﻿#ifndef RMDIRREQUEST_H
#define RMDIRREQUEST_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpRequest.h"
#include "ZFtpGlobal.h"
#include "ZFtpExports.h"

BEGIN_NAMESPACE_ZFTPNS

class RmDirRequestPrivate;
class ZFtpRequestPrivate;

class ZFTP_API RmDirRequest : public ZFtpRequest
{
    Q_DECLARE_PRIVATE(RmDirRequest)

public:
    explicit RmDirRequest(QObject *parent = 0);

    void setDirName(const QString &);

public:
    QSharedPointer<RmDirRequestPrivate> d_ptr;
};

END_NAMESPACE

#endif // RMDIRREQUEST_H
