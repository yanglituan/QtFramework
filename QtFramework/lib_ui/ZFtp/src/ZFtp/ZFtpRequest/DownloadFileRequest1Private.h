﻿#ifndef DOWNLOADFILEREQUEST1PRIVATE_H
#define DOWNLOADFILEREQUEST1PRIVATE_H

#include <QObject>
#include "ZFtpRequestPrivate.h"

BEGIN_NAMESPACE_ZFTPNS

class DownloadFileRequest1;
class DownloadFileRequest1Private : public ZFtpRequestPrivate
{
    Q_DECLARE_PUBLIC_D(q_ptr.data(), DownloadFileRequest1)

public:
    explicit DownloadFileRequest1Private(QObject *parent = 0);

    virtual int commandID() const;
    virtual int doRequest(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig> cfg);
    virtual QPointer<ZFtpRequest> publicObject();
    virtual ZFtpResponsePrivate *createResponse();

private:
    QString m_ftp_filepath;
    QIODevice *dev;
    QPointer<DownloadFileRequest1> q_ptr;
};

END_NAMESPACE

#endif // DOWNLOADFILEREQUEST1PRIVATE_H
