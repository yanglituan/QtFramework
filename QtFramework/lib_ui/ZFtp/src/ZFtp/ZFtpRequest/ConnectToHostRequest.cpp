﻿#include "ConnectToHostRequest.h"
#include "ConnectToHostRequestPrivate.h"

BEGIN_NAMESPACE_ZFTPNS

ConnectToHostRequest::ConnectToHostRequest(QObject *parent) :
    ZFtpRequest(parent),
    d_ptr(new ConnectToHostRequestPrivate)
{
    d_ptr->q_ptr = this;
}

void ConnectToHostRequest::setConnectInfo(const QString &hostname, const quint16 &port)
{
    d_ptr->m_hostname = hostname;
    d_ptr->m_port = port;
}

END_NAMESPACE
