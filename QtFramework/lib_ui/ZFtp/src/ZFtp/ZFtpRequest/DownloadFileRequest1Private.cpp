﻿#include "DownloadFileRequest1Private.h"
#include "ZFtpUtils.h"
#include "DownloadFileResponse1Private.h"
#include <QTextCodec>

BEGIN_NAMESPACE_ZFTPNS

DownloadFileRequest1Private::DownloadFileRequest1Private(QObject *parent) :
    ZFtpRequestPrivate(parent),
    q_ptr(NULL)
{

}

int DownloadFileRequest1Private::commandID() const
{
    return ZFtpRequestPrivate::Get;
}

int DownloadFileRequest1Private::doRequest(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg)
{
    if (!ftp) return -1;
    if (!dev) return -1;

    if (cfg)
    {
        // 进行编码转换，防止中文乱码
        QTextCodec *text_codec = cfg->ftpServerTextCodec();

        // QString直接包裹指定编码的数据
        QString coded_ftp_filepath = ZFtpUtils::toTextCodecString(text_codec, m_ftp_filepath);
        return ftp->get(coded_ftp_filepath, dev);
    }
    else
    {
        return ftp->get(m_ftp_filepath, dev);
    }
}

QPointer<ZFtpRequest> DownloadFileRequest1Private::publicObject()
{
    return QPointer<ZFtpRequest>(q_ptr);
}

ZFtpResponsePrivate *DownloadFileRequest1Private::createResponse()
{
    return new DownloadFileResponse1Private();
}

END_NAMESPACE
