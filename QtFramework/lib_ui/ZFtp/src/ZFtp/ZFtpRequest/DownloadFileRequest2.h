﻿#ifndef DOWNLOADFILEREQUEST2_H
#define DOWNLOADFILEREQUEST2_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpRequest.h"
#include "ZFtpGlobal.h"
#include "ZFtpExports.h"

class QIODevice;

BEGIN_NAMESPACE_ZFTPNS

class DownloadFileRequest2Private;

class ZFTP_API DownloadFileRequest2 : public ZFtpRequest
{
public:
    explicit DownloadFileRequest2(QObject *parent = 0);

    void setDownloadFileParam(const QString &local_savepath, const QString &ftp_filepath);

public:
    QSharedPointer<DownloadFileRequest2Private> d_ptr;
};

END_NAMESPACE

#endif // DOWNLOADFILEREQUEST_H
