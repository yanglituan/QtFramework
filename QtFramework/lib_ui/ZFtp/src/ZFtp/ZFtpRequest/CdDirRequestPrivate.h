﻿#ifndef CDDIRREQUESTPRIVATE_H
#define CDDIRREQUESTPRIVATE_H

#include <QObject>
#include "ZFtpRequestPrivate.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class CdDirRequest;
class CdDirRequestPrivate : public ZFtpRequestPrivate
{
    Q_OBJECT
    Q_DECLARE_PUBLIC_D(q_ptr.data(), CdDirRequest)

public:
    explicit CdDirRequestPrivate(QObject *parent = 0);

    virtual int commandID() const;
    virtual int doRequest(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig>);
    virtual QPointer<ZFtpRequest> publicObject();
    virtual ZFtpResponsePrivate *createResponse();

private:
    QString m_ftp_dirpath;
    QPointer<CdDirRequest> q_ptr;
};

END_NAMESPACE

#endif // CDDIRREQUESTPRIVATE_H
