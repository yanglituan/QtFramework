﻿#ifndef RENAMEFILEREQUESTPRIVATE_H
#define RENAMEFILEREQUESTPRIVATE_H

#include <QObject>
#include "ZFtpRequestPrivate.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class RenameFileRequest;
class RenameFileRequestPrivate : public ZFtpRequestPrivate
{
    Q_DECLARE_PUBLIC_D(q_ptr.data(), RenameFileRequest)

public:
    explicit RenameFileRequestPrivate(QObject *parent = 0);

    virtual int commandID() const;
    virtual int doRequest(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg);
    virtual QPointer<ZFtpRequest> publicObject();
    virtual ZFtpResponsePrivate *createResponse();

private:
    QString m_old_ftp_filepath;
    QString m_new_ftp_filepath;

    QPointer<RenameFileRequest> q_ptr;
};

END_NAMESPACE

#endif // RENAMEFILEREQUESTPRIVATE_H
