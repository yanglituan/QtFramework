﻿#include "CdDirRequest.h"
#include "CdDirRequestPrivate.h"

BEGIN_NAMESPACE_ZFTPNS

CdDirRequest::CdDirRequest(QObject *parent) : ZFtpRequest(parent),
    d_ptr(new CdDirRequestPrivate)
{
    d_ptr->q_ptr = this;
}

void CdDirRequest::setFtpDirname(const QString &ftp_dirname)
{
    d_ptr->m_ftp_dirpath = ftp_dirname;
}

END_NAMESPACE
