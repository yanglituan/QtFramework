﻿#include "LoginRequestPrivate.h"
#include "LoginResponsePrivate.h"

BEGIN_NAMESPACE_ZFTPNS

LoginRequestPrivate::LoginRequestPrivate(QObject *parent) :
    ZFtpRequestPrivate(parent),
    q_ptr(NULL)
{

}

int LoginRequestPrivate::commandID() const
{
    return ZFtpRequestPrivate::Login;
}

int LoginRequestPrivate::doRequest(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg)
{
    Q_UNUSED(cfg)

    if (!ftp) return -1;
    return ftp->login(m_username, m_password);
}

QPointer<ZFtpRequest> LoginRequestPrivate::publicObject()
{
    return QPointer<ZFtpRequest>(q_ptr);
}

ZFtpResponsePrivate *LoginRequestPrivate::createResponse()
{
    return new LoginResponsePrivate();
}

END_NAMESPACE
