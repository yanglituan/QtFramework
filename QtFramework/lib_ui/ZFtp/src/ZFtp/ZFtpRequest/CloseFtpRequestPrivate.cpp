﻿#include "CloseFtpRequestPrivate.h"
#include "CloseFtpResponsePrivate.h"

BEGIN_NAMESPACE_ZFTPNS

CloseFtpRequestPrivate::CloseFtpRequestPrivate(QObject *parent) :
    ZFtpRequestPrivate(parent)
{

}

int CloseFtpRequestPrivate::commandID() const
{
    return ZFtpRequestPrivate::Close;
}

int CloseFtpRequestPrivate::doRequest(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg)
{
    Q_UNUSED(cfg);

    if (!ftp) return -1;
    return ftp->close();
}

QPointer<ZFtpRequest> CloseFtpRequestPrivate::publicObject()
{
    return QPointer<ZFtpRequest>(q_ptr);
}

ZFtpResponsePrivate *CloseFtpRequestPrivate::createResponse()
{
    return new CloseFtpResponsePrivate();
}

END_NAMESPACE
