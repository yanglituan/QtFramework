﻿#include "ConnectToHostRequestPrivate.h"
#include "ZFtpResponsePrivate.h"

BEGIN_NAMESPACE_ZFTPNS

ConnectToHostRequestPrivate::ConnectToHostRequestPrivate(QObject *parent) :
    ZFtpRequestPrivate(parent),
    q_ptr(NULL)
{
    // 端口默认为21号端口
    m_port = 21;
}

int ConnectToHostRequestPrivate::commandID() const
{
    return ZFtpRequestPrivate::ConnectToHost;
}

int ConnectToHostRequestPrivate::doRequest(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg)
{
    Q_UNUSED(cfg);

    if (!ftp) return -1;
    return ftp->connectToHost(m_hostname, m_port);
}

QPointer<ZFtpRequest> ConnectToHostRequestPrivate::publicObject()
{
    return QPointer<ZFtpRequest>(q_ptr);
}

ZFtpResponsePrivate *ConnectToHostRequestPrivate::createResponse()
{
    return new ConnectToHostResponsePrivate();
}

END_NAMESPACE
