﻿#ifndef ZFTPREQUESTPRIVATE_H
#define ZFTPREQUESTPRIVATE_H

#include <QObject>
#include <QSharedPointer>
#include <QPointer>
#include <ZFtpConfig.h>
#include "qftp.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class ZFtpResponse;
class ZFtpRequest;
class ZFtpResponsePrivate;

/**
 * @brief The ZFtpCommand class
 *  Ftp命令基类
 */
class ZFtpRequestPrivate : public QObject
{
public:
    // 操作标志码
    enum
    {
        None,
        SetTransferMode,
        SetProxy,
        ConnectToHost,
        Login,
        Close,
        List,
        Cd,
        Get,
        PutV1,
        PutV2,
        PutV3,
        Remove,
        Mkdir,
        Rmdir,
        Rename
    };

    explicit ZFtpRequestPrivate(QObject *parent = 0);
    virtual ~ZFtpRequestPrivate();
    virtual int commandID() const = 0;
    virtual int doRequest(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig>) = 0;

    /**
     * @brief publicObject
     *  用于获取公有对象
     * @return
     */
    virtual QPointer<ZFtpRequest> publicObject() = 0;
    virtual ZFtpResponsePrivate *createResponse() = 0;
};

END_NAMESPACE

#include "ConnectToHostRequestPrivate.h"
#include "LoginRequestPrivate.h"
#include "CloseFtpRequestPrivate.h"
#include "RmDirRequestPrivate.h"
#include "MkDirRequestPrivate.h"
#include "CdDirRequestPrivate.h"
#include "DeleteFileRequestPrivate.h"
#include "ListDirRequestPrivate.h"
#include "RenameFileRequestPrivate.h"
#include "UploadFileRequest1Private.h"
#include "UploadFileRequest2Private.h"
#include "UploadFileRequest3Private.h"
#include "DownloadFileRequest1Private.h"
#include "DownloadFileRequest2Private.h"

#endif // ZFTPREQUESTPRIVATE_H
