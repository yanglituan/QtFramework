﻿#ifndef LISTDIRREQUESTPRIVATE_H
#define LISTDIRREQUESTPRIVATE_H

#include <QObject>
#include "ZFtpRequestPrivate.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class ListDirRequest;
class ListDirRequestPrivate : public ZFtpRequestPrivate
{
    Q_DECLARE_PUBLIC_D(q_ptr.data(), ListDirRequest)

public:
    explicit ListDirRequestPrivate(QObject *parent = 0);

    virtual int commandID() const;
    virtual int doRequest(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig> cfg);
    virtual QPointer<ZFtpRequest> publicObject();
    virtual ZFtpResponsePrivate *createResponse();

private:
    QString m_dirpath;
    QPointer<ListDirRequest> q_ptr;
};

END_NAMESPACE

#endif // LISTDIRREQUESTPRIVATE_H
