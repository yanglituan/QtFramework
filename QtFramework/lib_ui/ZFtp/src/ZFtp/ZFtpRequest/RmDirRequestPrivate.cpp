﻿#include "RmDirRequestPrivate.h"
#include "ZFtpUtils.h"
#include "RmDirResponsePrivate.h"
#include <QTextCodec>

BEGIN_NAMESPACE_ZFTPNS

RmDirRequestPrivate::RmDirRequestPrivate(QObject *parent) :
    ZFtpRequestPrivate(parent),
    q_ptr(NULL)
{

}

int RmDirRequestPrivate::commandID() const
{
    return ZFtpRequestPrivate::Rmdir;
}

int RmDirRequestPrivate::doRequest(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg)
{
    if (!ftp) return -1;

    if (!cfg)
    {
        // 进行编码转换，防止中文乱码
        QTextCodec *text_codec = cfg->ftpServerTextCodec();

        // QString直接包裹指定编码的数据
        QString coded_ftp_dirpath = ZFtpUtils::toTextCodecString(text_codec, m_ftp_dirpath);

        return ftp->rmdir(coded_ftp_dirpath);
    }
    else
    {
        return ftp->rmdir(m_ftp_dirpath);
    }
}

QPointer<ZFtpRequest> RmDirRequestPrivate::publicObject()
{
    return QPointer<ZFtpRequest>(q_ptr);
}

ZFtpResponsePrivate *RmDirRequestPrivate::createResponse()
{
    return new RmDirResponsePrivate();
}

END_NAMESPACE
