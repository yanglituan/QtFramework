﻿#ifndef CONNECTTOHOSTREQUESTPRIVATE_H
#define CONNECTTOHOSTREQUESTPRIVATE_H

#include <QObject>
#include "ZFtpRequestPrivate.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class ConnectToHostRequest;
class ConnectToHostRequestPrivate : public ZFtpRequestPrivate
{
    Q_DECLARE_PUBLIC_D(q_ptr.data(), ConnectToHostRequest)

public:
    explicit ConnectToHostRequestPrivate(QObject *parent = 0);
    ~ConnectToHostRequestPrivate() {}

    virtual int commandID() const;
    virtual int doRequest(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig>);
    virtual QPointer<ZFtpRequest> publicObject();
    virtual ZFtpResponsePrivate *createResponse();

private:
    QString m_hostname;
    quint16 m_port;
    QPointer<ConnectToHostRequest> q_ptr;
};

END_NAMESPACE

#endif // CONNECTTOHOSTREQUESTPRIVATE_H
