﻿#include "UploadFileRequest3.h"
#include "UploadFileRequest3Private.h"

BEGIN_NAMESPACE_ZFTPNS

UploadFileRequest3::UploadFileRequest3(QObject *parent) :
    ZFtpRequest(parent),
    d_ptr(new UploadFileRequest3Private)
{
    d_ptr->q_ptr = this;
}

void UploadFileRequest3::setUploadParam(const QString &local_filepath, const QString &ftp_filepath)
{
    d_ptr->m_local_filepath = local_filepath;
    d_ptr->m_ftp_filepath = ftp_filepath;
}

END_NAMESPACE
