﻿#include "ListDirRequest.h"
#include "ListDirRequestPrivate.h"

BEGIN_NAMESPACE_ZFTPNS

ListDirRequest::ListDirRequest(QObject *parent) : ZFtpRequest(parent),
    d_ptr(new ListDirRequestPrivate)
{
}

void ListDirRequest::setFtpDirpath(const QString &ftp_dirpath)
{
    d_ptr->m_dirpath = ftp_dirpath;
}

END_NAMESPACE
