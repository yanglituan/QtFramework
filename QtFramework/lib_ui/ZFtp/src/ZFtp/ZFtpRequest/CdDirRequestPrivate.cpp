﻿#include "CdDirRequestPrivate.h"
#include "ZFtpUtils.h"
#include "CdDirResponsePrivate.h"
#include "CdDirRequest.h"
#include <QTextCodec>

BEGIN_NAMESPACE_ZFTPNS

CdDirRequestPrivate::CdDirRequestPrivate(QObject *parent) :
    ZFtpRequestPrivate(parent),
    q_ptr(NULL)
{

}

int CdDirRequestPrivate::commandID() const
{
    return ZFtpRequestPrivate::Cd;
}

int CdDirRequestPrivate::doRequest(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg)
{
    if (!ftp) return -1;

    if (cfg)
    {
        // 进行编码转换，防止中文乱码
        QTextCodec *text_codec = cfg->ftpServerTextCodec();

        // QString直接包裹指定编码的数据
        QString coded_ftp_dirpath = ZFtpUtils::toTextCodecString(text_codec, m_ftp_dirpath);
        return ftp->cd(coded_ftp_dirpath);
    }
    else
    {
        return ftp->cd(m_ftp_dirpath);
    }
}

QPointer<ZFtpRequest> CdDirRequestPrivate::publicObject()
{
    return QPointer<ZFtpRequest>(q_ptr);
}

ZFtpResponsePrivate *CdDirRequestPrivate::createResponse()
{
    return new CdDirResponsePrivate();
}

END_NAMESPACE
