﻿#include "UploadFileRequest2Private.h"
#include "ZFtpUtils.h"
#include "UploadFileResponse2Private.h"
#include <QTextCodec>

BEGIN_NAMESPACE_ZFTPNS

UploadFileRequest2Private::UploadFileRequest2Private(QObject *parent) :
    ZFtpRequestPrivate(parent),
    q_ptr(NULL)
{
    dev = NULL;
}

int UploadFileRequest2Private::commandID() const
{
    return ZFtpRequestPrivate::PutV2;
}

int UploadFileRequest2Private::doRequest(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg)
{
    if (!ftp) return -1;

    if (cfg)
    {
        // 进行编码转换，防止中文乱码
        QTextCodec *text_codec = cfg->ftpServerTextCodec();

        // QString直接包裹指定编码的数据
        QString coded_ftp_filepath = ZFtpUtils::toTextCodecString(text_codec, m_dest_ftp_filepath);

        return ftp->put(dev, coded_ftp_filepath);
    }
    else
    {
        return ftp->put(dev, m_dest_ftp_filepath);
    }
}

QPointer<ZFtpRequest> UploadFileRequest2Private::publicObject()
{
    return QPointer<ZFtpRequest>(q_ptr);
}

ZFtpResponsePrivate *UploadFileRequest2Private::createResponse()
{
    return new UploadFileResponse2Private();
}

END_NAMESPACE
