﻿#include "RenameFileRequestPrivate.h"
#include "ZFtpUtils.h"
#include "RenameFileResponsePrivate.h"
#include <QTextCodec>

BEGIN_NAMESPACE_ZFTPNS

RenameFileRequestPrivate::RenameFileRequestPrivate(QObject *parent) :
    ZFtpRequestPrivate(parent),
    q_ptr(NULL)
{

}

int RenameFileRequestPrivate::commandID() const
{
    return ZFtpRequestPrivate::Rename;
}

int RenameFileRequestPrivate::doRequest(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg)
{
    if (!ftp) return -1;

    if (cfg)
    {
        // 进行编码转换，防止中文乱码
        QTextCodec *text_codec = cfg->ftpServerTextCodec();

        // QString直接包裹指定编码的数据
        QString coded_old_ftp_filepath = ZFtpUtils::toTextCodecString(text_codec, m_old_ftp_filepath);
        QString coded_new_ftp_filepath = ZFtpUtils::toTextCodecString(text_codec, m_new_ftp_filepath);
        return ftp->rename(coded_old_ftp_filepath, coded_new_ftp_filepath);
    }
    else
    {
        return ftp->rename(m_old_ftp_filepath, m_new_ftp_filepath);
    }
}

QPointer<ZFtpRequest> RenameFileRequestPrivate::publicObject()
{
    return QPointer<ZFtpRequest>(q_ptr);
}

ZFtpResponsePrivate *RenameFileRequestPrivate::createResponse()
{
    return new RenameFileResponsePrivate();
}

END_NAMESPACE
