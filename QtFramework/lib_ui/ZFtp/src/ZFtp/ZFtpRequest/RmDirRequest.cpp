﻿#include "RmDirRequest.h"
#include "RmDirRequestPrivate.h"

BEGIN_NAMESPACE_ZFTPNS

RmDirRequest::RmDirRequest(QObject *parent) : ZFtpRequest(parent),
    d_ptr(new RmDirRequestPrivate)
{
    d_ptr->q_ptr = this;
}

void RmDirRequest::setDirName(const QString &ftp_dirpath)
{
    d_ptr->m_ftp_dirpath = ftp_dirpath;
}

END_NAMESPACE
