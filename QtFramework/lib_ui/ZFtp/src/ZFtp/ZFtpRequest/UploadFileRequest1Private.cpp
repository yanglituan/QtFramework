﻿#include "UploadFileRequest1Private.h"
#include "ZFtpUtils.h"
#include "UploadFileResponse1Private.h"
#include <QTextCodec>

BEGIN_NAMESPACE_ZFTPNS

UploadFileRequest1Private::UploadFileRequest1Private(QObject *parent) :
    ZFtpRequestPrivate(parent),
    q_ptr(NULL)
{

}

int UploadFileRequest1Private::commandID() const
{
    return ZFtpRequestPrivate::PutV1;
}

int UploadFileRequest1Private::doRequest(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg)
{
    if (!ftp) return -1;

    if (!cfg)
    {
        // 进行编码转换，防止中文乱码
        QTextCodec *text_codec = cfg->ftpServerTextCodec();

        // QString直接包裹指定编码的数据
        QString coded_dest_ftp_filepath = ZFtpUtils::toTextCodecString(text_codec, m_dest_ftp_filepath);

        return ftp->put(m_data, coded_dest_ftp_filepath);
    }
    else
    {
        return ftp->put(m_data, m_dest_ftp_filepath);
    }
}

QPointer<ZFtpRequest> UploadFileRequest1Private::publicObject()
{
    return QPointer<ZFtpRequest>(q_ptr);
}

ZFtpResponsePrivate *UploadFileRequest1Private::createResponse()
{
    return new UploadFileResponse1Private();
}

END_NAMESPACE
