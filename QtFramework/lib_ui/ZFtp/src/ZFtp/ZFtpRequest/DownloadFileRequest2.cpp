﻿#include "DownloadFileRequest2.h"
#include "DownloadFileRequest2Private.h"

BEGIN_NAMESPACE_ZFTPNS

DownloadFileRequest2::DownloadFileRequest2(QObject *parent) : ZFtpRequest(parent),
    d_ptr(new DownloadFileRequest2Private)
{
    d_ptr->q_ptr = this;
}

void DownloadFileRequest2::setDownloadFileParam(const QString &local_savepath, const QString &ftp_filepath)
{
    d_ptr->m_ftp_filepath = ftp_filepath;
    d_ptr->m_local_savepath = local_savepath;
}

END_NAMESPACE
