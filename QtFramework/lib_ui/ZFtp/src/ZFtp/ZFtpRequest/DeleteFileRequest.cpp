﻿#include "DeleteFileRequest.h"
#include "DeleteFileRequestPrivate.h"

BEGIN_NAMESPACE_ZFTPNS

DeleteFileRequest::DeleteFileRequest(QObject *parent) : ZFtpRequest(parent),
    d_ptr(new DeleteFileRequestPrivate)
{
}

void DeleteFileRequest::setFilepath(const QString &filepath)
{
    d_ptr->m_ftp_filepath = filepath;
}

END_NAMESPACE
