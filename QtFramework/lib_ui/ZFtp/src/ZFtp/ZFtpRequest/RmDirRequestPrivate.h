﻿#ifndef RMDIRREQUESTPRIVATE_H
#define RMDIRREQUESTPRIVATE_H

#include <QObject>
#include "ZFtpRequestPrivate.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class RmDirRequest;
class RmDirRequestPrivate : public ZFtpRequestPrivate
{
    Q_DECLARE_PUBLIC_D(q_ptr.data(), RmDirRequest)

public:
    explicit RmDirRequestPrivate(QObject *parent = 0);

    virtual int commandID() const;
    virtual int doRequest(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg);
    virtual QPointer<ZFtpRequest> publicObject();
    virtual ZFtpResponsePrivate *createResponse();

private:
    QString m_ftp_dirpath;
    QPointer<RmDirRequest> q_ptr;
};

END_NAMESPACE

#endif // RMDIRREQUESTPRIVATE_H
