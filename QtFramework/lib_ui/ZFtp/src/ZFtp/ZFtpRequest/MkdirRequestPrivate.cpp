﻿#include "MkDirRequestPrivate.h"
#include "ZFtpUtils.h"
#include "MkDirResponsePrivate.h"
#include <QTextCodec>

BEGIN_NAMESPACE_ZFTPNS

MkDirRequestPrivate::MkDirRequestPrivate(QObject *parent) :
    ZFtpRequestPrivate(parent),
    q_ptr(NULL)
{

}

int MkDirRequestPrivate::commandID() const
{
    return ZFtpRequestPrivate::Mkdir;
}

int MkDirRequestPrivate::doRequest(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg)
{
    if (!ftp) return -1;

    if (!cfg)
    {
        // 进行编码转换，防止中文乱码
        QTextCodec *text_codec = cfg->ftpServerTextCodec();

        // QString直接包裹指定编码的数据
        QString coded_ftp_dirpath = ZFtpUtils::toTextCodecString(text_codec, m_ftp_dirpath);
        return ftp->mkdir(coded_ftp_dirpath);
    }
    else
    {
        return ftp->mkdir(m_ftp_dirpath);
    }
}

QPointer<ZFtpRequest> MkDirRequestPrivate::publicObject()
{
    return QPointer<ZFtpRequest>(q_ptr);
}

ZFtpResponsePrivate *MkDirRequestPrivate::createResponse()
{
    return new MkDirResponsePrivate();
}

END_NAMESPACE
