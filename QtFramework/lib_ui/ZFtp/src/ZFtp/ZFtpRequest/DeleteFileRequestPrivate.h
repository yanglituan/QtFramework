﻿#ifndef DELETEFILEREQUESTPRIVATE_H
#define DELETEFILEREQUESTPRIVATE_H

#include <QObject>
#include "ZFtpRequestPrivate.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class DeleteFileRequest;
class DeleteFileRequestPrivate : public ZFtpRequestPrivate
{
    Q_DECLARE_PUBLIC_D(q_ptr.data(), DeleteFileRequest)

public:
    explicit DeleteFileRequestPrivate(QObject *parent = 0);
    virtual int commandID() const;
    virtual int doRequest(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig>);
    virtual QPointer<ZFtpRequest> publicObject();
    virtual ZFtpResponsePrivate *createResponse();

private:
    QString m_ftp_filepath;
    QPointer<DeleteFileRequest> q_ptr;
};

END_NAMESPACE

#endif // DELETEFILEREQUESTPRIVATE_H
