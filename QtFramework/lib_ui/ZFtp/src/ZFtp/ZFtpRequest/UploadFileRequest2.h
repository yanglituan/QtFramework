﻿#ifndef UPLOADFILEREQUEST2_H
#define UPLOADFILEREQUEST2_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpRequest.h"
#include "ZFtpGlobal.h"
#include "ZFtpExports.h"

class QIODevice;

BEGIN_NAMESPACE_ZFTPNS

class UploadFileRequest2Private;

class ZFTP_API UploadFileRequest2 : public ZFtpRequest
{
    Q_DECLARE_PRIVATE(UploadFileRequest2)

public:
    explicit UploadFileRequest2(QObject *parent = 0);

    void setUploadParam(QIODevice *dev, const QString &ftp_filepath);

public:
    QSharedPointer<UploadFileRequest2Private> d_ptr;
};

END_NAMESPACE

#endif // UPLOADFILEREQUEST2_H
