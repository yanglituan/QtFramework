﻿#ifndef UPLOADFILEREQUEST1PRIVATE_H
#define UPLOADFILEREQUEST1PRIVATE_H

#include <QObject>
#include "ZFtpRequestPrivate.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class UploadFileRequest1;
class UploadFileRequest1Private : public ZFtpRequestPrivate
{
    Q_DECLARE_PUBLIC_D(q_ptr.data(), UploadFileRequest1)

public:
    explicit UploadFileRequest1Private(QObject *parent = 0);

    virtual int commandID() const;
    virtual int doRequest(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg);
    virtual QPointer<ZFtpRequest> publicObject();
    virtual ZFtpResponsePrivate *createResponse();

private:
    QByteArray m_data;
    QString m_dest_ftp_filepath;

    QPointer<UploadFileRequest1> q_ptr;
};

END_NAMESPACE

#endif // UPLOADFILEREQUEST1PRIVATE_H
