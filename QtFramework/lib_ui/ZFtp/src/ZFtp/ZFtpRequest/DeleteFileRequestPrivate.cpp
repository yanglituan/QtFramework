﻿#include "DeleteFileRequestPrivate.h"
#include "ZFtpUtils.h"
#include "DeleteFileResponsePrivate.h"
#include <QTextCodec>

BEGIN_NAMESPACE_ZFTPNS

DeleteFileRequestPrivate::DeleteFileRequestPrivate(QObject *parent) :
    ZFtpRequestPrivate(parent),
    q_ptr(NULL)
{

}

int DeleteFileRequestPrivate::commandID() const
{
    return ZFtpRequestPrivate::Remove;
}

int DeleteFileRequestPrivate::doRequest(QSharedPointer<QFtp> ftp, QSharedPointer<ZFtpConfig> cfg)
{
    if (!ftp) return -1;

    if (!cfg)
    {
        // 进行编码转换，防止中文乱码
        QTextCodec *text_codec = cfg->ftpServerTextCodec();

        // QString直接包裹指定编码的数据
        QString coded_ftp_filepath = ZFtpUtils::toTextCodecString(text_codec, m_ftp_filepath);
        return ftp->remove(coded_ftp_filepath);
    }
    else
    {
        return ftp->remove(m_ftp_filepath);
    }
}

QPointer<ZFtpRequest> DeleteFileRequestPrivate::publicObject()
{
    return QPointer<ZFtpRequest>(q_ptr);
}

ZFtpResponsePrivate *DeleteFileRequestPrivate::createResponse()
{
    return new DeleteFileResponsePrivate();
}

END_NAMESPACE
