﻿#include "UploadFileRequest1.h"
#include "UploadFileRequest1Private.h"

BEGIN_NAMESPACE_ZFTPNS

UploadFileRequest1::UploadFileRequest1(QObject *parent) : ZFtpRequest(parent),
    d_ptr(new UploadFileRequest1Private)
{
    d_ptr->q_ptr = this;
}

void UploadFileRequest1::setUploadParam(const QByteArray &data, const QString &ftp_filepath)
{
    d_ptr->m_data = data;
    d_ptr->m_dest_ftp_filepath = ftp_filepath;
}

END_NAMESPACE
