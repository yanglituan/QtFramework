﻿#ifndef LOGINREQUESTPRIVATE_H
#define LOGINREQUESTPRIVATE_H

#include <QObject>
#include "ZFtpRequestPrivate.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class LoginRequest;

class LoginRequestPrivate : public ZFtpRequestPrivate
{
    Q_DECLARE_PUBLIC_D(q_ptr.data(), LoginRequest)

public:
    explicit LoginRequestPrivate(QObject *parent = 0);

    virtual int commandID() const;
    virtual int doRequest(QSharedPointer<QFtp>, QSharedPointer<ZFtpConfig> cfg);
    virtual QPointer<ZFtpRequest> publicObject();
    virtual ZFtpResponsePrivate *createResponse();

private:
    QString m_username;
    QString m_password;

    QPointer<LoginRequest> q_ptr;
};

END_NAMESPACE

#endif // LOGINREQUESTPRIVATE_H
