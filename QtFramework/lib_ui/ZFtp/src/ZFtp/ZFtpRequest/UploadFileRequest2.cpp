﻿#include "UploadFileRequest2.h"
#include "UploadFileRequest2Private.h"

BEGIN_NAMESPACE_ZFTPNS

UploadFileRequest2::UploadFileRequest2(QObject *parent) : ZFtpRequest(parent),
    d_ptr(new UploadFileRequest2Private)
{
    d_ptr->q_ptr = this;
}

void UploadFileRequest2::setUploadParam(QIODevice *dev, const QString &ftp_filepath)
{
    d_ptr->dev = dev;
    d_ptr->m_dest_ftp_filepath = ftp_filepath;
}

END_NAMESPACE
