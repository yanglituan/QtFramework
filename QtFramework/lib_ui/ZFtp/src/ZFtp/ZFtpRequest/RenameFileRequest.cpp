﻿#include "RenameFileRequest.h"
#include "RenameFileRequestPrivate.h"

BEGIN_NAMESPACE_ZFTPNS

RenameFileRequest::RenameFileRequest(QObject *parent) : ZFtpRequest(parent),
    d_ptr(new RenameFileRequestPrivate)
{
    d_ptr->q_ptr = this;
}

void RenameFileRequest::setRenameParam(const QString &old_filename, const QString &new_filename)
{
    d_ptr->m_old_ftp_filepath = old_filename;
    d_ptr->m_new_ftp_filepath = new_filename;
}

END_NAMESPACE
