﻿#include "MkDirRequest.h"
#include "MkDirRequestPrivate.h"

BEGIN_NAMESPACE_ZFTPNS

MkDirRequest::MkDirRequest(QObject *parent) : ZFtpRequest(parent),
    d_ptr(new MkDirRequestPrivate)
{
    d_ptr->q_ptr = this;
}

void MkDirRequest::setDirName(const QString &dirname)
{
    d_ptr->m_ftp_dirpath = dirname;
}

END_NAMESPACE
