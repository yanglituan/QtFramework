﻿#ifndef ZFTPGLOBAL_H
#define ZFTPGLOBAL_H

#define BEGIN_NAMESPACE_ZFTPNS namespace ZFtpNS {
#define END_NAMESPACE }

#define Q_DECLARE_PUBLIC_D(Dptr, Class) \
    inline Class* d_func() { return reinterpret_cast<Class *>(Dptr); } \
    inline const Class* d_func() const { return reinterpret_cast<const Class *>(Dptr); } \
    friend class Class;

#endif // ZFTPGLOBAL_H
