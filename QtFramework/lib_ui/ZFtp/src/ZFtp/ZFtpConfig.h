﻿#ifndef ZFTPCONFIG_H
#define ZFTPCONFIG_H

#include <QObject>
#include "ZFtpGlobal.h"

class QTextCodec;

BEGIN_NAMESPACE_ZFTPNS

class ZFtpConfig : public QObject
{
    Q_OBJECT

public:
    explicit ZFtpConfig(QObject *parent = 0);

    /**
     * @brief setTextCodecName
     *  设置假定的Ftp服务器使用的文本编码
     * @param text_codec_name
     */
    void setTextCodecName(const QString &text_codec_name);

    /**
     * @brief textCodecName
     *  获取假定的Ftp使用的文本编码
     * @return
     */
    QString textCodecName() const;

    /**
     * @brief defaultTextCodecName
     *  获取Ftp服务器的默认编码名称
     * @return
     */
    QString defaultTextCodecName() const;

    /**
     * @brief textCodec
     *  获取一个Ftp编码转换器，用于将字符串转换为Ftp识别编码数据
     * @return
     *  返回值保证不为空，没有指定转换器器则使用默认编码转换器
     */
    QTextCodec *ftpServerTextCodec() const;

    /**
     * @brief setAutoReconnect
     */
    void setAutoReconnect(bool);

    /**
     * @brief autoReconnect
     * @return
     */
    bool autoReconnect();

private:
    QString m_text_codec_name;
    bool    m_auto_reconnect;
};

END_NAMESPACE

#endif // ZFTPCONFIG_H
