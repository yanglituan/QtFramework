﻿#include "ZFtpConfig.h"
#include <QTextCodec>

BEGIN_NAMESPACE_ZFTPNS

ZFtpConfig::ZFtpConfig(QObject *parent) : QObject(parent)
{
    m_text_codec_name = defaultTextCodecName();
    m_auto_reconnect = false;
}

void ZFtpConfig::setTextCodecName(const QString &text_codec_name)
{
    m_text_codec_name = text_codec_name;
}

QString ZFtpConfig::textCodecName() const
{
    return m_text_codec_name;
}

QString ZFtpConfig::defaultTextCodecName() const
{
    return QString("GB2312");
}

QTextCodec *ZFtpConfig::ftpServerTextCodec() const
{
    QTextCodec *text_codec = QTextCodec::codecForName(this->textCodecName().toLatin1());
    if (!text_codec)
    {
        text_codec = QTextCodec::codecForName(this->defaultTextCodecName().toLatin1());
    }

    Q_ASSERT(text_codec);
    return text_codec;
}

void ZFtpConfig::setAutoReconnect(bool reconnect)
{
    m_auto_reconnect = reconnect;
}

bool ZFtpConfig::autoReconnect()
{
    return m_auto_reconnect;
}

END_NAMESPACE
