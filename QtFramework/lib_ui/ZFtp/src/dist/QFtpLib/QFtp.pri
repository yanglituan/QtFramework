INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

CONFIG(debug, debug|release){
    LIBS += -L$$PWD -lQFtpd
} else {
    LIBS += -L$$PWD -lQFtp
}

#HEADERS += \
#    $$PWD/qftp.h \
#    $$PWD/qurlinfo.h \
#    $$PWD/QFtpExports.h
