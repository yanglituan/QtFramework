include($$PWD/../QFtpLib/QFtp.pri)

INCLUDEPATH += $$PWD $$PWD/ZFtpRequest $$PWD/ZFtpResponse
DEPENDPATH += $$PWD

HEADERS += \
    $$PWD/ZFtp.h \
    $$PWD/ZFtpExports.h \
    $$PWD/ZFtpGlobal.h \
    $$PWD/ZFtpRequest/CdDirRequest.h \
    $$PWD/ZFtpRequest/CloseFtpRequest.h \
    $$PWD/ZFtpRequest/ConnectToHostRequest.h \
    $$PWD/ZFtpRequest/DeleteFileRequest.h \
    $$PWD/ZFtpRequest/DownloadFileRequest1.h \
    $$PWD/ZFtpRequest/DownloadFileRequest2.h \
    $$PWD/ZFtpRequest/ListDirRequest.h \
    $$PWD/ZFtpRequest/LoginRequest.h \
    $$PWD/ZFtpRequest/MkDirRequest.h \
    $$PWD/ZFtpRequest/RenameFileRequest.h \
    $$PWD/ZFtpRequest/RmDirRequest.h \
    $$PWD/ZFtpRequest/UploadFileRequest1.h \
    $$PWD/ZFtpRequest/UploadFileRequest2.h \
    $$PWD/ZFtpRequest/UploadFileRequest3.h \
    $$PWD/ZFtpRequest/ZFtpRequest.h \
    $$PWD/ZFtpResponse/CdDirResponse.h \
    $$PWD/ZFtpResponse/CloseFtpResponse.h \
    $$PWD/ZFtpResponse/ConnectToHostResponse.h \
    $$PWD/ZFtpResponse/DeleteFileResponse.h \
    $$PWD/ZFtpResponse/DownloadFileResponse1.h \
    $$PWD/ZFtpResponse/DownloadFileResponse2.h \
    $$PWD/ZFtpResponse/ListDirResponse.h \
    $$PWD/ZFtpResponse/LoginResponse.h \
    $$PWD/ZFtpResponse/MkDirResponse.h \
    $$PWD/ZFtpResponse/RenameFileResponse.h \
    $$PWD/ZFtpResponse/RmDirResponse.h \
    $$PWD/ZFtpResponse/UploadFileResponse1.h \
    $$PWD/ZFtpResponse/UploadFileResponse2.h \
    $$PWD/ZFtpResponse/UploadFileResponse3.h \
    $$PWD/ZFtpResponse/ZFtpResponse.h

CONFIG(debug, debug|release) {
    LIBS += -L$$PWD -lZFtpd
} else {
    LIBS += -L$$PWD -lZFtp
}
