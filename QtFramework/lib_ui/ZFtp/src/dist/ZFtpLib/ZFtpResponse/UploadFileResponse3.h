﻿#ifndef UPLOADFILERESPONSE3_H
#define UPLOADFILERESPONSE3_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpResponse.h"
#include "ZFtpExports.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class UploadFileResponse3Private;

class ZFTP_API UploadFileResponse3 : public ZFtpResponse
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(UploadFileResponse3)

public:
    explicit UploadFileResponse3(QObject *parent = 0);

public:
    QSharedPointer<UploadFileResponse3Private> d_ptr;
    virtual QString lastErrorString();

signals:
    void sigUploadProgress(qint64 done, qint64 total);
    void sigResult(bool result);
};

END_NAMESPACE

#endif // UPLOADFILERESPONSE3_H
