﻿#ifndef RENAMEFILERESPONSE_H
#define RENAMEFILERESPONSE_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpExports.h"
#include "ZFtpGlobal.h"
#include "ZFtpResponse.h"

BEGIN_NAMESPACE_ZFTPNS

class RenameFileResponsePrivate;

class ZFTP_API RenameFileResponse : public ZFtpResponse
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(RenameFileResponse)

public:
    explicit RenameFileResponse(QObject *parent = 0);
    virtual QString lastErrorString();

public:
    QSharedPointer<RenameFileResponsePrivate> d_ptr;

signals:
    void sigResult(bool success);
};

END_NAMESPACE

#endif // RENAMEFILERESPONSE_H
