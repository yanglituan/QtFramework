﻿#ifndef CDDIRRESPONSE_H
#define CDDIRRESPONSE_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpResponse.h"
#include "ZFtpGlobal.h"
#include "ZFtpExports.h"

BEGIN_NAMESPACE_ZFTPNS

class CdDirResponsePrivate;

class ZFTP_API CdDirResponse : public ZFtpResponse
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(CdDirResponse)

public:
    explicit CdDirResponse(QObject *parent = 0);
    virtual QString lastErrorString();

public:
    QSharedPointer<CdDirResponsePrivate> d_ptr;

signals:
    void sigResult(bool);
};

END_NAMESPACE

#endif // CDDIRRESPONSE_H
