﻿#ifndef LOGINRESPONSE_H
#define LOGINRESPONSE_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpResponse.h"
#include "ZFtpExports.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class LoginResponsePrivate;

class ZFTP_API LoginResponse : public ZFtpResponse
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(LoginResponse)

public:
    explicit LoginResponse(QObject *parent = 0);
    virtual QString lastErrorString();

public:
    QSharedPointer<LoginResponsePrivate> d_ptr;

signals:
    /**
     * @brief sigResult
     *  结果信号
     * @param success
     *  标记登录操作是否成功
     */
    void sigResult(bool success);
};

END_NAMESPACE

#endif // LOGINRESPONSE_H
