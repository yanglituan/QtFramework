﻿#ifndef MKDIRRESPONSE_H
#define MKDIRRESPONSE_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpResponse.h"
#include "ZFtpExports.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class MkDirResponsePrivate;

class ZFTP_API MkDirResponse : public ZFtpResponse
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(MkDirResponse)

public:
    explicit MkDirResponse(QObject *parent = 0);
    virtual QString lastErrorString();

public:
    QSharedPointer<MkDirResponsePrivate> d_ptr;

signals:
    void sigResult(bool success);
};

END_NAMESPACE

#endif // MKDIRRESPONSE_H
