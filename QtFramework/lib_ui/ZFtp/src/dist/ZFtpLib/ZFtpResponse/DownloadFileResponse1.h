﻿#ifndef DOWNLOADFILERESPONSE1_H
#define DOWNLOADFILERESPONSE1_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpExports.h"
#include "ZFtpGlobal.h"
#include "ZFtpResponse.h"

BEGIN_NAMESPACE_ZFTPNS

class DownloadFileResponse1Private;

class ZFTP_API DownloadFileResponse1 : public ZFtpResponse
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(DownloadFileResponse1)

public:
    explicit DownloadFileResponse1(QObject *parent = 0);
    virtual QString lastErrorString();

public:
    QSharedPointer<DownloadFileResponse1Private> d_ptr;

signals:
    void sigDownloadProgress(qint64 done, qint64 total);
    void sigResult(bool success);
};

END_NAMESPACE

#endif // DOWNLOADFILERESPONSE1_H
