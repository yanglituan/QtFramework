﻿#ifndef UPLOADFILERESPONSE2_H
#define UPLOADFILERESPONSE2_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpResponse.h"
#include "ZFtpExports.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class UploadFileResponse2Private;

class ZFTP_API UploadFileResponse2 : public ZFtpResponse
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(UploadFileResponse2)

public:
    explicit UploadFileResponse2(QObject *parent = 0);
    virtual QString lastErrorString();

public:
    QSharedPointer<UploadFileResponse2Private> d_ptr;

signals:
    void sigUploadProgress(qint64 done, qint64 total);
    void sigResult(bool result);
};

END_NAMESPACE

#endif // UPLOADFILERESPONSE2_H
