﻿#ifndef UPLOADFILERESPONSE1_H
#define UPLOADFILERESPONSE1_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpResponse.h"
#include "ZFtpExports.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class UploadFileResponse1Private;

class ZFTP_API UploadFileResponse1 : public ZFtpResponse
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(UploadFileResponse1)

public:
    explicit UploadFileResponse1(QObject *parent = 0);
    virtual QString lastErrorString();

public:
    QSharedPointer<UploadFileResponse1Private> d_ptr;

signals:
    void sigUploadProgress(qint64 done, qint64 total);
    void sigResult(bool success);
};

END_NAMESPACE

#endif // UPLOADFILERESPONSE1_H
