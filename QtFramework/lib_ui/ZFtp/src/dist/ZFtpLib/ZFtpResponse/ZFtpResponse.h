﻿#ifndef ZFTPRESPONSE_H
#define ZFTPRESPONSE_H

#include <QSharedPointer>
#include <QVariant>
#include "ZFtpRequest/ZFtpRequest.h"
#include "ZFtpExports.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class ZFtpResponsePrivate;

// Ftp响应基类 ==================================================================
class ZFTP_API ZFtpResponse : public QObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(ZFtpResponse)

public:
    explicit ZFtpResponse(QObject *parent = NULL);
    virtual ~ZFtpResponse() {}

    /**
     * @brief lastErrorString
     *  支持错误信息获取
     * @return
     */
    virtual QString lastErrorString() = 0;

signals:
    void sigRequestQueued();
    void sigRequestStarted();
    void sigRequestFinished(bool);
};

END_NAMESPACE

#include "ConnectToHostResponse.h"
#include "LoginResponse.h"
#include "CloseFtpResponse.h"
#include "ListDirResponse.h"
#include "MkDirResponse.h"
#include "RmDirResponse.h"
#include "CdDirResponse.h"
#include "DeleteFileResponse.h"
#include "RenameFileResponse.h"
#include "DownloadFileResponse1.h"
#include "DownloadFileResponse2.h"
#include "UploadFileResponse1.h"
#include "UploadFileResponse2.h"
#include "UploadFileResponse3.h"
#include "DeleteFileResponse.h"

#endif // ZFTPRESPONSE_H
