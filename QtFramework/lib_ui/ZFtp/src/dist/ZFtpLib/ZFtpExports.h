﻿#ifndef ZFTP_GLOBAL_H
#define ZFTP_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(ZFTP_LIB_MAKER)
#  define ZFTP_API Q_DECL_EXPORT
#else
#  define ZFTP_API
#endif

#endif // ZFTP_GLOBAL_H
