﻿#ifndef DOWNLOADFILEREQUEST1_H
#define DOWNLOADFILEREQUEST1_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpRequest.h"
#include "ZFtpGlobal.h"
#include "ZFtpExports.h"

class QIODevice;

BEGIN_NAMESPACE_ZFTPNS

class DownloadFileRequest1Private;

class ZFTP_API DownloadFileRequest1 : public ZFtpRequest
{
public:
    explicit DownloadFileRequest1(QObject *parent = 0);

    void setDownloadFileParam(const QString &ftp_filepath, QIODevice *);

public:
    QSharedPointer<DownloadFileRequest1Private> d_ptr;
};

END_NAMESPACE

#endif // DOWNLOADFILEREQUEST1_H
