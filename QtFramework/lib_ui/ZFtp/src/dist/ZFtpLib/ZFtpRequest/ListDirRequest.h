﻿#ifndef LISTDIRREQUEST_H
#define LISTDIRREQUEST_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpRequest.h"
#include "ZFtpGlobal.h"
#include "ZFtpExports.h"

BEGIN_NAMESPACE_ZFTPNS

class ListDirRequestPrivate;

class ZFTP_API ListDirRequest : public ZFtpRequest
{
    Q_DECLARE_PRIVATE(ListDirRequest)

public:
    explicit ListDirRequest(QObject *parent = 0);

    /**
     * @brief setFtpDirpath
     */
    void setFtpDirpath(const QString &);

public:
    QSharedPointer<ListDirRequestPrivate> d_ptr;
};

END_NAMESPACE

#endif // LISTDIRREQUEST_H
