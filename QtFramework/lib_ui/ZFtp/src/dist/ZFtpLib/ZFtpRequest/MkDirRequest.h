﻿#ifndef MKDIRREQUEST_H
#define MKDIRREQUEST_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpRequest.h"
#include "ZFtpGlobal.h"
#include "ZFtpExports.h"

BEGIN_NAMESPACE_ZFTPNS

class MkDirRequestPrivate;

class ZFTP_API MkDirRequest : public ZFtpRequest
{
    Q_DECLARE_PRIVATE(MkDirRequest)

public:
    explicit MkDirRequest(QObject *parent = 0);

    void setDirName(const QString &dirname);

public:
    QSharedPointer<MkDirRequestPrivate> d_ptr;
};

END_NAMESPACE

#endif // MKDIRREQUEST_H
