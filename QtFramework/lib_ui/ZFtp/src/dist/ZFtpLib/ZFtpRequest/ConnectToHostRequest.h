﻿#ifndef CONNECTTOHOSTREQUEST_H
#define CONNECTTOHOSTREQUEST_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpRequest.h"
#include "ZFtpGlobal.h"
#include "ZFtpExports.h"

BEGIN_NAMESPACE_ZFTPNS

class ConnectToHostRequestPrivate;

class ZFTP_API ConnectToHostRequest : public ZFtpRequest
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(ConnectToHostRequest)

public:
    explicit ConnectToHostRequest(QObject *parent = 0);

    /**
     * @brief setConnectInfo
     *  设置连接信息
     * @param hostname
     * @param port
     */
    void setConnectInfo(const QString &hostname, const quint16 &port = 21);

public:
    QSharedPointer<ConnectToHostRequestPrivate> d_ptr;
};

END_NAMESPACE

#endif // CONNECTTOHOSTREQUEST_H
