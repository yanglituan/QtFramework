﻿#ifndef CLOSEFTPREQUEST_H
#define CLOSEFTPREQUEST_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpRequest.h"
#include "ZFtpGlobal.h"
#include "ZFtpExports.h"

BEGIN_NAMESPACE_ZFTPNS

class CloseFtpRequestPrivate;

class ZFTP_API CloseFtpRequest : public ZFtpRequest
{
    Q_DECLARE_PRIVATE(CloseFtpRequest)

public:
    explicit CloseFtpRequest(QObject *parent = 0);

public:
    QSharedPointer<CloseFtpRequestPrivate> d_ptr;
};

END_NAMESPACE

#endif // CLOSEFTPREQUEST_H
