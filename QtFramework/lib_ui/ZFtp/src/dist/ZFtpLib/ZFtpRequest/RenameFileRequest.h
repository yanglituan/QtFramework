﻿#ifndef RENAMEFILEREQUEST_H
#define RENAMEFILEREQUEST_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpRequest.h"
#include "ZFtpGlobal.h"
#include "ZFtpExports.h"

BEGIN_NAMESPACE_ZFTPNS

class RenameFileRequestPrivate;

class ZFTP_API RenameFileRequest : public ZFtpRequest
{
public:
    explicit RenameFileRequest(QObject *parent = 0);

    void setRenameParam(const QString &old_filename, const QString &new_filename);

public:
    QSharedPointer<RenameFileRequestPrivate> d_ptr;
};

END_NAMESPACE

#endif // RENAMEFILEREQUEST_H
