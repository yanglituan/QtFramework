﻿#ifndef UPLOADFILEREQUEST3_H
#define UPLOADFILEREQUEST3_H

#include "ZFtpRequest.h"
#include "ZFtpExports.h"
#include "ZFtpGlobal.h"
#include <QSharedPointer>

BEGIN_NAMESPACE_ZFTPNS

class UploadFileRequest3Private;

class ZFTP_API UploadFileRequest3 : public ZFtpRequest
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(UploadFileRequest3)

public:
    UploadFileRequest3(QObject *parent = 0);

    /**
     * @brief setUploadParam
     *  设置上传参数
     * @param local_filepath
     *  本地文件路径
     * @param ftp_filepath
     *  ftp目标文件路径
     */
    void setUploadParam(const QString &local_filepath, const QString &ftp_filepath);

private:
    QSharedPointer<UploadFileRequest3Private> d_ptr;
};

END_NAMESPACE

#endif // UPLOADFILEREQUEST3_H
