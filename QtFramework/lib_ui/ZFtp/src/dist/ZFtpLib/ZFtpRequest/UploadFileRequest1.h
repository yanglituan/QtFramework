﻿#ifndef UPLOADFILEREQUEST1_H
#define UPLOADFILEREQUEST1_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpRequest.h"
#include "ZFtpGlobal.h"
#include "ZFtpExports.h"

BEGIN_NAMESPACE_ZFTPNS

class UploadFileRequest1Private;

class ZFTP_API UploadFileRequest1 : public ZFtpRequest
{
    Q_DECLARE_PRIVATE(UploadFileRequest1)

public:
    explicit UploadFileRequest1(QObject *parent = 0);

    void setUploadParam(const QByteArray &data, const QString &ftp_filepath);

public:
    QSharedPointer<UploadFileRequest1Private> d_ptr;
};

END_NAMESPACE

#endif // UPLOADFILEREQUEST1_H
