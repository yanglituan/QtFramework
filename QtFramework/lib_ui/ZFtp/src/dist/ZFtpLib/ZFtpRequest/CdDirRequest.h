﻿#ifndef CDDIRREQUEST_H
#define CDDIRREQUEST_H

#include <QObject>
#include <QSharedPointer>
#include "ZFtpRequest.h"
#include "ZFtpExports.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class CdDirRequestPrivate;

class ZFTP_API CdDirRequest : public ZFtpRequest
{
    Q_DECLARE_PRIVATE(CdDirRequest)

public:
    explicit CdDirRequest(QObject *parent = 0);

    void setFtpDirname(const QString &ftp_dirname);

public:
    QSharedPointer<CdDirRequestPrivate> d_ptr;
};

END_NAMESPACE

#endif // CDDIRREQUEST_H
