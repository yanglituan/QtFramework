﻿#ifndef DELETEFILEREQUEST_H
#define DELETEFILEREQUEST_H

#include <QObject>
#include "ZFtpRequest.h"
#include "ZFtpGlobal.h"
#include "ZFtpExports.h"

BEGIN_NAMESPACE_ZFTPNS

class DeleteFileRequestPrivate;

class ZFTP_API DeleteFileRequest : public ZFtpRequest
{
    Q_DECLARE_PRIVATE(DeleteFileRequest)

public:
    explicit DeleteFileRequest(QObject *parent = 0);

    void setFilepath(const QString &);

public:
    QSharedPointer<DeleteFileRequestPrivate> d_ptr;
};

END_NAMESPACE

#endif // DELETEFILEREQUEST_H
