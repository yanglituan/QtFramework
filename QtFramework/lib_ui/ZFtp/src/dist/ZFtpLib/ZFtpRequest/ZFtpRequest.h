﻿#ifndef ZFtpRequest_H
#define ZFtpRequest_H

#include <QObject>
#include "qftp.h"
#include "ZFtpExports.h"
#include "ZFtpGlobal.h"

BEGIN_NAMESPACE_ZFTPNS

class ZFtpRequestPrivate;

class ZFTP_API ZFtpRequest : public QObject
{
public:
    explicit ZFtpRequest(QObject *parent = NULL);
    virtual ~ZFtpRequest();
};

END_NAMESPACE

#include "LoginRequest.h"
#include "ConnectToHostRequest.h"
#include "CloseFtpRequest.h"
#include "ListDirRequest.h"
#include "MkDirRequest.h"
#include "RmDirRequest.h"
#include "CdDirRequest.h"
#include "DeleteFileRequest.h"
#include "UploadFileRequest1.h"
#include "UploadFileRequest2.h"
#include "UploadFileRequest3.h"
#include "RenameFileRequest.h"
#include "DownloadFileRequest1.h"
#include "DownloadFileRequest2.h"

#endif // ZFtpRequest_H
