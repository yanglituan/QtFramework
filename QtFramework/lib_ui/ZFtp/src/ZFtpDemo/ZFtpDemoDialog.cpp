﻿#include "ZFtpDemoDialog.h"
#include "ui_ZFtpDemoDialog.h"
#include <QFileDialog>
#include <QMessageBox>

ZFtpDemoDialog::ZFtpDemoDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ZFtpDemoDialog)
{
    ui->setupUi(this);
}

ZFtpDemoDialog::~ZFtpDemoDialog()
{
    delete ui;
}

void ZFtpDemoDialog::on_pushButton_connectToHost_clicked()
{
    m_connect_to_host_request.reset(new ZFtpNS::ConnectToHostRequest);
    m_connect_to_host_response.reset(new ZFtpNS::ConnectToHostResponse);

    m_connect_to_host_request->setConnectInfo(ui->lineEdit_hostname->text());
    connect(m_connect_to_host_response.data(), SIGNAL(sigResult(bool)), SLOT(slotConnectToHostResult(bool)));
    m_zftp.connectToHost(m_connect_to_host_request, m_connect_to_host_response);
}

void ZFtpDemoDialog::on_pushButton_closeFtp_clicked()
{
    m_close_request.reset(new ZFtpNS::CloseFtpRequest);
    m_close_response.reset(new ZFtpNS::CloseFtpResponse);

    connect(m_close_response.data(), SIGNAL(sigResult(bool)), SLOT(slotCloseFtpResult(bool)));
    m_zftp.close(m_close_request, m_close_response);
}

void ZFtpDemoDialog::on_pushButton_login_clicked()
{
    m_login_request.reset(new ZFtpNS::LoginRequest);
    m_login_response.reset(new ZFtpNS::LoginResponse);

    m_login_request->setLoginInfo(ui->lineEdit_username->text(), ui->lineEdit_password->text());

    connect(m_login_response.data(), SIGNAL(sigResult(bool)), SLOT(slotLoginFtpResult(bool)));
    m_zftp.login(m_login_request, m_login_response);
}

void ZFtpDemoDialog::on_pushButton_cddir_clicked()
{
    m_cddir_request.reset(new ZFtpNS::CdDirRequest);
    m_cddir_response.reset(new ZFtpNS::CdDirResponse);

    m_cddir_request->setFtpDirname(ui->lineEdit_cddir->text());

    connect(m_cddir_response.data(), SIGNAL(sigResult(bool)), SLOT(slotCdDirFtpResult(bool)));
    connect(m_cddir_response.data(), SIGNAL(sigRequestQueued()), SLOT(slotCdDirRequestQueued()));
    connect(m_cddir_response.data(), SIGNAL(sigRequestStarted()), SLOT(slotCdDirRequestStarted()));
    connect(m_cddir_response.data(), SIGNAL(sigRequestFinished(bool)), SLOT(slotCdDirRequestFinished(bool)));
    m_zftp.cd(m_cddir_request, m_cddir_response);
}

void ZFtpDemoDialog::on_pushButton_deleteFile_clicked()
{
    m_deletefile_request.reset(new ZFtpNS::DeleteFileRequest);
    m_deletefile_response.reset(new ZFtpNS::DeleteFileResponse);

    m_deletefile_request->setFilepath(ui->lineEdit_deletefilepath->text());

    connect(m_deletefile_response.data(), SIGNAL(sigResult(bool)), SLOT(slotDeleteFileFtpResult(bool)));
    m_zftp.deleteFile(m_deletefile_request, m_deletefile_response);
}

void ZFtpDemoDialog::on_pushButton_renameFile_clicked()
{
    m_renamefile_request.reset(new ZFtpNS::RenameFileRequest);
    m_renamefile_response.reset(new ZFtpNS::RenameFileResponse);

    m_renamefile_request->setRenameParam(ui->lineEdit_old_ftp_filepath->text(), ui->lineEdit_new_ftp_filepath->text());

    connect(m_renamefile_response.data(), SIGNAL(sigResult(bool)), SLOT(slotRenameFileFtpResult(bool)));
    m_zftp.renameFile(m_renamefile_request, m_renamefile_response);
}

void ZFtpDemoDialog::on_pushButton_mkdir_clicked()
{
    m_mkdir_request.reset(new ZFtpNS::MkDirRequest);
    m_mkdir_response.reset(new ZFtpNS::MkDirResponse);

    m_mkdir_request->setDirName(ui->lineEdit_foldername->text());

    connect(m_mkdir_response.data(), SIGNAL(sigResult(bool)), SLOT(slotMkDirFtpResult(bool)));
    m_zftp.mkdir(m_mkdir_request, m_mkdir_response);
}

void ZFtpDemoDialog::slotConnectToHostResult(bool result)
{
    QString log = QStringLiteral("连接Ftp结果: %1, 错误信息：%2")
            .arg(result ? QStringLiteral("成功") : QStringLiteral("失败"))
            .arg(m_connect_to_host_response->lastErrorString());
    ui->textBrowser->append(log);
}

void ZFtpDemoDialog::slotCloseFtpResult(bool result)
{
    QString log = QStringLiteral("关闭Ftp结果: %1")
            .arg(result ? QStringLiteral("成功") : QStringLiteral("失败"));
    ui->textBrowser->append(log);
}

void ZFtpDemoDialog::slotLoginFtpResult(bool result)
{
    QString log = QStringLiteral("登录Ftp结果: %1")
            .arg(result ? QStringLiteral("成功") : QStringLiteral("失败"));
    ui->textBrowser->append(log);
}

void ZFtpDemoDialog::slotCdDirFtpResult(bool result)
{
    QString log = QStringLiteral("进入Ftp目录结果: %1")
            .arg(result ? QStringLiteral("成功") : QStringLiteral("失败"));
    ui->textBrowser->append(log);
}

void ZFtpDemoDialog::slotDeleteFileFtpResult(bool result)
{
    QString log = QStringLiteral("删除Ftp文件结果: %1")
            .arg(result ? QStringLiteral("成功") : QStringLiteral("失败"));
    ui->textBrowser->append(log);
}

void ZFtpDemoDialog::slotRenameFileFtpResult(bool result)
{
    QString log = QStringLiteral("重命名Ftp文件结果: %1")
            .arg(result ? QStringLiteral("成功") : QStringLiteral("失败"));
    ui->textBrowser->append(log);
}

void ZFtpDemoDialog::slotMkDirFtpResult(bool result)
{
    QString log = QStringLiteral("创建新文件夹结果: %1")
            .arg(result ? QStringLiteral("成功") : QStringLiteral("失败"));
    ui->textBrowser->append(log);
}

void ZFtpDemoDialog::slotDownloadFileProgress(qint64 done, qint64 total)
{
    if (total != 0)
    {
        qreal progress = done * 1.0 / total * 100;
        QString log = QStringLiteral("下载文件进度: %1")
                .arg(QString::number(progress));
        ui->textBrowser->append(log);
    }
    else
    {
        QString log = QStringLiteral("无法获取下载文件进度");
        ui->textBrowser->append(log);
    }
}

void ZFtpDemoDialog::slotDownloadFileResult(bool result)
{
    // 关闭文件
    m_download_file.close();

    QString log;
    if (result)
    {
        log = QStringLiteral("下载文件结果: 成功");
    }
    else
    {
        log = QStringLiteral("下载文件结果：失败,原因:%1")
                .arg(m_zftp.errorString());
    }

    ui->textBrowser->append(log);
}

void ZFtpDemoDialog::slotUploadFileProgress(qint64 done, qint64 total)
{
    qreal progress = done * 1.0 / total * 100;
    QString log = QStringLiteral("上传文件进度: %1")
            .arg(QString::number(progress));
    ui->textBrowser->append(log);
}

void ZFtpDemoDialog::slotUploadFileResult(bool result)
{
    // 关闭文件
    m_upload_file.close();

    QString log = QStringLiteral("上传文件结果: %1")
            .arg(result ? QStringLiteral("成功") : QStringLiteral("失败"));
    ui->textBrowser->append(log);
}

void ZFtpDemoDialog::slotListInfo(QUrlInfo info)
{
    QString log = QStringLiteral("%1")
            .arg(info.name());
    ui->textBrowser->append(log);
}

void ZFtpDemoDialog::slotListInfoResult(bool result)
{
    QString log = QStringLiteral("列出目录结果: %1")
            .arg(result ? QStringLiteral("成功") : QStringLiteral("失败"));
    ui->textBrowser->append(log);
}

void ZFtpDemoDialog::on_pushButton_dl_browserDir_clicked()
{
    QString save_filepath = QFileDialog::getExistingDirectory(this, QStringLiteral("选择保存路径"));
    ui->lineEdit_downloadfile_savepath->setText(save_filepath);
}

void ZFtpDemoDialog::on_pushButton_ul_browserDir_clicked()
{
    QString open_filepath = QFileDialog::getOpenFileName(this, QStringLiteral("选择需要上传的文件"));
    ui->lineEdit_ul_local_filepath->setText(open_filepath);
}

void ZFtpDemoDialog::on_pushButton_downloadFile_clicked()
{
    // 参数检查
    QString save_filepath = ui->lineEdit_downloadfile_savepath->text();
    QDir save_dirpath(save_filepath);
    if (!save_dirpath.exists())
    {
        QMessageBox::warning(this, QStringLiteral("提示"), QStringLiteral("请选择有效的保存路径"));
        return;
    }

    // 开始下载
    m_dlfile_request.reset(new ZFtpNS::DownloadFileRequest1);
    m_dlfile_response.reset(new ZFtpNS::DownloadFileResponse1);

    // 新建本地保存文件
    QFileInfo ftp_fi(ui->lineEdit_download_ftp_filepath->text());
    m_download_file.setFileName(save_dirpath.absoluteFilePath(ftp_fi.fileName()));
    if (!m_download_file.open(QFile::WriteOnly))
    {
        QString error_info = QStringLiteral("新建本地下载文件%1失败:%2")
                .arg(m_download_file.fileName())
                .arg(m_download_file.errorString());
        QMessageBox::warning(this, QStringLiteral("警告"), error_info);
        return;
    }

    // 设置下载参数
    m_dlfile_request->setDownloadFileParam(ui->lineEdit_download_ftp_filepath->text(), &m_download_file);

    // 开始下载
    connect(m_dlfile_response.data(), SIGNAL(sigResult(bool)), SLOT(slotDownloadFileResult(bool)));
    connect(m_dlfile_response.data(), SIGNAL(sigDownloadProgress(qint64,qint64)), SLOT(slotDownloadFileProgress(qint64,qint64)));
    m_zftp.downloadFile(m_dlfile_request, m_dlfile_response);
}

void ZFtpDemoDialog::on_pushButton_uploadFile_clicked()
{
    // 检查本地文件是否存在
    QFileInfo local_fi(ui->lineEdit_ul_local_filepath->text());
    if (!local_fi.exists())
    {
        QMessageBox::warning(this, QStringLiteral("提示"), QStringLiteral("本地文件不存在"));
        return;
    }

    // 打开本地文件
    m_upload_file.setFileName(local_fi.absoluteFilePath());
    if (!m_upload_file.open(QFile::ReadOnly))
    {
        QString error_info = QStringLiteral("打开本地文件失败: %1")
                .arg(m_upload_file.errorString());
        QMessageBox::warning(this, QStringLiteral("提示"), error_info);
        return;
    }

    if (ui->lineEdit_ul_dest_ftp_filepath->text().isEmpty())
    {
        QMessageBox::warning(this, QStringLiteral("提示"), QStringLiteral("请填写上传到Ftp的目标文件名"));
        return;
    }

    // 开始上传
    m_ulfile_request.reset(new ZFtpNS::UploadFileRequest2);
    m_ulfile_response.reset(new ZFtpNS::UploadFileResponse2);

    m_ulfile_request->setUploadParam(&m_upload_file, ui->lineEdit_ul_dest_ftp_filepath->text());
    connect(m_ulfile_response.data(), SIGNAL(sigUploadProgress(qint64,qint64)), SLOT(slotUploadFileProgress(qint64,qint64)));
    connect(m_ulfile_response.data(), SIGNAL(sigResult(bool)), SLOT(slotUploadFileResult(bool)));
    m_zftp.uploadFile(m_ulfile_request, m_ulfile_response);
}

void ZFtpDemoDialog::on_pushButton_ls_clicked()
{
    if (ui->lineEdit_ls_foldername->text().isEmpty())
    {
        QMessageBox::warning(this, QStringLiteral("提示"), QStringLiteral("请填写目录名"));
        return;
    }

    // 开始
    m_ls_request.reset(new ZFtpNS::ListDirRequest);
    m_ls_response.reset(new ZFtpNS::ListDirResponse);

    m_ls_request->setFtpDirpath(ui->lineEdit_ls_foldername->text());
    connect(m_ls_response.data(), SIGNAL(sigListInfo(QUrlInfo)), SLOT(slotListInfo(QUrlInfo)));
    connect(m_ls_response.data(), SIGNAL(sigResult(bool)), SLOT(slotListInfoResult(bool)));
    m_zftp.listDir(m_ls_request, m_ls_response);
}

void ZFtpDemoDialog::on_pushButton_downloadFile_2_clicked()
{
    // 参数检查
    QString save_filepath = ui->lineEdit_downloadfile_savepath_2->text();
    QDir save_dirpath(save_filepath);
    if (!save_dirpath.exists())
    {
        QMessageBox::warning(this, QStringLiteral("提示"), QStringLiteral("请选择有效的保存路径"));
        return;
    }

    // 开始下载
    static QList<QSharedPointer<ZFtpNS::DownloadFileRequest2> > request_list;
    static QList<QSharedPointer<ZFtpNS::DownloadFileResponse2> > response_list;

    QSharedPointer<ZFtpNS::DownloadFileRequest2> request;
    QSharedPointer<ZFtpNS::DownloadFileResponse2> response;

    request.reset(new ZFtpNS::DownloadFileRequest2);
    response.reset(new ZFtpNS::DownloadFileResponse2);

    request_list.append(request);
    response_list.append(response);

    // 新建本地保存文件
    QFileInfo ftp_fi(ui->lineEdit_download_ftp_filepath_2->text());

    // 设置下载参数
    request->setDownloadFileParam(save_dirpath.absoluteFilePath(ftp_fi.fileName()), ui->lineEdit_download_ftp_filepath_2->text());

    // 开始下载
    connect(response.data(), SIGNAL(sigResult(bool)), SLOT(slotDownloadFileResult(bool)));
    connect(response.data(), SIGNAL(sigDownloadProgress(qint64,qint64)), SLOT(slotDownloadFileProgress(qint64,qint64)));
    m_zftp.downloadFile(request, response);
}

void ZFtpDemoDialog::on_pushButton_dl_browserDir_2_clicked()
{
    QString save_filepath = QFileDialog::getExistingDirectory(this, QStringLiteral("选择保存路径"));
    ui->lineEdit_downloadfile_savepath_2->setText(save_filepath);
}

void ZFtpDemoDialog::on_pushButton_ul_browserDir_2_clicked()
{
    QString open_filepath = QFileDialog::getOpenFileName(this, QStringLiteral("选择需要上传的文件"));
    ui->lineEdit_ul_local_filepath_2->setText(open_filepath);
}

void ZFtpDemoDialog::on_pushButton_uploadFile_2_clicked()
{
    // 检查本地文件是否存在
    QFileInfo local_fi(ui->lineEdit_ul_local_filepath_2->text());
    if (!local_fi.exists())
    {
        QMessageBox::warning(this, QStringLiteral("提示"), QStringLiteral("本地文件不存在"));
        return;
    }

    // 开始上传
    m_ulfile2_request.reset(new ZFtpNS::UploadFileRequest3);
    m_ulfile2_response.reset(new ZFtpNS::UploadFileResponse3);

    m_ulfile2_request->setUploadParam(ui->lineEdit_ul_local_filepath_2->text(), ui->lineEdit_ul_dest_ftp_filepath_2->text());

    connect(m_ulfile2_response.data(), SIGNAL(sigUploadProgress(qint64,qint64)), SLOT(slotUploadFileProgress(qint64,qint64)));
    connect(m_ulfile2_response.data(), SIGNAL(sigResult(bool)), SLOT(slotUploadFileResult(bool)));
    m_zftp.uploadFile(m_ulfile2_request, m_ulfile2_response);
}

void ZFtpDemoDialog::slotCdDirRequestQueued()
{
    ui->textBrowser->append("cddir queued");
}

void ZFtpDemoDialog::slotCdDirRequestStarted()
{
    ui->textBrowser->append("cddir started");
}

void ZFtpDemoDialog::slotCdDirRequestFinished(bool)
{
    ui->textBrowser->append("cddir finished");
}

void ZFtpDemoDialog::on_pushButton_abort_clicked()
{
    m_zftp.abort();
}
