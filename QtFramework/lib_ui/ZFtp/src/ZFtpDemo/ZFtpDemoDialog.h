﻿#ifndef ZFTPDEMODIALOG_H
#define ZFTPDEMODIALOG_H

#include <QDialog>
#include "ZFtp.h"
#include <QFile>

namespace Ui {
class ZFtpDemoDialog;
}

class ZFtpDemoDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ZFtpDemoDialog(QWidget *parent = 0);
    ~ZFtpDemoDialog();

private slots:
    void on_pushButton_connectToHost_clicked();

    void on_pushButton_closeFtp_clicked();

    void on_pushButton_login_clicked();

    void on_pushButton_cddir_clicked();

    void on_pushButton_deleteFile_clicked();

    void on_pushButton_renameFile_clicked();

    void on_pushButton_mkdir_clicked();

private:
    Ui::ZFtpDemoDialog *ui;
    ZFtpNS::ZFtp m_zftp;
    QFile m_upload_file;
    QFile m_download_file;

    // 请求/响应对象
    QSharedPointer<ZFtpNS::ConnectToHostRequest> m_connect_to_host_request;
    QSharedPointer<ZFtpNS::ConnectToHostResponse> m_connect_to_host_response;

    QSharedPointer<ZFtpNS::CloseFtpRequest> m_close_request;
    QSharedPointer<ZFtpNS::CloseFtpResponse> m_close_response;

    QSharedPointer<ZFtpNS::LoginRequest> m_login_request;
    QSharedPointer<ZFtpNS::LoginResponse> m_login_response;

    QSharedPointer<ZFtpNS::CdDirRequest> m_cddir_request;
    QSharedPointer<ZFtpNS::CdDirResponse> m_cddir_response;

    QSharedPointer<ZFtpNS::DeleteFileRequest> m_deletefile_request;
    QSharedPointer<ZFtpNS::DeleteFileResponse> m_deletefile_response;

    QSharedPointer<ZFtpNS::RenameFileRequest> m_renamefile_request;
    QSharedPointer<ZFtpNS::RenameFileResponse> m_renamefile_response;

    QSharedPointer<ZFtpNS::MkDirRequest> m_mkdir_request;
    QSharedPointer<ZFtpNS::MkDirResponse> m_mkdir_response;

    QSharedPointer<ZFtpNS::DownloadFileRequest1> m_dlfile_request;
    QSharedPointer<ZFtpNS::DownloadFileResponse1> m_dlfile_response;

    QSharedPointer<ZFtpNS::UploadFileRequest2> m_ulfile_request;
    QSharedPointer<ZFtpNS::UploadFileResponse2> m_ulfile_response;

    QSharedPointer<ZFtpNS::ListDirRequest> m_ls_request;
    QSharedPointer<ZFtpNS::ListDirResponse> m_ls_response;

    QSharedPointer<ZFtpNS::UploadFileRequest3> m_ulfile2_request;
    QSharedPointer<ZFtpNS::UploadFileResponse3> m_ulfile2_response;

private slots:
    void slotConnectToHostResult(bool);
    void slotCloseFtpResult(bool);
    void slotLoginFtpResult(bool);
    void slotCdDirFtpResult(bool);
    void slotDeleteFileFtpResult(bool);
    void slotRenameFileFtpResult(bool);
    void slotMkDirFtpResult(bool);

    void slotDownloadFileProgress(qint64 done, qint64 total);
    void slotDownloadFileResult(bool);

    void slotUploadFileProgress(qint64 done, qint64 total);
    void slotUploadFileResult(bool);

    void slotListInfo(QUrlInfo);
    void slotListInfoResult(bool);

    void on_pushButton_dl_browserDir_clicked();
    void on_pushButton_ul_browserDir_clicked();
    void on_pushButton_downloadFile_clicked();
    void on_pushButton_uploadFile_clicked();
    void on_pushButton_ls_clicked();
    void on_pushButton_downloadFile_2_clicked();
    void on_pushButton_dl_browserDir_2_clicked();
    void on_pushButton_ul_browserDir_2_clicked();
    void on_pushButton_uploadFile_2_clicked();

    void slotCdDirRequestQueued();
    void slotCdDirRequestStarted();
    void slotCdDirRequestFinished(bool);
    void on_pushButton_abort_clicked();
};

#endif // ZFTPDEMODIALOG_H

