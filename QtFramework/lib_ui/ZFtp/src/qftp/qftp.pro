TEMPLATE = lib
#CONFIG += static
CONFIG += dll
QT = core network
DEFINES += QFTP_DLL_MAKER

# output
DESTDIR = $$PWD/../dist/QFtpLib
CONFIG(debug, debug|release) {
    TARGET = QFtpd
} else {
    TARGET = QFtp
}

# Input
HEADERS += qurlinfo.h \
    QFtpExports.h \
    qftp.h
SOURCES += qftp.cpp qurlinfo.cpp
