﻿#ifndef QFTPEXPORTS_H
#define QFTPEXPORTS_H

#include <QtGlobal>

#ifdef QFTP_DLL_MAKER
#define QFTP_API Q_DECL_EXPORT
#else
#define QFTP_API
#endif

#endif // QFTPEXPORTS_H
