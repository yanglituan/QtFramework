#include "navlistview.h"
#include "qpainter.h"
#include "qfile.h"
#include "qdom.h"
#include "qdebug.h"

/*
 * 函数名：NavDelegate
 * 函数说明: 构造函数
 *
 *
 * 输入参数：parent： 父类指针
 * 输出参数：无
 * 返回值：无
 * */
NavDelegate::NavDelegate(QObject *parent) : QStyledItemDelegate(parent)
{
	nav = (NavListView *)parent;
}

/*
 * 函数名：~NavDelegate
 * 函数说明: 析构函数
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
NavDelegate::~NavDelegate()
{

}


/*
 * 函数名：sizeHint
 * 函数说明: 返回一个被推荐给widget的尺寸
 *
 *
 * 输入参数：option:用于描述用于在视图小部件中绘制项的参数  index：表格索引
 * 输出参数：无
 * 返回值：无
 * */
QSize NavDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	NavModel::TreeNode *node = (NavModel::TreeNode *)index.data(Qt::UserRole).toUInt();

	if (node->level == 1) {
		return QSize(50, 35);
	} else {
		return QSize(50, 28);
	}
}

/*
 * 函数名：paint
 * 函数说明: 绘制函数 设置表格样式，绘制复选框-状态、大小、显示区域等
 *
 *
 * 输入参数： painter：画笔 option:用于描述用于在视图小部件中绘制项的参数  index：表格索引
 * 输出参数：无
 * 返回值：无
 * */
void NavDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	painter->setRenderHint(QPainter::Antialiasing);
	NavModel::TreeNode *node = (NavModel::TreeNode *)index.data(Qt::UserRole).toUInt();

	//绘制背景
	QColor colorBg;

	if (option.state & QStyle::State_Selected) {
		colorBg = nav->getColorBgSelected();
	} else if (option.state & QStyle::State_MouseOver) {
		colorBg = nav->getColorBgHover();
	} else {
		colorBg = nav->getColorBgNormal();
	}

	painter->fillRect(option.rect, colorBg);

	//绘制+-伸缩图片
	if (node->children.size() != 0) {
		QPixmap pix(18, 16);
		pix.fill(Qt::transparent);
		QPainter p(&pix);
		p.setRenderHint(QPainter::Antialiasing);
		int penWidth = 2;

		//根据采用的背景色判断
		QColor icoColorSelected;
		QColor icoColorNormal;
		QColor icoColorHover;

		if (nav->getIcoColorBg()) {
			icoColorSelected = nav->getColorBgNormal();
			icoColorNormal = nav->getColorBgSelected();
			icoColorHover = nav->getColorBgNormal();
		} else {
			icoColorSelected = nav->getColorTextSelected();
			icoColorNormal = nav->getColorTextNormal();
			icoColorHover = nav->getColorTextHover();
		}

		if (nav->getIcoStyle() == NavListView::IcoStyle_Cross) {
			p.setBrush(Qt::NoBrush);

			if (option.state & QStyle::State_Selected) {
				p.setPen(QPen(icoColorSelected, penWidth));
			} else if (option.state & QStyle::State_MouseOver) {
				p.setPen(QPen(icoColorHover, penWidth));
			} else {
				p.setPen(QPen(icoColorNormal, penWidth));
			}

			//绘制+-线条图片
			if (node->collapse) {
				p.drawLine(QPointF(8, 8), QPointF(18, 8));
				p.drawLine(QPointF(12, 4), QPointF(12, 12));
			} else {
				p.drawLine(QPointF(8, 8), QPointF(18, 8));
			}
		} else if (nav->getIcoStyle() == NavListView::IcoStyle_Triangle) {
			p.setPen(Qt::NoPen);

			if (option.state & QStyle::State_Selected) {
				p.setBrush(icoColorSelected);
			} else if (option.state & QStyle::State_MouseOver) {
				p.setBrush(icoColorHover);
			} else {
				p.setBrush(icoColorNormal);
			}

			QVector<QPointF> points;

			//绘制三角形图片
			if (node->collapse) {
				points.append(QPointF(6, 4));
				points.append(QPointF(6, 12));
				points.append(QPointF(16, 8));
			} else {
				points.append(QPointF(6, 4));
				points.append(QPointF(18, 4));
				points.append(QPointF(12, 10));
			}

			p.drawPolygon(points);
		}

		QPixmap img(pix);
		QRect targetRect = option.rect;
		targetRect.setWidth(16);
		targetRect.setHeight(16);
		QPoint c = option.rect.center();
		c.setX(8);
		targetRect.moveCenter(c);
		painter->drawPixmap(targetRect, img, img.rect());
	}

	//绘制条目文字
	QColor colorText;

	if (option.state & QStyle::State_Selected) {
		colorText = nav->getColorTextSelected();
	} else if (option.state & QStyle::State_MouseOver) {
		colorText = nav->getColorTextHover();
	} else {
		colorText = nav->getColorTextNormal();
	}

	painter->setPen(QPen(colorText));

	//绘制文字离左边的距离
	int margin = 25;

	if (node->level == 2) {
		margin = 45;
	}

	QRect rect = option.rect;
	rect.setWidth(rect.width() - margin);
	rect.setX(rect.x() + margin);

	QFont normalFont("Microsoft Yahei", 9);
	painter->setFont(normalFont);
	painter->drawText(rect, Qt::AlignLeft | Qt::AlignVCenter, index.data(Qt::DisplayRole).toString());

	//绘制分隔符线条
	if (nav->getLineVisible()) {
		painter->setPen(QPen(nav->getColorLine(), 1));

		if (node->level == 1 || (node->level == 2 && node->theLast)) {
			painter->drawLine(QPointF(option.rect.x(), option.rect.y() + option.rect.height()),
			                  QPointF(option.rect.x() + option.rect.width(), option.rect.y() + option.rect.height()));
		}
	}

	//绘制提示信息
	QString recordInfo = node->info;

	//如果不需要显示提示信息或者提示信息为空则返回
	if (!nav->getInfoVisible() || recordInfo.isEmpty()) {
		return;
	}

	QPen decorationPen(option.state & QStyle::State_Selected ? nav->getColorBgSelected() : nav->getColorTextSelected());
	QBrush decorationBrush(option.state & QStyle::State_Selected ? nav->getColorTextSelected() : nav->getColorBgSelected());
	QFont decorationFont("Microsoft Yahei", 8);
	painter->setFont(decorationFont);

	//绘制提示信息背景
	QRect decoration = option.rect;
	decoration.setHeight(15);
	decoration.moveCenter(option.rect.center());
	decoration.setLeft(option.rect.right() - 45);
	decoration.setRight(option.rect.right() - 5);

	painter->setPen(decorationPen);
	QPainterPath path;
	path.addRoundedRect(decoration, 7, 7);
	painter->fillPath(path, decorationBrush);

	//如果是数字则将超过999的数字显示成 999+
	if (recordInfo.toInt() > 999) {
		recordInfo = "999+";
	}

	//如果显示的提示信息长度超过4则将多余显示成省略号..
	if (recordInfo.length() > 4) {
		recordInfo = recordInfo.mid(0, 4) + "..";
	}

	painter->drawText(decoration, Qt::AlignCenter, recordInfo);
}

/*
 * 函数名：NavModel
 * 函数说明: 构造函数
 *
 *
 * 输入参数：parent： 父类指针
 * 输出参数：无
 * 返回值：无
 * */
NavModel::NavModel(QObject *parent)	: QAbstractListModel(parent)
{

}

/*
 * 函数名：~NavModel
 * 函数说明: 析构函数
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
NavModel::~NavModel()
{
	for (std::vector<TreeNode *>::iterator it = treeNode.begin(); it != treeNode.end();) {
		for (std::list<TreeNode *>::iterator child = (*it)->children.begin(); child != (*it)->children.end();) {
			delete(*child);
			child = (*it)->children.erase(child);
		}

		delete(*it);
		it = treeNode.erase(it);
	}
}


/*
 * 函数名：readData
 * 函数说明: 读取数据
 *
 *
 * 输入参数：path：文件路径
 * 输出参数：无
 * 返回值：无
 * */
void NavModel::readData(QString path)
{
	QFile xml(path);

	if (!xml.open(QIODevice::ReadOnly | QIODevice::Text)) {
		return;
	}

	QDomDocument doc;

	if (!doc.setContent(&xml, false)) {
		return;
	}

	treeNode.clear();
	listNode.clear();

	QDomNode root = doc.documentElement().firstChildElement("layout");
	QDomNodeList children = root.childNodes();

	for (int i = 0; i != children.count(); ++i) {
		QDomElement nodeInfo = children.at(i).toElement();
		TreeNode *node = new TreeNode;
		node->label = nodeInfo.attribute("label");
		node->collapse = nodeInfo.attribute("collapse").toInt();
		node->info = nodeInfo.attribute("info");
		node->level = 1;

		QDomNodeList secondLevel = nodeInfo.childNodes();

		for (int j = 0; j != secondLevel.count(); ++j) {
			QDomElement secNodeInfo = secondLevel.at(j).toElement();
			TreeNode *secNode = new TreeNode;
			secNode->label = secNodeInfo.attribute("label");
			secNode->info = secNodeInfo.attribute("info");
			secNode->collapse = false;
			secNode->level = 2;
			secNode->theLast = (j == secondLevel.count() - 1 && i != children.count() - 1);
			node->children.push_back(secNode);
		}

		treeNode.push_back(node);
	}

	refreshList();
	beginResetModel();
	endResetModel();
}


/*
 * 函数名：setData
 * 函数说明: 设置数据
 *
 *
 * 输入参数：listItem：数据存储的list列表
 * 输出参数：无
 * 返回值：无
 * */
void NavModel::setData(QStringList listItem)
{
	int count = listItem.count();

	if (count == 0) {
		return;
	}

	treeNode.clear();
	listNode.clear();

	//listItem格式: 标题|父节点标题(父节点为空)|是否展开|提示信息
	for (int i = 0; i < count; i++) {
		QString item = listItem.at(i);
		QStringList list = item.split("|");

		if (list.count() < 4) {
			continue;
		}

		//首先先将父节点即父节点标题为空的元素加载完毕
		QString title = list.at(0);
		QString fatherTitle = list.at(1);
		QString collapse = list.at(2);
		QString info = list.at(3);

		if (fatherTitle.isEmpty()) {
			TreeNode *node = new TreeNode;
			node->label = title;
			node->collapse = collapse.toInt();
			node->info = info;
			node->level = 1;

			//查找该父节点是否有对应子节点,有则加载
			for (int j = 0; j < count; j++) {
				QString secItem = listItem.at(j);
				QStringList secList = secItem.split("|");

				if (secList.count() < 4) {
					continue;
				}

                QString secTitle = secList.at(0);
                QString secFatherTitle = secList.at(1);
                QString secInfo = secList.at(3);

				if (secFatherTitle == title) {
					TreeNode *secNode = new TreeNode;
					secNode->label = secTitle;
					secNode->info = secInfo;
					secNode->collapse = false;
					secNode->level = 2;
					secNode->theLast = (j == count - 1);
					node->children.push_back(secNode);
				}
			}

			treeNode.push_back(node);
		}
	}

	refreshList();
	beginResetModel();
	endResetModel();
}

/*
 * 函数名：rowCount
 * 函数说明: 获取父节点下面的子节点数量
 *
 *
 * 输入参数：parent：父节点索引
 * 输出参数：无
 * 返回值：无
 * */
int NavModel::rowCount(const QModelIndex &parent) const
{
	return listNode.size();
}

/*
 * 函数名：data
 * 函数说明: 获取节点数据
 *
 *
 * 输入参数：index：节点索引 role：角色
 * 输出参数：无
 * 返回值：无
 * */
QVariant NavModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid()) {
		return QVariant();
	}

	if (index.row() >= listNode.size() || index.row() < 0) {
		return QVariant();
	}

	if (role == Qt::DisplayRole) {
		return listNode[index.row()].label;
	} else if (role == Qt::UserRole) {
		return (unsigned int)(listNode[index.row()].treeNode);
	}

	return QVariant();
}

/*
 * 函数名：refreshList
 * 函数说明: 刷新list节点数据
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
void NavModel::refreshList()
{
	listNode.clear();

	for (std::vector<TreeNode *>::iterator it = treeNode.begin(); it != treeNode.end(); ++it) {
		ListNode node;
		node.label = (*it)->label;
		node.treeNode = *it;

		listNode.push_back(node);

		if ((*it)->collapse) {
			continue;
		}

		for (std::list<TreeNode *>::iterator child = (*it)->children.begin(); child != (*it)->children.end(); ++child) {
			ListNode node;
			node.label = (*child)->label;
			node.treeNode = *child;
			node.treeNode->theLast = false;
			listNode.push_back(node);
		}

		if (!listNode.empty()) {
			listNode.back().treeNode->theLast = true;
		}
	}
}

/*
 * 函数名：collapse
 * 函数说明: 折叠节点
 *
 *
 * 输入参数：index：节点索引
 * 输出参数：无
 * 返回值：无
 * */
void NavModel::collapse(const QModelIndex &index)
{
	TreeNode *node = listNode[index.row()].treeNode;

	if (node->children.size() == 0) {
		return;
	}

	node->collapse = !node->collapse;
	refreshList();

	if (!node->collapse) {
		beginInsertRows(QModelIndex(), index.row() + 1, index.row() + node->children.size());
		endInsertRows();
	} else {
		beginRemoveRows(QModelIndex(), index.row() + 1, index.row() + node->children.size());
		endRemoveRows();
	}
}


/*
 * 函数名：NavListView
 * 函数说明: 构造函数
 *
 *
 * 输入参数：parent：父类指针
 * 输出参数：无
 * 返回值：无
 * */
NavListView::NavListView(QWidget *parent) : QListView(parent)
{
	infoVisible = true;
	lineVisible = true;
	icoColorBg = false;

	style = NavListView::IcoStyle_Cross;

	colorLine = QColor(214, 216, 224);

	colorBgNormal = QColor(239, 241, 250);
	colorBgSelected = QColor(133, 153, 216);
	colorBgHover = QColor(209, 216, 240);

	colorTextNormal = QColor(58, 58, 58);
	colorTextSelected = QColor(255, 255, 255);
	colorTextHover = QColor(59, 59, 59);

	this->setMouseTracking(true);
	model = new NavModel(this);
	delegate = new NavDelegate(this);
	connect(this, SIGNAL(doubleClicked(QModelIndex)), model, SLOT(collapse(QModelIndex)));
}


/*
 * 函数名：~NavListView
 * 函数说明: 析构函数
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
NavListView::~NavListView()
{
	delete model;
	delete delegate;
}

/*
 * 函数名：readData
 * 函数说明: 读取数据
 *
 *
 * 输入参数：xmlPath：xml文件路径
 * 输出参数：无
 * 返回值：无
 * */
void NavListView::readData(QString xmlPath)
{
	model->readData(xmlPath);
	this->setModel(model);
	this->setItemDelegate(delegate);
}


/*
 * 函数名：setData
 * 函数说明: 设置数据
 *
 *
 * 输入参数：listItem：数据存储的list列表
 * 输出参数：无
 * 返回值：无
 * */
void NavListView::setData(QStringList listItem)
{
	model->setData(listItem);
	this->setModel(model);
	this->setItemDelegate(delegate);
}


/*
 * 函数名：setInfoVisible
 * 函数说明: 设置是否显示提示信息
 *
 *
 * 输入参数：infoVisible：true：显示提示信息 false：不显示提示信息
 * 输出参数：无
 * 返回值：无
 * */
void NavListView::setInfoVisible(bool infoVisible)
{
	this->infoVisible = infoVisible;
}


/*
 * 函数名：setLineVisible
 * 函数说明: 设置是否显示间隔线条
 *
 *
 * 输入参数：lineVisible：true：显示间隔线条 false：不显示间隔线条
 * 输出参数：无
 * 返回值：无
 * */
void NavListView::setLineVisible(bool lineVisible)
{
	this->lineVisible = lineVisible;
}


/*
 * 函数名：setIcoColorBg
 * 函数说明: 设置伸缩图片是否采用背景色
 *
 *
 * 输入参数：icoColorBg：true：采用背景色 false：无背景色
 * 输出参数：无
 * 返回值：无
 * */
void NavListView::setIcoColorBg(bool icoColorBg)
{
	this->icoColorBg = icoColorBg;
}


/*
 * 函数名：setIcoStyle
 * 函数说明: 设置伸缩图片样式
 *
 *
 * 输入参数：style：IcoStyle_Cross:设置展开箭头为- IcoStyle_Triangle:设置展开箭头为三角形
 * 输出参数：无
 * 返回值：无
 * */
void NavListView::setIcoStyle(NavListView::IcoStyle style)
{
	this->style = style;
}


/*
 * 函数名：setColorLine
 * 函数说明: 设置间隔线条的颜色
 *
 *
 * 输入参数：colorLine：间隔线条的颜色
 * 输出参数：无
 * 返回值：无
 * */
void NavListView::setColorLine(QColor colorLine)
{
	this->colorLine = colorLine;
}


/*
 * 函数名：setColorBg
 * 函数说明: 设置背景各种状态下颜色
 *
 *
 * 输入参数：colorBgNormal：正常状态下的颜色  colorBgSelected：选中状态下的颜色  colorBgHover：划过状态下的颜色
 * 输出参数：无
 * 返回值：无
 * */
void NavListView::setColorBg(QColor colorBgNormal, QColor colorBgSelected, QColor colorBgHover)
{
	this->colorBgNormal = colorBgNormal;
	this->colorBgSelected = colorBgSelected;
	this->colorBgHover = colorBgHover;
}


/*
 * 函数名：setColorText
 * 函数说明: 设置文本各种状态下颜色
 *
 *
 * 输入参数：colorTextNormal：正常状态下的颜色  colorTextSelected：选中状态下的颜色  colorTextHover：划过状态下的颜色
 * 输出参数：无
 * 返回值：无
 * */
void NavListView::setColorText(QColor colorTextNormal, QColor colorTextSelected, QColor colorTextHover)
{
	this->colorTextNormal = colorTextNormal;
	this->colorTextSelected = colorTextSelected;
	this->colorTextHover = colorTextHover;
}
