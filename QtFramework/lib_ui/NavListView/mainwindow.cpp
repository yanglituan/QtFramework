#include "mainwindow.h"
#include "ui_mainwindow.h"

/*
 * 函数名：MainWindow
 * 函数说明: 构造函数
 *
 *
 * 输入参数：parent： 父类指针
 * 输出参数：无
 * 返回值：无
 * */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

/*
 * 函数名：~MainWindow
 * 函数说明: 析构函数
 *
 *
 * 输入参数：无
 * 输出参数：无
 * 返回值：无
 * */
MainWindow::~MainWindow()
{
    delete ui;
}
