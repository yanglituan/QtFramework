#ifndef FRMMAINWIN_H
#define FRMMAINWIN_H

#include <QWidget>

namespace Ui {
class FrmMainWin;
}

class FrmMainWin : public QWidget
{
    Q_OBJECT

public:
    explicit FrmMainWin(QWidget *parent = 0);
    ~FrmMainWin();

private:
    Ui::FrmMainWin *ui;
};

#endif // FRMMAINWIN_H
