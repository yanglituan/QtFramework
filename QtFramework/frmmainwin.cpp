#include "frmmainwin.h"
#include "ui_frmmainwin.h"

FrmMainWin::FrmMainWin(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FrmMainWin)
{
    ui->setupUi(this);
}

FrmMainWin::~FrmMainWin()
{
    delete ui;
}
