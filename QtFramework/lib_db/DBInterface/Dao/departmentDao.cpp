#include "departmentDao.h"

/***************************************************
 * 名称:DepartmentDao
 * 功能:构造函数
 * 输入:parent对象
 * 输出:无
 * 备注:
 **************************************************/
DepartmentDao::DepartmentDao(QObject *parent) : BaseDao(parent)
{

}

/***************************************************
 * 名称:departmentList
 * 功能:查询部门所有信息
 * 输入:无
 * 输出:部门信息列表
 * 备注:
 **************************************************/
QList<DepartmentInfo> DepartmentDao::departmentList()
{
    //QString sql = "select * from DepartmentInfo";
    //QList<QVariantMap> result = sqlDML.queryList(sql);
    QList<QVariantMap> result = selectToList("DepartmentInfo");
    QList<DepartmentInfo> deps ;
    for(QVariantMap map : result)
    {
        int dep_id = map["dep_id"].toInt();

        QString dep_name = map["dep_name"].toString();

        DepartmentInfo depInfo;
        depInfo.dep_id = dep_id;
        depInfo.dep_name = dep_name;
        deps<<depInfo;
    }

    return deps;
}

/***************************************************
 * 名称:delDep
 * 功能:删除部门指定信息
 * 输入:dep_id 部门编号
 * 输出:-1 编号不正常，0 删除失败，1 删除正常
 * 备注:
 **************************************************/
int DepartmentDao::delDep(int dep_id)
{
    if(dep_id<0)
    {
        return -1;
    }
//    QString sql = "delete from DepartmentInfo where dep_id = ?";

//    if(sqlDML.exec(sql,QVariantList()<<dep_id)){
//        //需要把小组中该部门的所有部门id更新为“其他”
//        int result = groups.updateGroup(dep_id);
//        if(0 == result)
//        {
//            //需要回滚该数据删除操作
//            //...
//        }
//        qDebug() << "删除部门成功。";
//        return 1;
//    }
//    else{
//        qDebug() << "删除部门失败。";
//        return 0;
//    }

    int result = deleteByInfo("DepartmentInfo",QVariantList()<<dep_id,QVariantList()<<"dep_id");
    return result;
}

/***************************************************
 * 名称:delDep
 * 功能:删除部门所有信息
 * 输入:无
 * 输出:0 删除失败，1 删除正常
 * 备注:
 **************************************************/
int DepartmentDao::delDep()
{
    //首先记录所有部门编号
    //QStringList depId = sqlDML.queryOneFieldResult("select dep_id from DepartmentInfo");
    QStringList depId =  selectOneField("DepartmentInfo","dep_id");
    if(depId.size()<0)
    {
        return 1;
    }
    qDebug() << "------DepartmentDao::delDep()------" << depId;

//    QString sql = "delete from DepartmentInfo";
//    if(sqlDML.exec(sql)){
//        for(int i=0;i<depId.size();i++)
//        {
//            int result = groups.updateGroup(depId.at(i).toInt());
//            if(0 == result)
//            {
//                qDebug() << "删除部门所有信息,更新" << depId.at(i) << "部门的小组部门id时失败。";
//                //需要回滚该数据删除操作
//                return 0;
//            }
//        }
//        qDebug() << "删除部门成功。";
//        return 1;
//    }
//    else{
//        qDebug() << "删除部门失败。";
//        return 0;
//    }
    int result = deleteAll("DepartmentInfo");
    return result;
}

/***************************************************
 * 名称:addDep
 * 功能:添加部门信息
 * 输入:部门名称
 * 输出:0 添加失败，1 添加成功
 * 备注:
 **************************************************/
int DepartmentDao::addDep(QString dep_name)
{
//    QString sql = "insert into DepartmentInfo(dep_name) values(?)";

//    if(sqlDML.exec(sql,QVariantList()<<dep_name)){
//        qDebug() << tr("添加部门信息成功。");
//        return 1;
//    }
//    else{
//        qDebug() << tr("添加部门信息失败。");
//        return 0;
//    }
    int result = add("DepartmentInfo",QVariantList()<<dep_name,QVariantList()<<"dep_name");
    return result;
}

/***************************************************
 * 名称:updateDep
 * 功能:更新部门信息
 * 输入:dep_id 部门编号，dep_name 部门名称
 * 输出:0 更新失败，1 更新成功
 * 备注:
 **************************************************/
int DepartmentDao::updateDep(int dep_id, QString dep_name)
{
//    QString sql = "update DepartmentInfo set dep_name = ?  where dep_id = ?";
//    if(sqlDML.exec(sql,QVariantList()<<dep_name<<dep_id)){
//        qDebug() << tr("修改部门信息成功。");
//        return 1;
//    }
//    else{
//        qDebug() << tr("修改部门信息失败。");
//        return 0;
//    }
    int result = update("DepartmentInfo",QVariantList()<<"dep_name",QVariantList()<<dep_name,QVariantList()<<"dep_id",QVariantList()<<dep_id);
    return result;
}

/***************************************************
 * 名称:selOneDep
 * 功能:查询一个部门的信息总数
 * 输入:dep_id 部门编号
 * 输出:记录总数
 * 备注:
 **************************************************/
int DepartmentDao::selOneDep(int dep_id)
{
//    QString sql = "select count(*) from DepartmentInfo where dep_id=?";
//    int result = sqlDML.queryCount(sql,QVariantList()<<dep_id);

    int result = selectRecordCount("DepartmentInfo","",QVariantList()<<"dep_id",QVariantList()<<dep_id);
    return result;
}

/***************************************************
 * 名称:selDepCount
 * 功能:查询一个部门的信息总数
 * 输入:无
 * 输出:记录总数
 * 备注:
 **************************************************/
int DepartmentDao::selDepCount()
{
    //QString sql = "select count(*) from t_department";
    //int result = sqlDML.queryCount(sql);
    int result = selectRecordCount("DepartmentInfo");
    return result;
}

/***************************************************
 * 名称:selPageDepList
 * 功能:分页查询
 * 输入:pageSize 每页显示数，pageNum 第几页
 * 输出:结果集(list类型)
 * 备注:
 **************************************************/
QList<DepartmentInfo> DepartmentDao::selPageDepList(int pageSize, int pageNum)
{
//    QString sql = "";
//    int offset = (pageNum-1)*pageSize;
//    QVariantList param;
//    if("QSQLITE" == g_driver)
//    {
//        //offset代表从第几条记录“之后“开始查询，limit表明查询多少条结果
//        sql = "select * from DepartmentInfo order by dep_id limit ? offset ?";
//        param << pageSize << offset;
//    }
//    else if("QMYSQL" == g_driver)
//    {
//        //limit (2-1)*10,10
//        sql = "select * from DepartmentInfo order by dep_id limit ?,?";
//        param <<offset << pageSize;
//    }

//    QList<QVariantMap> result = sqlDML.queryList(sql,param);

    QList<QVariantMap> result = selectPageList("DepartmentInfo",pageNum,pageSize,QVariantList(),QVariantList(),QVariantList(),"","dep_id");
    QList<DepartmentInfo> deps ;
    for(QVariantMap map : result)
    {
        int dep_id = map["dep_id"].toInt();
        QString dep_name = map["dep_name"].toString();

        DepartmentInfo depInfo;
        depInfo.dep_id = dep_id;
        depInfo.dep_name = dep_name;
        deps<<depInfo;
    }
    return deps;
}
/***************************************************
 * 名称:selPageDepList
 * 功能:分页查询
 * 输入:pageSize 每页显示数，pageNum 第几页
 * 输出:结果集(model类型)
 * 备注:
 **************************************************/
QSqlQueryModel *DepartmentDao::selPageDepModel(int pageSize, int pageNum)
{
    QSqlQueryModel *model = selectPageModel("DepartmentInfo",pageNum,pageSize,QVariantList(),QVariantList(),QVariantList(),"","dep_id");
    return model;
}
