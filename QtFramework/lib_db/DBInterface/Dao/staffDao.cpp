#include "staffDao.h"

/***************************************************
 * 名称:StaffDao
 * 功能:构造函数
 * 输入:parent对象
 * 输出:无
 * 备注:
 **************************************************/
StaffDao::StaffDao(QObject *parent) : BaseDao(parent)
{

}

/***************************************************
 * 名称:staffList
 * 功能:查询所有小组内的信息
 * 输入:无
 * 输出:小组信息列表
 * 备注:
 **************************************************/
QList<StaffInfo> StaffDao::staffList()
{
    //QString sql = "select * from t_staffInfo";
    //QList<QVariantMap> result = sqlDML.queryList(sql);
    QList<QVariantMap> result = selectToList("StaffInfo");

    QList<StaffInfo> staffs ;
    for(QVariantMap map : result)
    {
        int group_id = map["group_id"].toInt();
        int staff_id = map["staff_id"].toInt();
        int staff_sex = map["staff_sex"].toInt();
        QString staff_name = map["staff_name"].toString();

        StaffInfo staffInfo;
        staffInfo.group_id = group_id;
        staffInfo.staff_id = staff_id;
        staffInfo.staff_sex = staff_sex;
        staffInfo.staff_name = staff_name;

        staffs<<staffInfo;
    }

//    StaffInfo sta;
//    selectToLists(&sta);
    return staffs;
}

/***************************************************
 * 名称:staffList
 * 功能:查询一个小组内所有信息
 * 输入:group_id 组编号
 * 输出:小组信息列表
 * 备注:
 **************************************************/
QList<StaffInfo> StaffDao::staffList(int group_id)
{
    //QString sql = "select * from StaffInfo where group_id=?";
    //QList<QVariantMap> result = sqlDML.queryList(sql,QVariantList()<<group_id);
    QList<QVariantMap> result = selectToList("StaffInfo",QVariantList(),QVariantList()<<group_id,QVariantList()<<"group_id");


    QList<StaffInfo> staffs ;
    for(QVariantMap map : result)
    {
        int group_id = map["group_id"].toInt();
        int staff_id = map["staff_id"].toInt();
        int staff_sex = map["staff_sex"].toInt();
        QString staff_name = map["staff_name"].toString();

        StaffInfo staffInfo;
        staffInfo.group_id = group_id;
        staffInfo.staff_id = staff_id;
        staffInfo.staff_sex = staff_sex;
        staffInfo.staff_name = staff_name;

        staffs<<staffInfo;
    }

    return staffs;
}

/***************************************************
 * 名称:staffModel
 * 功能:查询所有小组的信息
 * 输入:无
 * 输出:小组信息列表model
 * 备注:
 **************************************************/
QSqlQueryModel* StaffDao::staffModel()
{
    //QString sql = "select * from StaffInfo";
    //QSqlQueryModel *result = new QSqlQueryModel();
    QSqlQueryModel *result = selectToModel("StaffInfo");
    //result = sqlDML.queryModel(sql);

    return result;
}

/***************************************************
 * 名称:staffModel
 * 功能:查询一个小组的信息
 * 输入:group_id 小组编号
 * 输出:小组信息列表model
 * 备注:
 **************************************************/
QSqlQueryModel* StaffDao::staffModel(int group_id)
{
    //QString sql = "select * from StaffInfo where group_id=?";
    //QSqlQueryModel* result = new QSqlQueryModel();
    QSqlQueryModel* result = selectToModel("StaffInfo",QVariantList(),QVariantList()<<group_id,QVariantList()<<"group_id");
    return result;
}

/***************************************************
 * 名称:delStaff
 * 功能:删除指定员工信息
 * 输入:staff_id 员工编号
 * 输出:-1 编号不正常，0 删除失败，1 删除正常
 * 备注:
 **************************************************/
int StaffDao::delStaff(int staff_id)
{
    if(staff_id<0)
    {
        return -1;
    }
//    QString sql = "delete from StaffInfo where staff_id = ?";

//    if(sqlDML.exec(sql,QVariantList()<<staff_id)){
//        return 1;
//    }
//    else{
//        return 0;
//    }
    int result = deleteByInfo("StaffInfo",QVariantList() << staff_id,QVariantList() << "staff_id");
    return result;
}

/***************************************************
 * 名称:delDep
 * 功能:删除部门所有信息
 * 输入:无
 * 输出:0 删除失败，1 删除正常
 * 备注:
 **************************************************/
int StaffDao::delStaff()
{
    //首先记录所有小组编号
    //QStringList staffId = sqlDML.queryOneFieldResult("select staff_id from StaffInfo");
    QStringList staffId = selectOneField("StaffInfo","staff_id");
    if(staffId.size()<0)
    {
        return 1;
    }
//    qDebug() << "------StaffDao::delStaff()------" << staffId;

//    QString sql = "delete from StaffInfo";
//    if(sqlDML.exec(sql)){
//        qDebug() << tr("删除员工成功。");
//        return 1;
//    }
//    else{
//        qDebug() << tr("删除员工失败。");
//        return 0;
//    }
    int result = deleteAll("StaffInfo");
    return result;
}

/***************************************************
 * 名称:addStaff
 * 功能:添加小组信息
 * 输入:group_id 小组编号,
 *     staff_name 员工名称,
 *     staff_sex  员工性别(0-男，1-女)
 * 输出:-1 参数无效，-2 小组编号冲突，0 失败，1 成功
 * 备注:
 **************************************************/
int StaffDao::addStaff(int group_id, int staff_sex, QString staff_name)
{
    //无效参数
    if(group_id<0 || staff_name.isEmpty())
    {
        return -1;
    }
    qDebug() << "sex:" << staff_sex;
    if(staff_sex>1 && staff_sex<0)
    {
        return -1;
    }

//    //开始插入
//    QString sql = "insert into StaffInfo(group_id,staff_sex,staff_name) values(?,?,?)";
//    qDebug() << sql;
//    if(sqlDML.exec(sql,QVariantList()<<group_id<<staff_sex<<staff_name)){
//        qDebug() << "添加员工成功。" ;
//        return 1;
//    }
//    else{
//        qDebug() << "添加员工失败。";
//        return 0;
//    }
    int result = add("StaffInfo",QVariantList()<<group_id<<staff_sex<<staff_name,QVariantList()<<"group_id"<<"staff_sex"<<"staff_name");
    QList<QVariantList> list;
    QVariantList one;
    one << 5<< 0<< "ade";
    list << one;
    one.clear();
    one << 5<< 1<< "adf";
    list << one;
    one.clear();
    one << 5<< 0<< "adg";
    list << one;
    addMore("StaffInfo",list,QVariantList()<<"group_id"<<"staff_sex"<<"staff_name");
    return result;
}

/***************************************************
 * 名称:updateStaff
 * 功能:更新小组信息
 * 输入:group_id 小组编号，
 *     group_name 小组名称
 *     type  0-同一个部门更新小组信息,1-变更到不同部门，但是编号和名称不变
 * 输出:-1 参数无效，0 失败，1 成功
 * 备注:
 **************************************************/
int StaffDao::updateStaff(int group_id, int staff_id, int staff_sex, QString staff_name)
{
    QString sql = "";
    //无效参数
    if(group_id<0 || staff_id<0 || staff_name.isEmpty())
    {
        return -1;
    }
    if(staff_sex>1 || staff_sex<0)
    {
        return -1;
    }


//    sql = "update StaffInfo set group_id=?,staff_sex=?,staff_name=? where staff_id=?";
//    if(sqlDML.exec(sql,QVariantList()<<group_id<<staff_sex<<staff_name<<staff_id)){
//        qDebug() << "更新员工多个信息成功。" << sql;
//        return 1;
//    }
//    else{
//        qDebug() << "更新员工多个信息失败。" << sql;
//        return 0;
//    }

    int result = update("StaffInfo",QVariantList()<<"group_id"<<"staff_sex"<<"staff_name",QVariantList()<<group_id<<staff_sex<<staff_name,QVariantList()<<"staff_id",QVariantList()<<staff_id);
    return result;
}

/***************************************************
 * 名称:updateStaff
 * 功能:更新小组信息（更新的是同一个小组的同一个信息）
 * 输入:group_id 小组编号，
 * 输出:-1 参数无效，0 失败，1 成功
 * 备注:
 **************************************************/
int StaffDao::updateStaff(int group_id)
{
    QString sql = "";
    //无效参数
    if(group_id<0)
    {
        return -1;
    }
    //QStringList list = sqlDML.queryOneFieldResult("select staff_id from StaffInfo where group_id=?",QVariantList()<<group_id);
    QStringList list = selectOneField("StaffInfo","staff_id",QVariantList()<<group_id,QVariantList()<<"group_id");
    if(list.size()<0)
    {
        return 1;
    }
    for(int i=0;i<list.size();i++)
    {
//        sql = "update StaffInfo set group_id=-1 where staff_id=?";
//        sqlDML.exec(sql,QVariantList()<<list.at(i).toInt());
        update("StaffInfo",QVariantList()<<"group_id",QVariantList()<<"-1",QVariantList()<<"staff_id",QVariantList()<<list.at(i).toInt());
    }
    return 1;
}

/***************************************************
 * 名称:selStaffCount
 * 功能:更新小组信息
 * 输入:无，
 * 输出:组员总记录数
 * 备注:
 **************************************************/
int StaffDao::selStaffCount()
{
//    QString sql = "select count(*) from StaffInfo";
//    int result = sqlDML.queryCount(sql);
    int result = selectRecordCount("StaffInfo");
    return result;
}

/***************************************************
 * 名称:selPageStaffList
 * 功能:分页查询
 * 输入:pageSize 每页显示数，pageNum 第几页
 * 输出:结果集（list类型）
 * 备注:
 **************************************************/
QList<StaffInfo> StaffDao::selPageStaffList(int pageSize, int pageNum)
{
    QList<QVariantMap> result = selectPageList("StaffInfo",pageNum,pageSize,QVariantList(),QVariantList(),QVariantList(),"","staff_id");

    QList<StaffInfo> staffs ;
    for(QVariantMap map : result)
    {
        int group_id = map["group_id"].toInt();
        int staff_id = map["staff_id"].toInt();
        int staff_sex = map["staff_sex"].toInt();
        QString staff_name = map["staff_name"].toString();

        StaffInfo staffInfo;
        staffInfo.group_id = group_id;
        staffInfo.staff_id = staff_id;
        staffInfo.staff_sex = staff_sex;
        staffInfo.staff_name = staff_name;

        staffs<<staffInfo;
    }
    return staffs;
}

/***************************************************
 * 名称:selPageStaffModel
 * 功能:分页查询
 * 输入:pageSize 每页显示数，pageNum 第几页
 * 输出:结果集（model类型）
 * 备注:
 **************************************************/
QSqlQueryModel *StaffDao::selPageStaffModel(int pageSize, int pageNum)
{
    QSqlQueryModel* model = selectPageModel("StaffInfo",pageNum,pageSize,QVariantList(),QVariantList(),QVariantList(),"","staff_id");
    return model;
}
