#include "groupDao.h"

/***************************************************
 * 名称:GroupDao
 * 功能:构造函数
 * 输入:parent对象
 * 输出:无
 * 备注:
 **************************************************/
GroupDao::GroupDao(QObject *parent) : BaseDao(parent)
{
}

/***************************************************
 * 名称:groupList
 * 功能:查询所有小组信息
 * 输入:无
 * 输出:小组信息列表
 * 备注:
 **************************************************/
QList<GroupInfo> GroupDao::groupList()
{
    QList<GroupInfo> groups ;

    //QString sql = "select * from t_groupInfo";
    //QList<QVariantMap> result = sqlDML.queryList(sql);
    QList<QVariantMap> result = selectToList("GroupInfo");

    for(QVariantMap map : result)
    {
        int dep_id = map["dep_id"].toInt();
        int group_id = map["group_id"].toInt();

        QString group_name = map["group_name"].toString();

        GroupInfo groupInfo;
        groupInfo.dep_id = dep_id;
        groupInfo.group_id = group_id;
        groupInfo.group_name = group_name;

        groups<<groupInfo;
    }

    return groups;
}

/***************************************************
 * 名称:groupList
 * 功能:查询一个部门下所有小组信息
 * 输入:dep_id 编号
 * 输出:小组信息列表
 * 备注:
 **************************************************/
QList<GroupInfo> GroupDao::groupList(int dep_id)
{
    QList<GroupInfo> groups ;
    if(dep_id<0)
    {
        return groups;
    }
    //QString sql = "select * from GroupInfo where dep_id=?";
    //QList<QVariantMap> result = sqlDML.queryList(sql,QVariantList()<<dep_id);
    QList<QVariantMap> result = selectToList("GroupInfo",QVariantList(),QVariantList()<<dep_id,QVariantList()<<"dep_id");

    for(QVariantMap map : result)
    {
        int dep_id = map["dep_id"].toInt();
        int group_id = map["group_id"].toInt();

        QString group_name = map["group_name"].toString();

        GroupInfo groupInfo;
        groupInfo.dep_id = dep_id;
        groupInfo.group_id = group_id;
        groupInfo.group_name = group_name;

        groups<<groupInfo;
    }

    return groups;
}

/***************************************************
 * 名称:delDep
 * 功能:删除指定小组信息
 * 输入:group_id 小组编号
 * 输出:-1 编号不正常，0 删除失败，1 删除正常
 * 备注:
 **************************************************/
int GroupDao::delGroup(int group_id)
{
    if(group_id<0)
    {
        return -1;
    }
//    QString sql = "delete from GroupInfo where group_id = ?";

//    if(sqlDML.exec(sql,QVariantList()<<group_id)){
//        int result = staffs.updateStaff(group_id);
//        if(0 == result)
//        {
//            //需要回滚该数据删除操作
//            //...
//        }
//        qDebug() << "删除小组" << group_id << "成功。";
//        return 1;
//    }
//    else{
//        qDebug() << "删除小组" << group_id << "失败。";
//        return 0;
//    }
    int result = deleteByInfo("GroupInfo",QVariantList()<<group_id,QVariantList()<<"group_id");
    return result;
}

/***************************************************
 * 名称:delDep
 * 功能:删除所有小组信息
 * 输入:无
 * 输出:-1 编号不正常，0 删除失败，1 删除正常
 * 备注:
 **************************************************/
int GroupDao::delGroup()
{
    //首先记录所有小组编号
    //QStringList groupId = sqlDML.queryOneFieldResult("select group_id from GroupInfo");
    QStringList groupId = selectOneField("GroupInfo","group_id");
    if(groupId.size()<0)
    {
        return 1;
    }
//    QString sql = "delete from GroupInfo";
//    if(sqlDML.exec(sql)){
//        for(int i=0;i<groupId.size();i++)
//        {
//            QString id = groupId.at(i);
//            qDebug() << id;
//            int result = staffs.updateStaff(id.toInt());
//            if(0 == result)
//            {
//                //需要回滚该数据删除操作
//                //...
//            }
//        }

//        qDebug() << "删除小组成功。";
//        return 1;
//    }
//    else{
//        qDebug() << "删除小组失败。";
//        return 0;
//    }
    int result = deleteAll("GroupInfo");
    return result;
}

/***************************************************
 * 名称:addDep
 * 功能:添加小组信息
 * 输入:小组名称
 * 输出:-1 参数无效，-2 小组编号冲突，0 失败，1 成功
 * 备注:
 **************************************************/
int GroupDao::addGroup(int dep_id,QString group_name)
{
    //无效参数
    if(dep_id<0 || group_name.isEmpty())
    {
        return -1;
    }


    //开始插入
//    QString sql = "insert into t_groupInfo(dep_id,group_name) values(?,?)";
//    if(sqlDML.exec(sql,QVariantList()<<dep_id<<group_name)){
//        qDebug() << "添加小组成功。";
//        return 1;
//    }
//    else{
//        qDebug() << "添加小组失败。";
//        return 0;
//    }
    int result = add("GroupInfo",QVariantList()<<dep_id<<group_name,QVariantList()<<"dep_id"<<"group_name");
    return result;
}

/***************************************************
 * 名称:updateDep
 * 功能:更新小组信息
 * 输入:group_id 小组编号，
 *     group_name 小组名称
 * 输出:-1 参数无效，0 失败，1 成功
 * 备注:
 **************************************************/
int GroupDao::updateGroup(int dep_id,int group_id,QString group_name)
{
    //无效参数
    if(dep_id<0 || group_id<0 || group_name.isEmpty())
    {
        return -1;
    }

//    QString sql = "";
//    sql = "update GroupInfo set dep_id=?,group_name = ?  where group_id=?";
//    if(sqlDML.exec(sql,QVariantList()<<dep_id<<group_name<<group_id)){
//        qDebug() << "更新小组成功。";
//        return 1;
//    }
//    else{
//        qDebug() << "更新小组失败。";
//        return 0;
//    }
    int result = update("GroupInfo",QVariantList()<<"dep_id"<<"group_name",QVariantList()<<dep_id<<group_name,QVariantList()<<"group_id",QVariantList()<<group_id);
    return result;
}


/***************************************************
 * 名称:updateDep
 * 功能:更新小组信息
 * 输入:dep_id 部门编号，
 * 输出:-1 参数无效，0 失败，1 成功
 * 备注:
 **************************************************/
int GroupDao::updateGroup(int dep_id)
{
    QString sql = "";
    //无效参数
    if(dep_id<0)
    {
        return -1;
    }
//    sql = "select group_id from GroupInfo where dep_id=?";
//    QStringList list = sqlDML.queryOneFieldResult(sql,QVariantList()<<dep_id);
    QStringList list = selectOneField("GroupInfo","group_id",QVariantList()<<dep_id,QVariantList()<<"dep_id");
    if(list.size()<0)
    {
        return 1;
    }

    for(int i=0;i<list.size();i++)
    {
        //sql = "update GroupInfo set dep_id=-1 where group_id=?";
        //sqlDML.exec(sql,QVariantList()<<list.at(i).toInt());
        update("GroupInfo",QVariantList()<<"dep_id",QVariantList()<<"-1",QVariantList()<<"group_id",QVariantList()<<list.at(i).toInt());
    }
    return 1;
}

/***************************************************
 * 名称:selAllGroup
 * 功能:查询所有信息记录数
 * 输入:无
 * 输出:记录数
 * 备注:
 **************************************************/
int GroupDao::selGroupCount()
{
//    QString sql = "select count(*) from GroupInfo";
//    int result = sqlDML.queryCount(sql);
//    return result;
    int result = selectRecordCount("GroupInfo");
    return result;
}

/***************************************************
 * 名称:selAllGroup
 * 功能:查询一个小组的所有信息记录数
 * 输入:group_id 小组编号
 * 输出:记录数
 * 备注:
 **************************************************/
int GroupDao::selOneGroup(int group_id)
{
    //QString sql = "select count(*) from t_groupInfo where group_id=?";
    //int result = sqlDML.queryCount(sql,QVariantList()<<group_id);
    int result = selectRecordCount("GroupInfo","",QVariantList()<<"group_id",QVariantList()<<group_id);
    return result;
}

/***************************************************
 * 名称:selPageGroupList
 * 功能:分页查询
 * 输入:pageSize 每页显示数，pageNum 第几页
 * 输出:结果集（list类型）
 * 备注:
 **************************************************/
QList<GroupInfo> GroupDao::selPageGroupList(int pageSize, int pageNum)
{
    QList<QVariantMap> result = selectPageList("GroupInfo",pageNum,pageSize,QVariantList(),QVariantList(),QVariantList(),"","group_id");
    QList<GroupInfo> groups ;
    for(QVariantMap map : result)
    {
        int dep_id = map["dep_id"].toInt();
        int group_id = map["group_id"].toInt();

        QString group_name = map["group_name"].toString();

        GroupInfo groupInfo;
        groupInfo.dep_id = dep_id;
        groupInfo.group_id = group_id;
        groupInfo.group_name = group_name;

        groups<<groupInfo;
    }
    return groups;
}

/***************************************************
 * 名称:selPageGroupModel
 * 功能:分页查询
 * 输入:pageSize 每页显示数，pageNum 第几页
 * 输出:结果集（model类型）
 * 备注:
 **************************************************/
QSqlQueryModel *GroupDao::selPageGroupModel(int pageSize, int pageNum)
{
    QSqlQueryModel *model = selectPageModel("GroupInfo",pageNum,pageSize,QVariantList(),QVariantList(),QVariantList(),"","group_id");
    return model;
}
