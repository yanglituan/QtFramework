/***************************************************
 * 类名:staffDao.h
 * 功能:员工信息dao层
 * 日期:
 * 作者:xhh
 * 备注:
 **************************************************/
#ifndef STAFFSERVICE_H
#define STAFFSERVICE_H

#include <QObject>
#include <QSql>
#include "allEntitys.h"
#include "DB/sqlInterface.h"
#include "baseDao.h"

class StaffDao : public BaseDao
{
    Q_OBJECT
public:
    explicit StaffDao(QObject *parent = 0);

    QList<StaffInfo> staffList();//查询成员所有信息
    QList<StaffInfo> staffList(int group_id);//查询指定成员信息
    QSqlQueryModel* staffModel();//查询成员所有信息
    QSqlQueryModel* staffModel(int group_id);//查询指定成员信息
    int delStaff(int staff_id);//删除成员信息
    int delStaff();//删除所有成员信息
    int addStaff(int group_id, int staff_sex, QString staff_name);//添加成员信息
    int updateStaff(int group_id, int staff_id, int staff_sex, QString group_name);//更新小组信息
    int updateStaff(int group_id);//小组删除时更新所有小组id为空

    int selStaffCount();//查询所有记录数

    //分页查询
    QList<StaffInfo> selPageStaffList(int pageSize,int pageNum);
    QSqlQueryModel*  selPageStaffModel(int pageSize,int pageNum);
public:
    SqlInterface sqlDML;
};

#endif // STAFFSERVICE_H
