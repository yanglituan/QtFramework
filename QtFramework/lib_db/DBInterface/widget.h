#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "Service/serviceDepartment.h"
#include "Service/serviceGroup.h"
#include "Service/serviceStaff.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_pushButton_staffadd_clicked();

    void on_pushButton_depadd_clicked();

    void on_pushButton_depdel_clicked();

    void on_pushButton_depup_clicked();

    void on_pushButton_depsel_clicked();

    void on_pushButton_staffdel_clicked();

    void on_pushButton_staffup_clicked();

    void on_pushButton_staffsel_clicked();

    void on_pushButton_groupadd_clicked();

    void on_pushButton_groupdel_clicked();

    void on_pushButton_groupup_clicked();

    void on_pushButton_groupsel_clicked();

    void on_pushButton_depadd_2_clicked();

    void on_pushButton_depadd_3_clicked();

    void on_pushButton_depadd_4_clicked();

    void on_pushButton_depadd_5_clicked();

    void on_pushButton_depadd_6_clicked();

    void on_pushButton_depadd_7_clicked();

    void on_pushButton_clicked();

private:
    Ui::Widget *ui;
    ServiceStaff      *staff;
    ServiceGroup      *group;
    ServiceDepartment *dep;
};

#endif // WIDGET_H
