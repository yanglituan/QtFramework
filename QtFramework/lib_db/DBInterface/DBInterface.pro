#-------------------------------------------------
#
# Project created by QtCreator 2019-04-29T09:19:46
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DBInterface
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
    DB/sqlBackUp.cpp \
    DB/sqlInterface.cpp \
    DB/sqlHelper.cpp \
    DB/sqlDatabase.cpp \
    Dao/departmentDao.cpp \
    Dao/staffDao.cpp \
    Dao/groupDao.cpp \
    widget.cpp \
    Dao/baseDao.cpp \
    Service/serviceStaff.cpp \
    Service/serviceGroup.cpp \
    Service/serviceDepartment.cpp

HEADERS  += \
    DB/sqlBackUp.h \
    DB/sqlInterface.h \
    DB/sqlDatabase.h \
    DB/sqlHelper.h \
    Dao/departmentDao.h \
    Dao/staffDao.h \
    Dao/groupDao.h \
    allEntitys.h \
    widget.h \
    Dao/baseDao.h \
    Service/serviceStaff.h \
    Service/serviceGroup.h \
    Service/serviceDepartment.h
FORMS    += \
    widget.ui
