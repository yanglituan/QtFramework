#include "serviceDepartment.h"

/***************************************************
 * 名称:ServiceDepartment
 * 功能:构造函数
 * 输入:parent对象
 * 输出:无
 * 备注:
 **************************************************/
ServiceDepartment::ServiceDepartment(QObject *parent) : QObject(parent)
{

}
/***************************************************
 * 名称:departmentList
 * 功能:查询部门所有信息
 * 输入:无
 * 输出:部门信息列表
 * 备注:
 **************************************************/
QList<DepartmentInfo> ServiceDepartment::departmentList()
{
    QList<DepartmentInfo> list = dep.departmentList();
    return list;
}
/***************************************************
 * 名称:delDep
 * 功能:删除部门指定信息
 * 输入:dep_id 部门编号
 * 输出:-1 编号不正常，0 删除失败，1 删除正常
 * 备注:
 **************************************************/
int ServiceDepartment::delDep(int dep_id)
{
    int result = dep.delDep(dep_id);
    return result;
}
/***************************************************
 * 名称:delDep
 * 功能:删除部门所有信息
 * 输入:无
 * 输出:0 删除失败，1 删除正常
 * 备注:
 **************************************************/
int ServiceDepartment::delDep()
{
    int result = dep.delDep();
    return result;
}
/***************************************************
 * 名称:addDep
 * 功能:添加部门信息
 * 输入:部门名称
 * 输出:0 添加失败，1 添加成功
 * 备注:
 **************************************************/
int ServiceDepartment::addDep(QString dep_name)
{
    int result = dep.addDep(dep_name);
    return result;
}
/***************************************************
 * 名称:updateDep
 * 功能:更新部门信息
 * 输入:dep_id 部门编号，dep_name 部门名称
 * 输出:0 更新失败，1 更新成功
 * 备注:
 **************************************************/
int ServiceDepartment::updateDep(int dep_id, QString dep_name)
{
    int result = dep.updateDep(dep_id,dep_name);
    return result;
}

/***************************************************
 * 名称:selDepCount
 * 功能:查询一个部门的信息总数
 * 输入:无
 * 输出:记录总数
 * 备注:
 **************************************************/
int ServiceDepartment::selDepCount()
{
    int result = dep.selDepCount();
    return result;
}

/***************************************************
 * 名称:selOneDep
 * 功能:查询一个部门的信息总数
 * 输入:dep_id 部门编号
 * 输出:记录总数
 * 备注:
 **************************************************/
int ServiceDepartment::selOneDep(int dep_id)
{
    int result = dep.selOneDep(dep_id);
    return result;
}
/***************************************************
 * 名称:selPageGroupList
 * 功能:分页查询
 * 输入:pageSize 页大小，pageNum 当前显示页号
 * 输出:返回查询结果集（list类型）
 * 备注:
 **************************************************/
QList<DepartmentInfo> ServiceDepartment::selPageDepList(int pageSize, int pageNum)
{
    QList<DepartmentInfo> list = dep.selPageDepList(pageSize,pageNum);
    return list;
}

/***************************************************
 * 名称:selPageDepModel
 * 功能:分页查询
 * 输入:pageSize 每页显示数，pageNum 第几页
 * 输出:结果集(model类型)
 * 备注:
 **************************************************/
QSqlQueryModel *ServiceDepartment::selPageDepModel(int pageSize, int pageNum)
{
    QSqlQueryModel *model = dep.selPageDepModel(pageSize,pageNum);
    return model;
}
