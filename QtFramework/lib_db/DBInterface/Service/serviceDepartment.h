/***************************************************
 * 类名:serviceDepartment.h
 * 功能:部门信息逻辑层
 * 日期:
 * 作者:xhh
 * 备注:
 **************************************************/
#ifndef SERVICEDEPARTMENT_H
#define SERVICEDEPARTMENT_H

#include <QObject>
#include "Dao/departmentDao.h"

class ServiceDepartment : public QObject
{
    Q_OBJECT
public:
    explicit ServiceDepartment(QObject *parent = 0);

    QList<DepartmentInfo> departmentList();//查询部门所有信息
    int delDep(int dep_id);//删除部门信息
    int delDep();//删除部门所有信息
    int addDep(QString dep_name);//添加部门信息
    int updateDep(int dep_id,QString dep_name);

    int selDepCount();//查询所有记录数
    int selOneDep(int dep_id);//查询一个部门的所有记录数

    //分页查询
    QList<DepartmentInfo> selPageDepList(int pageSize,int pageNum);
    QSqlQueryModel*       selPageDepModel(int pageSize,int pageNum);
private:
    DepartmentDao dep;
};

#endif // SERVICEDEPARTMENT_H
