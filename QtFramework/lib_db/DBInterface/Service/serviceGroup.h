/***************************************************
 * 类名:serviceGroup.h
 * 功能:小组信息逻辑层
 * 日期:
 * 作者:xhh
 * 备注:
 **************************************************/
#ifndef SERVICEGROUP_H
#define SERVICEGROUP_H

#include <QObject>
#include <QSqlQueryModel>
#include "Dao/groupDao.h"

class ServiceGroup : public QObject
{
    Q_OBJECT
public:
    explicit ServiceGroup(QObject *parent = 0);
    QList<GroupInfo> groupList();//查询所有小组信息
    QList<GroupInfo> groupList(int dep_id);//查询一个部门下的小组信息
    int delGroup(int group_id);//删除小组信息
    int delGroup();//删除所有小组信息
    int addGroup(int dep_id, QString group_name);//添加小组信息
    int updateGroup(int dep_id,int group_id,QString group_name);//更新小组信息
    int updateGroup(int dep_id);//部门删除时更新所有部门id为空

    int selGroupCount();//查询所有记录数
    int selOneGroup(int group_id);//查询一个小组的所有记录数

    //分页查询
    QList<GroupInfo> selPageGroupList(int pageSize,int pageNum);
    QSqlQueryModel*  selPageGroupModel(int pageSize,int pageNum);
private:
    GroupDao group;
};

#endif // SERVICEGROUP_H
