#include "serviceStaff.h"

/***************************************************
 * 名称:ServiceStaff
 * 功能:构造函数
 * 输入:
 * 输出:无
 * 备注:
 **************************************************/
ServiceStaff::ServiceStaff(QObject *parent) : QObject(parent)
{

}

/***************************************************
 * 名称:staffList
 * 功能:获取所有员工信息列表
 * 输入:无
 * 输出:小组信息列表（list类型）
 * 备注:
 **************************************************/
QList<StaffInfo> ServiceStaff::staffList()
{
    QList<StaffInfo> list = staff.staffList();
    return list;
}

/***************************************************
 * 名称:staffList
 * 功能:根据小组编号查询所在小组的员工信息列表
 * 输入:group_id 小组编号
 * 输出:小组信息列表（list类型）
 * 备注:
 **************************************************/
QList<StaffInfo> ServiceStaff::staffList(int group_id)
{
    QList<StaffInfo> list = staff.staffList(group_id);
    return list;
}

/***************************************************
 * 名称:staffModel
 * 功能:获取所有员工信息列表
 * 输入:无
 * 输出:小组信息列表（model类型）
 * 备注:
 **************************************************/
QSqlQueryModel *ServiceStaff::staffModel()
{
    QSqlQueryModel *model = staff.staffModel();
    return model;
}

/***************************************************
 * 名称:staffModel
 * 功能:根据小组编号查询所在小组的员工信息列表
 * 输入:group_id 小组编号
 * 输出:小组信息列表（model类型）
 * 备注:
 **************************************************/
QSqlQueryModel *ServiceStaff::staffModel(int group_id)
{
    QSqlQueryModel *model = staff.staffModel(group_id);
    return model;
}

/***************************************************
 * 名称:staffModel
 * 功能:根据员工编号删除员工信息
 * 输入:staff_id 员工编号
 * 输出:1-成功，0-失败
 * 备注:
 **************************************************/
int ServiceStaff::delStaff(int staff_id)
{
    int result = staff.delStaff(staff_id);
    return result;
}

/***************************************************
 * 名称:delStaff
 * 功能:删除所有员工信息
 * 输入:无
 * 输出:1-成功，0-失败
 * 备注:
 **************************************************/
int ServiceStaff::delStaff()
{
    int result = staff.delStaff();
    return result;
}

/***************************************************
 * 名称:addStaff
 * 功能:添加员工信息
 * 输入:group_id 小组编号
 *     staff_sex 员工性别
 *     staff_name 员工名称
 * 输出:1-成功，0-失败
 * 备注:
 **************************************************/
int ServiceStaff::addStaff(int group_id, int staff_sex, QString staff_name)
{
    int result = staff.addStaff(group_id,staff_sex,staff_name);
    return result;
}

/***************************************************
 * 名称:updateStaff
 * 功能:更新员工信息
 * 输入:group_id 小组编号
 *     staff_id  员工编号
 *     staff_sex 员工性别
 *     staff_name 员工名称
 * 输出:1-成功，0-失败
 * 备注:
 **************************************************/
int ServiceStaff::updateStaff(int group_id, int staff_id, int staff_sex, QString group_name)
{
    int result = staff.updateStaff(group_id,staff_id,staff_sex,group_name);
    return result;
}

/***************************************************
 * 名称:updateStaff
 * 功能:更新员工信息（更新的是同一个小组的同一个信息）
 * 输入:group_id 小组编号
 * 输出:1-成功，0-失败
 * 备注:
 **************************************************/
int ServiceStaff::updateStaff(int group_id)
{
    int result = staff.updateStaff(group_id);
    return result;
}

/***************************************************
 * 名称:selStaffCount
 * 功能:查询记录总数
 * 输入:无
 * 输出:1-记录总数
 * 备注:
 **************************************************/
int ServiceStaff::selStaffCount()
{
    int result = staff.selStaffCount();
    return result;
}

/***************************************************
 * 名称:selPageStaffList
 * 功能:分页查询
 * 输入:pageSize 页大小，pageNum 当前显示页号
 * 输出:返回查询结果集（list类型）
 * 备注:
 **************************************************/
QList<StaffInfo> ServiceStaff::selPageStaffList(int pageSize, int pageNum)
{
    QList<StaffInfo> list = staff.selPageStaffList(pageSize,pageNum);
    return list;
}

/***************************************************
 * 名称:selPageStaffModel
 * 功能:分页查询
 * 输入:pageSize 页大小，pageNum 当前显示页号
 * 输出:返回查询结果集（model类型）
 * 备注:
 **************************************************/
QSqlQueryModel *ServiceStaff::selPageStaffModel(int pageSize, int pageNum)
{
    QSqlQueryModel *model = staff.selPageStaffModel(pageSize,pageNum);
    return model;
}
