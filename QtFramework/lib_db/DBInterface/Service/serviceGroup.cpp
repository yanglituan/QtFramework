#include "serviceGroup.h"

/***************************************************
 * 名称:ServiceGroup
 * 功能:构造函数
 * 输入:
 * 输出:无
 * 备注:
 **************************************************/
ServiceGroup::ServiceGroup(QObject *parent) : QObject(parent)
{

}

/***************************************************
 * 名称:groupList
 * 功能:获取所有部门信息列表
 * 输入:无
 * 输出:小组信息列表（list类型）
 * 备注:
 **************************************************/
QList<GroupInfo> ServiceGroup::groupList()
{
    QList<GroupInfo> list = group.groupList();
    return list;
}

/***************************************************
 * 名称:groupList
 * 功能:根据部门编号查询所有小组的信息列表
 * 输入:dep_id 部门编号
 * 输出:小组信息列表（list类型）
 * 备注:
 **************************************************/
QList<GroupInfo> ServiceGroup::groupList(int dep_id)
{
    QList<GroupInfo> list = group.groupList(dep_id);
    return list;
}

/***************************************************
 * 名称:delStaff
 * 功能:删除指定小组信息
 * 输入:group_id 小组编号
 * 输出:1-成功，0-失败
 * 备注:
 **************************************************/
int ServiceGroup::delGroup(int group_id)
{
    int result = group.delGroup(group_id);
    return result;
}

/***************************************************
 * 名称:delStaff
 * 功能:删除所有小组信息
 * 输入:无
 * 输出:1-成功，0-失败
 * 备注:
 **************************************************/
int ServiceGroup::delGroup()
{
    int result = group.delGroup();
    return result;
}

/***************************************************
 * 名称:addGroup
 * 功能:添加小组信息
 * 输入:dep_id 部门编号
 *     group_name 部门名称
 * 输出:1-成功，0-失败
 * 备注:
 **************************************************/
int ServiceGroup::addGroup(int dep_id, QString group_name)
{
    int result = group.addGroup(dep_id,group_name);
    return result;
}

/***************************************************
 * 名称:updateGroup
 * 功能:更新小组信息
 * 输入:group_id 小组编号
 *     dep_id  部门编号
 *     group_name 小组名称
 * 输出:1-成功，0-失败
 * 备注:
 **************************************************/
int ServiceGroup::updateGroup(int dep_id, int group_id, QString group_name)
{
    int result = group.updateGroup(dep_id,group_id,group_name);
    return result;
}

/***************************************************
 * 名称:updateGroup
 * 功能:更新小组信息（更新的是同一个部门的同一个信息）
 * 输入:dep_id 部门编号
 * 输出:1-成功，0-失败
 * 备注:
 **************************************************/
int ServiceGroup::updateGroup(int dep_id)
{
    int result = group.updateGroup(dep_id);
    return result;
}

/***************************************************
 * 名称:selGroupCount
 * 功能:查询记录总数
 * 输入:无
 * 输出:1-记录总数
 * 备注:
 **************************************************/
int ServiceGroup::selGroupCount()
{
    int result = group.selGroupCount();
    return result;
}

/***************************************************
 * 名称:selOneGroup
 * 功能:查询指定小组记录总数
 * 输入:group_id 小组编号
 * 输出:1-记录总数
 * 备注:
 **************************************************/
int ServiceGroup::selOneGroup(int group_id)
{
    int result = group.selOneGroup(group_id);
    return result;
}

/***************************************************
 * 名称:selPageGroupList
 * 功能:分页查询
 * 输入:pageSize 页大小，pageNum 当前显示页号
 * 输出:返回查询结果集（list类型）
 * 备注:
 **************************************************/
QList<GroupInfo> ServiceGroup::selPageGroupList(int pageSize, int pageNum)
{
    QList<GroupInfo> list = group.selPageGroupList(pageSize,pageNum);
    return list;
}

/***************************************************
 * 名称:selPageGroupModel
 * 功能:分页查询
 * 输入:pageSize 页大小，pageNum 当前显示页号
 * 输出:返回查询结果集（model类型）
 * 备注:
 **************************************************/
QSqlQueryModel *ServiceGroup::selPageGroupModel(int pageSize, int pageNum)
{
    QSqlQueryModel *model = group.selPageGroupModel(pageSize,pageNum);
}
