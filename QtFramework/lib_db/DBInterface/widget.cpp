#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QSqlQueryModel>
#include "allEntitys.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    staff = new ServiceStaff();
    group = new ServiceGroup();
    dep = new ServiceDepartment();
}

Widget::~Widget()
{
    delete ui;
}

/**********************部门操作**********************/
//dep add
void Widget::on_pushButton_depadd_clicked()
{
    dep->addDep("abc");
}

//dep delete
void Widget::on_pushButton_depdel_clicked()
{
    dep->delDep();
}

//dep update
void Widget::on_pushButton_depup_clicked()
{
    dep->updateDep(1,"xxxae");
}

//dep select
void Widget::on_pushButton_depsel_clicked()
{
    QList<DepartmentInfo> list = dep->departmentList();
    for(int i=0;i<list.size();i++)
    {
        qDebug() << "dep select---------"<< i << ",dep_id:" << list.at(i).dep_id
                                              << ",dep_name:" << list.at(i).dep_name;
    }
}

//dep selCount
void Widget::on_pushButton_depadd_2_clicked()
{
    int result = dep->selDepCount();
    qDebug() << "dep select count(*)--all-------" << result;
    result = dep->selOneDep(1);
    qDebug() << "dep select count(*)--one-------" << result;
}


/**********************员工操作**********************/
//staff add
void Widget::on_pushButton_staffadd_clicked()
{
    staff->addStaff(1,1,"baicai");
}
//staff delete
void Widget::on_pushButton_staffdel_clicked()
{
    staff->delStaff(1);
}
//staff update
void Widget::on_pushButton_staffup_clicked()
{
    staff->updateStaff(2,1,0,"ddd");
}
//staff select
void Widget::on_pushButton_staffsel_clicked()
{
    QList<StaffInfo> list = staff->staffList();
    for(int i=0;i<list.size();i++)
    {
        qDebug() << "staff select---"<< i << ",group_id:" << list.at(i).group_id
                                          << ",staff_id:" << list.at(i).staff_id
                                          << ",staff_sex:" << list.at(i).staff_sex
                                          << ",staff_name:" << list.at(i).staff_name;
    }
    qDebug() << "-------------------------------------------";
    QList<StaffInfo> onelist = staff->staffList(1);
    for(int i=0;i<onelist.size();i++)
    {
        qDebug() << "staff select---"<< i << ",group_id:" << onelist.at(i).group_id
                                          << ",staff_id:" << onelist.at(i).staff_id
                                          << ",staff_sex:" << onelist.at(i).staff_sex
                                          << ",staff_name:" << onelist.at(i).staff_name;
    }
    QSqlQueryModel *model = staff->staffModel(5);
    qDebug()<< model->rowCount();
    ui->tableView->setModel(model);
    ui->tableView->show();
}



//staff selCount
void Widget::on_pushButton_depadd_4_clicked()
{
    int result = staff->selStaffCount();
    qDebug() << "staff select count(*)--all-------" << result;
}

/**********************小组操作**********************/
//group add
void Widget::on_pushButton_groupadd_clicked()
{
    group->addGroup(2,"567");
}
//group delete
void Widget::on_pushButton_groupdel_clicked()
{
    //group->delGroup(5);
    //on_pushButton_groupsel_clicked();
    group->delGroup();
}
//group update
void Widget::on_pushButton_groupup_clicked()
{
    group->updateGroup(1,1,"345");
}
//group select
void Widget::on_pushButton_groupsel_clicked()
{
    QList<GroupInfo> list = group->groupList();
    for(int i=0;i<list.size();i++)
    {
        qDebug() << "group select---"<< i << ",dep_id:" << list.at(i).dep_id
                                          << ",group_id:" << list.at(i).group_id
                                          << ",group_name:" << list.at(i).group_name;
    }
qDebug() << "-------------------------------------------";
    QList<GroupInfo> onelist = group->groupList(1);
    for(int i=0;i<onelist.size();i++)
    {
        qDebug() << "group select---"<< i << ",dep_id:" << onelist.at(i).dep_id
                                          << ",group_id:" << onelist.at(i).group_id
                                          << ",group_name:" << onelist.at(i).group_name;
    }
}

//group selCount
void Widget::on_pushButton_depadd_3_clicked()
{
    int result = group->selGroupCount();
    qDebug() << "group select count(*)--all-------" << result;
    result = group->selOneGroup(1);
    qDebug() << "group select count(*)--one-------" << result;


}

//group page
void Widget::on_pushButton_depadd_5_clicked()
{
    QList<GroupInfo> list = group->selPageGroupList(2,3);
    for(int i=0;i<list.size();i++)
    {
        qDebug() << "group select---"<< i << ",dep_id:" << list.at(i).dep_id
                                          << ",group_id:" << list.at(i).group_id
                                          << ",group_name:" << list.at(i).group_name;
    }
}
//staff page
void Widget::on_pushButton_depadd_6_clicked()
{
    QList<StaffInfo> list = staff->selPageStaffList(2,3);
    for(int i=0;i<list.size();i++)
    {
        qDebug() << "staff select---"<< i << ",group_id:" << list.at(i).group_id
                                          << ",staff_id:" << list.at(i).staff_id
                                          << ",staff_sex:" << list.at(i).staff_sex
                                          << ",staff_name:" << list.at(i).staff_name;
    }
}
//dep page
void Widget::on_pushButton_depadd_7_clicked()
{
    QList<DepartmentInfo> list = dep->selPageDepList(3,2);
    for(int i=0;i<list.size();i++)
    {
        qDebug() << "dep select---------"<< i << ",dep_id:" << list.at(i).dep_id
                                              << ",dep_name:" << list.at(i).dep_name;
    }
}

void Widget::on_pushButton_clicked()
{
    #if defined(Q_OS_LINUX)
    qDebug() << "Q_OS_LINUX";
    #elif defined(Q_OS_WIN32)
    qDebug() << "Q_OS_WIN32";
    #endif

}
