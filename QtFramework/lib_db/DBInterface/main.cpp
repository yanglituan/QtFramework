#include "widget.h"
#include <QApplication>
#include <QtSql>
#include <QDebug>
#include <QTextCodec>
#include "DB/sqlHelper.h"
#include "DB/sqlBackUp.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");//情况2
    QTextCodec::setCodecForLocale(codec);

    createConn c;
    QString appPath = QCoreApplication::applicationDirPath()+"/DB.db";
    qDebug() << appPath;
    c.setDriver("QSQLITE");
    c.setDBName(appPath);
    c.openDB();

    Widget w;
    w.show();

    sqlBackUp back;
    QString path="C:/Users/Administrator/Desktop/DB/DBInterface/build-DBInterface-Desktop_Qt_5_6_2_MinGW_32bit-Debug/debug";
    QString dbName = "DB.db";
    QString backName = "back.DB.db";
    back.sqliteBackUp(path,dbName,backName);
    back.sqliteRecovery(path,backName,"DB.rec.db");
    return a.exec();
}
