#-------------------------------------------------
#
# Project created by QtCreator 2019-04-28T18:45:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtFramework
TEMPLATE = app


SOURCES += main.cpp\
        frmmainwin.cpp

HEADERS  += frmmainwin.h

FORMS    += frmmainwin.ui


#net>>com
#INCLUDEPATH +=  $$PWD/lib_net/serialportapi
#include     +=  $$PWD/lib_net/serialportapi/serialportapi.pri
