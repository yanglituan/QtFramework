﻿/*******************************************
*	Copyright(C), 2015-2025, HFDZ Technology.
*
*	Version:1.0
*
*	Author:yw
*
*	Date:2019-5-18
*
*	Description://文件接收服务器，通过监听某一端口，并保存发到这一端口的所有文件。
*********************************************/

#ifndef RECEIVEFILESERVER_H
#define RECEIVEFILESERVER_H

#include <QTcpServer>

class ReceiveFileServer : public QTcpServer
{
    Q_OBJECT
public:
    explicit ReceiveFileServer(QObject *parent = 0);
    ~ReceiveFileServer();

    bool startServer(int port);
    void stopServer();

signals:
    void receiveFileName(QString name);
    void receiveFileSize(qint64 size);
    void finished();
    void message(QString msg);
    void receiveData(qint64 size);

protected:
    void incomingConnection(int handle);

};

#endif // RECEIVEFILESERVER_H
