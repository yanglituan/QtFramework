﻿/*******************************************
*	Copyright(C), 2015-2025, HFDZ Technology.
*
*	Version:1.0
*
*	Author:    yw
*
*	Date:2019-5-18
*
*	Description://使用线程接受文件信息及内容，并保存到文件
*********************************************/

#ifndef RECEIVEFILETHREAD_H
#define RECEIVEFILETHREAD_H

#include <QThread>
#include <QTcpSocket>
#include <QFile>

class ReceiveFileThread : public QThread
{
    Q_OBJECT
public:
    explicit ReceiveFileThread(int socketDescriptor, QObject *parent = 0);
    ~ReceiveFileThread();

    void proccessData(QByteArray &array);

protected:
    void run();

signals:
    void receiveFileName(QString name);
    void receiveFileSize(qint64 size);
    void message(QString msg);
    void receiveData(qint64 size);

private slots:
    void receiveData();
    void disConnect();
    void error();

private:
    QFile file;
    QString fileName;
    QTcpSocket *s;
    qint64 blockSize;
    qint64 blockNumber;

};

#endif // RECEIVEFILETHREAD_H
