﻿/*******************************************
*	Copyright(C), 2015-2025, HFDZ Technology.
*
*	Version:1.0
*
*	Author:  yw
*
*	Date:2019-5-18
*
*	Description://发送文件处理类。
*********************************************/

#ifndef SENDFILECLIENT_H
#define SENDFILECLIENT_H

#include <QTcpSocket>

class SendFileClient : public QTcpSocket
{
    Q_OBJECT
public:
    explicit SendFileClient(QObject *parent = 0);
    ~SendFileClient();

    void sendFile(QString fileName, QString serverIP, int serverPort);

signals:
    void fileSize(qint64 size);
    void message(QString msg);

private slots:
    void sendData();
    void error();

private:
    QString fileName;

};

#endif // SENDFILECLIENT_H
