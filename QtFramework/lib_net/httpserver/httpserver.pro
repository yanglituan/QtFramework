#-------------------------------------------------
#
# Project created by QtCreator 2017-02-08T09:21:04
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET              = httpserver
TEMPLATE            = app
MOC_DIR             = temp/moc
RCC_DIR             = temp/rcc
UI_DIR              = temp/ui
OBJECTS_DIR         = temp/obj
DESTDIR             = bin

CONFIG              += warn_off
SOURCES             += main.cpp

HEADERS             += frmmain.h
SOURCES             += frmmain.cpp
FORMS               += frmmain.ui

INCLUDEPATH         += $$PWD/httpserver
include             ($$PWD/httpserver/httpserver.pri)
