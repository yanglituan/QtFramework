#include "frmmainwin.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    FrmMainWin w;
    w.show();

    return a.exec();
}
